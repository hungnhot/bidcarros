// var baseURL = Drupal.settings.basePath;
var space_allowed = 0;

function init_fileuploader(vars) {
  space_allowed = vars.space_remaining > 0
    ? vars.space_remaining : 1*1024*1024*1024;
  // if set = 0 that is get default value is 1GB

  var max_file_size = vars.max_file_size ? vars.max_file_size : 5000000;

  var types = vars.filetypes.length ? vars.filetypes : 'gif|jpe?g|png';
  var pattern = new RegExp("(\.|\/)(" + types + ")$");

  var total_bytes_cal = 0;
  var added_files = new Array();

  jQuery('form.jquery-file-upload-form').fileupload({
    sequentialUploads: true,
    acceptFileTypes: pattern,
    previewMaxWidth: 120,
    maxFileSize: max_file_size,
    maxNumberOfFiles: 5,
    // Portuguese localization:
    // locale: {
    //     'File is too big': 'O arquivo é muito grande',
    //     'File is too small': 'O arquivo é muito pequeno',
    //     'Filetype not allowed': 'Filetype não permitidos',
    //     'Max number exceeded': 'Número máximo excedido'
    // },
    url: baseURL + '/jquery_file_upload/upload',

    change: function(e, data) {

      var total_bytes = 0;

      jQuery.each(data.files, function(index, file) {
        added_files.push(index);
        total_bytes = total_bytes + file.size;
      });

      if (total_bytes > space_allowed) {
        total_bytes_cal = total_bytes;
        var msg = "You have selected to upload a total of " + hsize(total_bytes) + ".\n";
        msg += "However, you only have " + hsize(space_allowed) + " of remaining space.\n";
        alert(msg);
        total_bytes = 0;
      }
      else {
        total_bytes_cal = 0;
      }
    },

    start: function(e, data) {
      if (total_bytes_cal > space_allowed) {
        jQuery('button.cancel').trigger('click');
        jQuery('.progress-success').hide();
      }
    },

    added: function(e, data) {
      if (total_bytes_cal > space_allowed) {
        jQuery('button.cancel').trigger('click');
        jQuery('.progress-success').hide();
      }
    },

    completed:
            function(e, data) {
              jQuery.ajax({
                type: "POST",
                url: baseURL + '/jquery_file_upload/get_remaining_space',
                success: function(data) {
                  space_allowed = data;
                }});
            }
  });

  jQuery('form.jquery-file-upload-form').bind('fileuploadfinished', function (e, data) {
    // insert to hidden field to apply drupal validation
    var files = new Array();
    jQuery('table tbody.files tr.template-download').find('td.name a').each(function(index) {
      files[index] = jQuery(this).text();
    });
    jQuery('input[name="photo_seller"]').val(files.join());
    if (files.length) {
      jQuery('#check-all-rows').show();
    } else {
      jQuery('#check-all-rows').hide();
    }
  });

  jQuery('form.jquery-file-upload-form').bind('fileuploaddestroyed', function (e, data) {
    var files = new Array();
    jQuery('table tbody.files tr.template-download').find('td.name a').each(function(index) {
      files[index] = jQuery(this).text();
    });
    jQuery('input[name="photo_seller"]').val(files.join());
    if (files.length) {
      jQuery('#check-all-rows').show();
    } else {
      jQuery('#check-all-rows').hide();
    }
  });

  jQuery('form.jquery-file-upload-form').bind('fileuploadfailed', function (e, data) {
      console.log('upload fail');
      jQuery('table tbody.files tr.template-download td.error').each(function(index) {
        var html = jQuery(this).html();
        html = html.replace('Service unavailable (with message)', 'Serviço não disponível');
        jQuery(this).html(html);
      });
  });
  // Load existing files:
  jQuery('form.jquery-file-upload-form').each(function() {
    var that = this;
    jQuery.getJSON(this.action, function(result) {
      if (result && result.length) {
        $(that).fileupload('option', 'done')
                .call(that, null, {
          result: result
        });
      }
    });
  });

}

function hsize(b) {
  if (b >= 1048576)
    return (Math.round((b / 1048576) * 100) / 100) + " mb";
  else if (b >= 1024)
    return (Math.round((b / 1024) * 100) / 100) + " kb";
  else
    return b + " b";
}
