<!-- Redirect browsers with JavaScript disabled to the origin page -->
<noscript><?php print t('Please enable Javascript to use this feature.'); ?></noscript>
<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
<div class="row fileupload-buttonbar">
  <div class="col-md-9 col-xs-12 span8">
    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class="btn btn-success fileinput-button col-md-3  col-sm-3  col-xs-12">
      <i class="icon-plus icon-white fa fa-plus"></i>
      <span><?php print t('Adicionar arquivos'); ?>...</span>
      <input type="file" name="files[]" multiple>
    </span>
    <button type="submit" class="btn btn-primary start col-md-3  col-sm-3  col-xs-12">
      <i class="icon-upload icon-white fa fa-upload"></i>
      <span><?php print t('Iniciar o upload'); ?></span>
    </button>
    <button type="reset" class="btn btn-warning cancel col-md-3  col-sm-3  col-xs-12">
      <i class="icon-ban-circle icon-white fa fa-ban"></i>
      <span><?php print t('Cancelar o envio'); ?></span>
    </button>
    <button type="button" class="btn btn-danger delete col-md-3  col-sm-3 col-xs-12">
      <i class="icon-trash icon-white fa fa-trash"></i>
      <span><?php print t('Excluir'); ?></span>
    </button>
    <span id="check-all-rows" style="display: none">
        <input id="check_all_row" class="toggle" type="checkbox">
        <label for="check_all_row"><?php print t('selecionar tudo'); ?></label>
    </span>
  </div>
  <!-- The global progress information -->
  <div class="col-md-3 col-xs-12 span4 fileupload-progress fade">
    <!-- The global progress bar -->
    <div class="progress progress-success progress-striped active">
      <div class="progress-bar bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
    </div>
    <!-- The extended global progress information -->
    <div class="progress-extended">&nbsp;</div>
  </div>
</div>
<!-- The loading indicator is shown during file processing -->
<div class="fileupload-loading"></div>
<br>
<!-- The table listing the files available for upload/download -->
<table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important"><?php print t('Erro'); ?></span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active"><div class="progress-bar bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="width:0%"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white fa fa-upload"></i>
                    <span><?php print t('Iniciar o upload'); ?></span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel" colspan="2">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white fa fa-ban"></i>
                <span><?php print t('Cancelar'); ?></span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important"><?php print t('Erro'); ?></span> {%=file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="icon-trash icon-white fa fa-trash"></i>
                <span><?php print t('Excluir'); ?></span>
            </button>
            <input type="checkbox" name="delete" value="1">
        </td>
    </tr>
{% } %}
</script>
