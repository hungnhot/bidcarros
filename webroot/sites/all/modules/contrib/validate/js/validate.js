jQuery.validator.addMethod("notEqual", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify a different (non-default) value");
jQuery.validator.addMethod("match", function(value, element, param) {
    var regx = new RegExp(param);
    return this.optional(element) || regx.test(value);
}, "The value does not match with format");
// jQuery(document).ready(function($) {
function scrollToErrMesssage() {
    console.log('scroll');
    jQuery('html, body').animate({ scrollTop: jQuery('.alert-block').offset().top - 100 }, 'slow');
}
(function($) {

    var validate = Drupal.settings.validate;
    if (validate) {
        $.each(validate, function(key, value) {
            var container_name =  "#" + key.replace(/_/g, '-');

            value.errorContainer = container_name + "-error";
            value.errorLabelContainer = container_name + "-error ul";
            value.wrapper = "li";
            value.invalidHandler = function(form, validator) {
                $(this).parent().parent().parent()
                    .find('> .alert-block').remove();
                $errContainer = $(this).find('.alert-block');
                $errContainer.removeClass('hidden');
                setTimeout("scrollToErrMesssage();", 200);
            };
            value.unhighlight = function(element, errorClass) {
                console.log(this.numberOfInvalids());
                if (this.numberOfInvalids() == 0) {
                    $("form .alert-block").addClass('hidden');
                }
                $(element).removeClass(errorClass);
            };
            // value.submitHandler = function (form) {
            //     $(form).find(":submit")
            //         .attr("disabled", true);
            //     $(form).submit();
            // };
            // console.log(value);
            $(container_name).validate(value);
            $(container_name).data("validator").settings.ignore = "";
        });
    }
// });
})(jQuery);
