<?php
/**
 * @file
 * Core file of commerce_auction module
 */


/**
 * Implements hook_views_api().
 */
function commerce_auction_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_menu().
 */
function commerce_auction_menu() {
  $items = array();
  $items['node/%node/placebid'] = array(
    'title' => 'Place bid',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'commerce_auction_access_callback',
    'access arguments' => array(1, 'place bids'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_auction_place_bid_form', 1),
    'file' => 'includes/commerce_auction.pages.inc',
  );

  $items['admin/commerce/auction'] = array(
    'title' => 'Commerce Auction',
    'description' => 'Configure commerce auction behavior and display node types',
    'access arguments' => array('administer commerce auction'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_auction_admin_form'),
    'file' => 'includes/commerce_auction.admin.inc',
  );

  $items['node/%node/bids_list'] = array(
    'title' => 'Bids',
    'page callback' => array('commerce_auction_bids_list'),
    'page arguments' => array(1),
    'access callback' => 'commerce_auction_bids_list_access_callback',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Embeds bids_list view in the page.
 */
function commerce_auction_bids_list($node) {
  return commerce_embed_view('bids_list', 'master', array($node->nid));
}

/**
 * Access callback for bids list page.
 */
function commerce_auction_bids_list_access_callback($node) {
  $types = variable_get('commerce_auction_display_types', array());
  if (!in_array($node->type, $types)) {
    return FALSE;
  }
  if (user_access('eck view commerce_auction_bid auction_bid entities')) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_permission().
 */
function commerce_auction_permission() {
  return array(
    'place bids' =>  array(
      'title' => t('Place bids on auctions'),
    ),
    'remove won auctions from cart' => array(
      'title' => t('Remove won auctioned products from cart'),
    ),
    'administer commerce auction' => array(
      'title' => t('Change Commerce auction options'),
    ),
  );
}

/**
 * Access callback for bidding menu item.
 */
function commerce_auction_access_callback($node, $access_arg) {
  global $user;
  $types = variable_get('commerce_auction_display_types', array());
  if (in_array($node->type, $types) === FALSE) {
    return FALSE;
  }
  $timeout_field = field_get_items('node', $node, 'auction_timeout');
  if (!isset($timeout_field) ||
      empty($timeout_field) ||
      !$timeout_field[0]['value']) {
    drupal_set_message(
      t('Auction timeout has no value, Please set a value for the timeout field.'),
      'warning'
    );
    return FALSE;
  }
  $expiration_timestamp = $timeout_field[0]['value'];
  // If this auction is hosted by this user or is expired, do not allow her to
  // access this page.
  if ($expiration_timestamp <= REQUEST_TIME) {
    return FALSE;
  }

  if ($user->uid == 1) {
    return TRUE;
  }
  if (!user_access($access_arg)) {
    return FALSE;
  }

  // If this auction is hosted by this user or is expired, do not allow her to
  // access this page.
  if ($user->uid == $node->uid
      || $expiration_timestamp <= REQUEST_TIME
      || !user_access('place bids')) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Returns highest bid on an auction.
 *
 * @param object $node
 *   display node of the auctioned product.
 *
 * @return int
 *   highest bid raw value or -1 if there is no bid on the product.
 */
function commerce_auction_get_highest_bid($node) {
  $field_value = field_get_items('node', $node, 'auction_highest_bid');
  return ($field_value) ? $field_value[0]['amount'] : -1;
}

/**
 * Returns highest bid owner or FALSE if there are no bids on $node.
 */
function commerce_auction_get_highest_bid_uid($node) {
  $field_value = field_get_items('node', $node, 'auction_bid_refs');
  if (empty($field_value)) {
    return FALSE;
  }

  $highest_bid_entity = !empty($field_value) ? entity_load('commerce_auction_bid', array($field_value[0]['target_id'])) : NULL;
  $highest_bid = commerce_auction_get_highest_bid($node);
  foreach ($field_value as $value) {
    $ent = entity_load('commerce_auction_bid', array($value['target_id']));
    $bid_amount_field = field_get_items('commerce_auction_bid', $ent[$value['target_id']], 'bid_amount');
    if ($bid_amount_field[0]['amount'] == $highest_bid) {
      $highest_bid_entity = $ent[$value['target_id']];
      break;
    }
  }

  return $highest_bid_entity ? $highest_bid_entity->uid : FALSE;
}

/**
 * Returns name of a field instance.
 *
 * @param string $entity_type
 *   Type of the entity to which the field is attached.
 *
 * @param string $bundle
 *   Bundle of the entity type
 *
 * @param string $field_type
 *   Type of the field instance
 */
function commerce_auction_find_field_name($entity_type, $bundle, $field_type) {
  foreach (field_info_instances($entity_type, $bundle) as $instance_name => $instance) {
    // Load the field info for the current instance.
    $field = field_info_field($instance['field_name']);

    // If it's of the proper type...
    if ($field['type'] == $field_type) {
      // Save its name and exit the loop if it isn't the billing profile.
      return $instance_name;
    }
  }
  return NULL;
}

/**
 * Implements hook_node_view().
 */
function commerce_auction_node_view($node, $view_mode, $langcode) {
  $types = variable_get('commerce_auction_display_types', array());
  if (in_array($node->type, $types) !== FALSE) {
    $timeout_field = field_get_items('node', $node, 'auction_timeout');
    $expiration_timestamp = $timeout_field[0]['value'];
    // If auction is expired, add the product to winenr cart.
    if ($expiration_timestamp <= REQUEST_TIME) {
      $uid = commerce_auction_get_highest_bid_uid($node);
      if (!$uid) {
        return;
      }

      // Add the product referenced by this auction to the user cart with a
      // commerce_auction_lineitem.
      $product_reference = field_get_items('node', $node, 'field_product');
      $product_reference = $product_reference[0];
      $order = commerce_cart_order_load($uid);
      $product = commerce_product_load($product_reference['product_id']);

      if (empty($order)) {
        $order = commerce_order_new($uid, 'checkout_checkout');
        commerce_order_save($order);
      }
      elseif (commerce_auction_order_contains_product($order, $product)) {
        return;
      }

      // Update product price.
      $highest_bid = field_get_items('node', $node, 'auction_highest_bid');
      $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
      $product_wrapper->commerce_price = $highest_bid[0];
      $product_wrapper->save();

      $line_item = commerce_product_line_item_new($product, 1, $order->order_id, array(), 'commerce_auction_lineitem');
      commerce_line_item_save($line_item);
      commerce_cart_product_add($uid, $line_item);
    }
  }
}

/**
 * Returns bid count of an auction.
 *
 * @param object $node
 *   auction display node
 */
function commerce_auction_get_bid_count($node) {
  $node = node_load($node->nid, NULL, TRUE);
  $field_value = field_get_items('node', $node, 'auction_bid_refs');
  if (!$field_value) {
    return 0;
  }
  return count($field_value);
}

/**
 * Implements hook_node_presave().
 */
function commerce_auction_node_presave($node) {
  $types = variable_get('commerce_auction_display_types', array());
  if (in_array($node->type, $types) !== FALSE) {
    $product_reference = field_get_items('node', $node, 'field_product');
    $product_reference = $product_reference[0];
    $product = commerce_product_load($product_reference['product_id']);
    $price = field_get_items('commerce_product', $product, 'commerce_price');
    $node->auction_starting_price[LANGUAGE_NONE][0]['amount'] = $price[0]['amount'];
    $node->auction_starting_price[LANGUAGE_NONE][0]['currency_code'] = $price[0]['currency_code'];
  }
}


/**
 * The function checks to see if the first parameter can be cleanly.
 */
function commerce_auction_dividable($big, $small) {
  $div = $big / $small;

  if (!is_numeric(strpos($div, '.')) === TRUE) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_node_validate().
 */
function commerce_auction_node_validate($node, $form, &$form_state) {
  if (isset($form_state['values']['field_max_bid_inc'])) {
    $max_bid_inc = $form_state['values']['field_max_bid_inc'][LANGUAGE_NONE]['0']['value'];
    if ($max_bid_inc == 0) {
      $max_bid_inc = variable_get('commerce_auction_max_bid_inc', 1000);
    }
  }
  else {
    $max_bid_inc = variable_get('commerce_auction_max_bid_inc', 1000);
  }

  if (isset($form_state['values']['field_min_bid_inc'])) {
    $min_bid_inc = $form_state['values']['field_min_bid_inc'][LANGUAGE_NONE]['0']['value'];
    if ($min_bid_inc == 0) {
      $min_bid_inc = variable_get('commerce_auction_min_bid_inc', 0.25);
    }
  }
  else {
    $min_bid_inc = variable_get('commerce_auction_min_bid_inc', 0.25);
  }

  if ($max_bid_inc == 0 && $min_bid_inc > 0) {
    $global_max_bid_inc = variable_get('commerce_auction_max_bid_inc', 1000);
    if ($global_max_bid_inc < $min_bid_inc) {
      form_set_error('field_max_bid_inc', t('Minimum bid increment value is more than the global maximum bid increment value (@val).', array('@val' => $global_max_bid_inc)));
    }
  }
  if ($min_bid_inc == 0 && $max_bid_inc > 0) {
    $global_max_bid_inc = variable_get('commerce_auction_min_bid_inc', 0.25);
    if ($global_max_bid_inc < $min_bid_inc) {
      form_set_error('field_max_bid_inc', t('Maximum bid increment value is less than the global minimum bid increment value (@val).', array('@val' => $global_min_bid_inc)));
    }
  }
  if ($max_bid_inc != -1 && $max_bid_inc < $min_bid_inc) {
    form_set_error('field_max_bid_inc', t('Maximum bid increment value should be more than Minimum bid increment.'));
  }
}

/**
 * This function returns increment settings using the given parameters.
 *
 * @param array $items
 *   the array returned by field_get_items().
 *   should be in format array(0 => array('value' => 123))
 *
 * @param string $global
 *   global options for the increment settings.
 */
function commerce_auction_get_increment_limit($items, $global) {
  if (!$items) {
    return $global;
  }
  if ($items[0]['value'] == -1) {
    return 0;
  }
  elseif ($items[0]['value'] > 0) {
    return $items[0]['value'];
  }
  return $global;
}

/**
 * Implements hook_form_alter().
 */
function commerce_auction_form_alter(&$form, &$form_state, $form_id) {
  $types = variable_get('commerce_auction_display_types', array());
  if ($form_id == 'views_form_commerce_cart_form_default') {
    foreach (array('edit_delete', 'edit_quantity') as $form_key) {
      foreach ($form[$form_key] as $key => $item) {
        if (is_array($item)) {
          $line_item = commerce_line_item_load($item['#line_item_id']);
          if ($line_item->type == 'commerce_auction_lineitem' && !user_access('remove won auctions from cart')) {
            $form[$form_key][$key]['#disabled'] = TRUE;
          }
        }
      }
    }
  }
}

/**
 * This function auction related removes fields from content types.
 *
 * @param array $old_types
 *   The auction display content types before updating. Auction related fields
 *   will be removed from content types in this array if those types are not in
 *   $new_types.
 *
 * @param array $new_types
 *   The new auction display content types.
 */
function commerce_auction_remove_fields($old_types, $new_types = array()) {
  $field_names = array(
    'auction_bid_refs',
    'auction_highest_bid',
    'auction_timeout',
    'auction_starting_price',
    'field_bid_increment',
    'field_max_bid_inc_percent',
    'field_min_bid_inc',
    'field_max_bid_inc',
    'field_product',
  );
  foreach ($old_types as $type) {
    if (in_array($type, $new_types) === FALSE) {
      // $type has been removed from auction display types,
      // remove unused fields.
      foreach ($field_names as $field_name) {
        $field = field_info_instance('node', $field_name, $type);
        if ($field) {
          field_delete_instance($field);
        }
      }
    }
  }
}

/**
 * Check to see if $order contains $product.
 *
 * @param object $order
 *   commerce_order object.
 *
 * @param object $product
 *   commerce_product object.
 */
function commerce_auction_order_contains_product($order, $product) {
  $line_items = field_get_items('commerce_order', $order, 'commerce_line_items');
  if (!empty($line_items)) {
    foreach ($line_items as $delta => $line_item_entry) {
      if ($line_item = commerce_line_item_load($line_item_entry['line_item_id'])) {
        $product_field = field_get_items('commerce_line_item', $line_item, 'commerce_product');
        if ($product_field[0]['product_id'] == $product->product_id) {
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}
