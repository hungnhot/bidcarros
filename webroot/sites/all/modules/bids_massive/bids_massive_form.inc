<?php 
  function bids_massive_form(){
  	global $user;
	  // $form['notes'] = array(
	  //   '#type' => 'markup',
	  //   '#markup' => '<div class="import-notes">A few notes when uploading. <ul><li>Make sure the file is in a .csv format.</li><li>Columns should be in *this* order</li><li>Be sure to click the "Upload" button when you select a csv.</li></ul></div>',
	  //   '#upload_location' => 'public://tmp/',
	  // );
	  $form['import'] = array(
	    '#title' => t('Importação CSV'),
	    '#type' => 'managed_file',
	    '#description' => t('O CSV carregou será importado e guardado temporariamente.'),
	    '#upload_location' => 'public://',
	    '#upload_validators' => array(
	      'file_validate_extensions' => array('csv'),
	    ),
	  );
	  $form['submit'] = array (
	    '#type' => 'submit',
	    '#value' => t('Importação'),
	  );
	  return $form;
  }

  function bids_massive_form_submit($form, $form_state) {
  // Check to make sure that the file was uploaded to the server properly
	  $uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array(
	    ':fid' => $form_state['values']['import'],
	  ))->fetchField();
	  // echo"<pre>";print_r($form_state);
	  // die();
	  if(!empty($uri)) {
	    if(file_exists(drupal_realpath($uri))) { 
	      // Open the csv
	      $handle = fopen(drupal_realpath($uri), "r");
	      // Go through each row in the csv and run a function on it. In this case we are parsing by '|' (pipe) characters.
	      // If you want commas are any other character, replace the pipe with it.
	      while (($data = fgetcsv($handle, 0, ',', '"')) !== FALSE) {
	        $operations[] = array($data);
	      }

	      bids_massive_processing($operations);
	      // Once everything is gathered and ready to be processed... well... process it!
	      $batch = array(
	        'title' => t('Importação CSV...'),
	        'operations' => $operations,  // Runs all of the queued processes from the while loop above.
	        'finished' => 'bids_massive_import_finished', // Function to run when the import is successful
	        'error_message' => t('The installation has encountered an error.'),
	        'progress_message' => t('Imported @current of @total products.'),
	      );
	      batch_set($batch);
	      fclose($handle);    
	    }
	  }
	  else {
	    drupal_set_message(t('There was an error uploading your file. Please contact a System administator.'), 'error');
	  }
	}
	function bids_massive_processing($data) {
		global $user;
		$fields = $data[0][0];
		$len = count($fields);
		while (list($key, $val) = each($data)) {
			$import_car = validate_car_name($data[$key][0][0], $data[$key][0][1]);
			$comment = validate_comment($data[$key][0][16]);
			$valid_value = validate_value(floatval($data[$key][0][17]));
			$msg = "importação de erro";
			$type_msg = 'error';
			if($key > 0 && $import_car == true && $comment == true && $valid_value == true){
				$entity = entity_create('sale_bid', array('seller_id' => $user->uid));
				$entity->vehicle_id = $import_car->modelo;
				$entity->vehicle_year = $import_car->name_ano;
				$entity->body_type = $body;
				$entity->purpose = $purpose;
				$entity->km_travelled = $data[$key][0][4];
				$entity->patent = $data[$key][0][5];
				$entity->vehicle_status = $data[$key][0][6];
				$entity->registration = $data[$key][0][7];
				$entity->air_conditioning = $data[$key][0][8] ? 0 : 1;
				$entity->air_bag = $data[$key][0][9] ? 0 : 1;
				$entity->power_windows = $data[$key][0][10] ? 0 : 1;
				$entity->sunroof = $data[$key][0][11] ? 0 : 1;
				$entity->brakes = $data[$key][0][12] ? 0 : 1;
				$entity->shield_type = $data[$key][0][13];
				$entity->shield_year = $data[$key][0][14];
				$entity->color = $data[$key][0][15];
				$entity->comment = $data[$key][0][16];
				$entity->initial_sale_value = floatval($data[$key][0][17]);
				$entity->location = $data[$key][0][18];
			  $entity->lat = $data[$key][0][19];
			  $entity->lng = $data[$key][0][20];
				$entity->created_at = date('Y-m-d H:i:s');
				$entity->updated_at = date('Y-m-d H:i:s');
			  $date = date_add(date_create(), date_interval_create_from_date_string('30 days'));
				$entity->expired_at = $date-> format('Y-m-d H:i:s');
				$entity->sale_bid_status = $data[$key][0][21];
				if ($entity->save()) {
					$msg = "importação completa";
					$type_msg = 'status';
				}
			}
		}
		drupal_set_message($msg, $type = $type_msg);
	}

	function validate_value($value){
		if(is_numeric($value)){
			return true;
		}else{
			return false;
		}
	}

	function validate_car_name($name,$year){
		$data = $year."%";
    $query = "SELECT * FROM(
													SELECT n.id  AS id_modelo ,n.nome, a.id, a.nome AS name_ano,a.modelo
													FROM tbl_modelo n
						            	INNER JOIN tbl_ano_modelo a ON n.id = a.modelo
													WHERE n.nome = :name) b
							WHERE b.name_ano like :year" ;
    $result = db_query($query,array(':name' => $name,':year' => $data))->fetchObject();
    return $result;
	}

	function validate_comment($comment){
		if(strlen($comment) > 200){
			return false;
		}else{
			return true;
		}
	}
	function bids_massive_import_finished() {
	  drupal_set_message(t('Import Completed Successfully'));
	}
?>