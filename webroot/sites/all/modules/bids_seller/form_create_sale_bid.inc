<?php
/**
 * Create Sale-BID form
 * @author lbngoc@LARION
 */

/**
 * BID-191 and BID-198
 * The Client wishes to remove this page (step 1) from the Sales Bid Flow
 */
define('DISABLE_FORM_STEP', 'Refer: BID-191 and BID-198');
//== END

function form_create_sale_bid($form, &$form_state)
{
  $form['#attributes'] = array(
    'class' => 'jquery-file-upload-form',
    // 'enctype' => 'multipart/form-data'
    );
  $form['#client_validation'] = TRUE;

  $form['validate_container'] = array(
    '#type' => 'item',
    '#markup' => '<div id="form-create-sale-bid-error" class="alert alert-block alert-danger hidden"><a class="close" data-dismiss="alert" href="#">×</a><ul></ul></div>'
    );

  if (isset($_SESSION['form-create-sale-bid-state']) && user_is_logged_in()) {
    $form_state = array_merge($form_state, $_SESSION['form-create-sale-bid-state']);
    unset($_SESSION['form-create-sale-bid-state']);
    $form['#token'] = FALSE;
  }

  if (!isset($_SESSION['car']) || $_SESSION['car'] === false) {
    // die('no car');
    drupal_goto('search-car');
  }
  $car = $_SESSION['car'];
  drupal_add_js(drupal_get_path('module', 'bids_seller') .
    '/bids_seller.js', array('group' => JS_THEME));
  $options = bids_seller_first_dropdown_options();
  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    if (isset($_SESSION['form_step']) && user_is_logged_in()) {
      $form_state['step'] = intval($_SESSION['form_step']);
      unset($_SESSION['form_step']);
      // var_dump("get from step from seesion");
    } else {
      $form_state['step'] = 1;
    }
  }
  $step = $form_state['step'];
  // Show advance filter or not
  if (empty($form_state['show_advance_filter'])) {
    $form_state['show_advance_filter'] = defined('DISABLE_FORM_STEP') ? true : false;
  }
  // var_dump(sizeof($form_state));
  /************ Form Step 1 **************/
  // row 1
  $form['model_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Modelo'),
    '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
    '#field_suffix' => '<i class="fa fa-check-circle fa-2x"></i>',
    '#default_value' => $car->model,
    // '#attributes' => array(
    //   'disabled' => 'disabled'
    // ),
    '#disabled' => TRUE,
    );
  // row 2
  // $form['car'] = array(
  //   '#type' => 'textfield',
  //   '#title' => t('Carro'),
  //   '#wrapper_attributes' => array('class' => array('col-md-4 col-sm-6')),
  //   '#default_value' => $car->car,
  //   '#disabled' => TRUE,
  // );

  $form['make'] = array(
    '#type' => 'textfield',
    '#title' => t('Marca'),
    '#wrapper_attributes' => array('class' => array('col-md-4 col-sm-6 col-xs-12')),
    '#default_value' => $car->make,
    '#attributes' => array(
      // 'disabled' => 'disabled',
      'style' => array('background-image: url("'.get_marca_logo($car->make).'")'),
      ),
    '#disabled' => TRUE,
    );

  $options_ano = array( 0 => '- Todos -');
  $ano_select = $car->years_list;
  foreach ($ano_select as $key => $value) {
    $options_ano[$key] = $value;
  }

  $form['year'] = array(
    '#type' => 'select',
    '#title' => t('Ano'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-4 col-sm-6 col-xs-12'),
      ),
    '#options' => $options_ano,
    '#default_value' => 0,
    // '#attributes' => array(
    //   // jQuery validate
    //   'required' => true,
    //   'notEqual' => "0",
    //   'title' => t('Por favor, selecione o ano.')
    // ),
    // '#disabled' => ($step > 1),
    );

  $form['num_of_seller'] = array(
    '#type' => 'item',
    '#title' => t('Ofertas disponíveis'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-4 col-sm-12 col-xs-12'),
      ),
    '#markup' => '<i class="fa fa-map-marker"></i><div class="ctn"></div>',
    );

  /************ Form Step 2 **************/
  if ( ($step > 1 && user_is_logged_in()) || defined('DISABLE_FORM_STEP') )
  {
    if ($form_state['show_advance_filter']) {
      // separater
      // $form['separater'] = array(
      //   '#type' => 'item',
      //   '#wrapper_attributes' => array(
      //     'class' => array('col-md-12 col-sm-12 form-group'),
      //     ),
      //   '#markup' => '<h4 class="title">' . t('Indique recursos') . '</h4>'
      //   );

      $form['condition'] = array(
        '#type' => 'select',
        '#wrapper_attributes' => array(
          'class' => array('col-md-4 col-sm-4 col-xs-12 form-group'),
          ),
        '#title' => 'Estado veiculo',
        '#options' => $options['condition'],
        );

      $form['km_travell'] = array(
        '#type' => 'textfield',
        '#title' => 'KM rodados',
        '#wrapper_attributes' => array(
          'class' => array('col-md-4 col-sm-4 col-xs-12 form-group'),
          ),
        //'#field_prefix' => '<div class="input-group spinner">',
        //'#field_suffix' => '<div class="input-group-btn-vertical"><button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button><button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button></div></div>',
        // '#element_validate' => array('_validate_km_travell'),
        '#default_value' => 0,
        '#element_validate' => array('_validate_km_travell'),
        '#maxlength' => 9,
        // '#validate' => array(
        //   'max' => array(9999999, t('KM viajar não é grande do que 9999999 e deve ser dígitos. ')),
        //   ),
        );

      $form['enviroment'] = array(
        '#type' => 'textfield',
        '#title' => 'Placa do veículo',
        '#wrapper_attributes' => array(
          'class' => array('col-md-4 col-sm-4 col-xs-12 form-group '),
          ),
        '#element_validate' => array('_validate_enviroment'),
        '#validate' => array(
          'match' => array('([A-Z]{3,3}-[0-9]{4,4})', t('Você deve digitar registo correcto'))
          ),
        '#maxlength' => 8,
        );
      /* BID-204 : Remove  Blindagem */
      // $form['type_of_sheild'] = array(
      //   '#type' => 'select',
      //   '#title' => 'Blindagem',
      //   '#options' => $options['type_of_sheild'],
      //   '#wrapper_attributes' => array(
      //     'class' => array('col-md-3 col-sm-6 form-group'),
      //   ),
      // );

      $form['optional_extras'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Opcionais / Acessórios',
        '#wrapper_attributes' => array('class' => array('group-colour-sheild col-md-12 col-sm-12 col-xs-12')),
        // '#prefix' => '<div class="container">',
        // '#suffix' => '</div>',
        '#options' => $options['optional_extras'],
        '#options_wrapper_attributes' => $options['optional_extras_wrapper_attributes']
        );

      $default_colors = array('Preto', 'Prata', 'Branco', 'Outro');
      // hidden color for validation
      $form['color_textfield_hidden'] = array(
        '#type' => 'textfield',
        '#wrapper_attributes' => array(
          'class' => array('col-md-12 col-sm-12 form-item-step-2 hidden'),
          ),
        '#validate' => array(
          'required' => array(1, t('Você deve escolher uma cor')),
          // 'notEqual' => array(0, t('Por favor, selecione pelo menos uma cor.'))
          ),
        // "#default_value" => count($default_colors),
        );
      $form['color'] = array(
        '#type' => 'radios',
        '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2')),
        '#title' => t('Selecione a cor do veículo'),
        '#options' => $options['colors'],
        '#options_wrapper_attributes' => $options['colors_wrapper_attributes'],
        // '#required' => TRUE,
        '#element_validate' => array('_validate_colors'),
        // '#attributes' => array(
        // // jQuery validate
        //   'required' => true,
        //   'title' => t('Você deve escolher uma cor'),
        //   ),
        // "#default_value" => $default_colors[0],
        );
    }

    $form['price'] = array(
      '#type' => 'textfield',
      '#title' => 'Minha Oferta',
      '#required' => TRUE,
      '#element_validate' => array('_validate_price'),
      '#maxlength' => 9,
      '#attributes' => array(
        // jQuery validate
        'required' => true,
        'title' => t('Você deve digitar preço valor correto.'),
        ),
      // '#validate' => array(
      //   'min' => array(1,t('Você deve digitar preço de valor > 0 e basta digitar o número. ') ),
      //   ),
      );
    /* BID-204: Add “Market values” +/-5% (26.287 – 29.054).*/
    $price_market_min = "-";
    $price_market_max = "-";
    if (is_numeric($car->price_popular) && $car->price_popular > 0) {
      $price_market_min = number_format($car->price_popular * 95/100, 0, ',', '.');
      $price_market_max = number_format($car->price_popular * 105/100, 0, ',', '.');
    }
    $price_min     = is_numeric($car->price_min) ? number_format($car->price_min, 0, ',', '.') : $car->price_min;
    $price_max     = is_numeric($car->price_max) ? number_format($car->price_max, 0, ',', '.') : $car->price_max;
    $insurance_min = get_seguro_price_of_vehicle($car->model_id)->min_seguro;
    $insurance_min = is_numeric($insurance_min) && $insurance_min > 0 ? number_format($insurance_min, 0, ',', '.') : "-";

    $form['vehicle_price_info'] = array(
      '#type' => 'item',
      '#title' => t('Valores BIDCARROS'),
      '#wrapper_attributes' => array(
        'class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2'),
        ),
      '#markup' => '<div class="col-md-2 col-sm-12 col-xs-12" id="form-item-price"><!-- .form-item-price //--></div>' .
      '<div id="segurar-price" class="col-md-4 col-sm-12 col-xs-12">
      <div id="min-price-create-sale" class="minn txt col-sm-5 col-md-5 col-xs-12">
        <span class="prefix  col-sm-2 col-md-2 col-xs-1">R$</span>
        <span class="ctn col-sm-10 col-md-10 col-xs-10 ">'.$price_market_min.'</span>
      </div>
      <div id="line-seperate" class="col-xs-2 col-sm-2 col-md-2 text-center" >-</div>
      <div id="max-price-create-sale" class="max txt col-sm-5 col-md-5 col-xs-12">
        <span class="prefix col-sm-2 col-md-2 col-xs-1">R$</span>
        <span class="ctn col-sm-10 col-md-10 col-xs-10">'.$price_market_max.'</span>
      </div>
      <div class="txt-desc col-md-12 col-sm-12">'.t('Valores de mercado').'</div>
    </div>'.
    '<div class="col-md-2 col-sm-4 col-xs-12" id="price-min">
    <i class="fa fa-arrow-circle-down col-md-3 col-sm-3 col-xs-2"></i>
    <div class="txt col-sm-9 col-md-9 col-xs-10">
      <span class="prefix">R$</span>
      <span class="ctn">'.$price_min.'</span><br/>'
      .t('menor preço').
      '</div>
    </div>' .
    '<div class="col-md-2 col-sm-4 col-xs-12" id="price-max">
    <i class="fa fa-arrow-circle-up col-md-3 col-sm-3 col-xs-2 "></i>
    <div class="txt col-sm-9 col-md-9 col-xs-10">
      <span class="prefix">R$</span>
      <span class="ctn">'.$price_max.'</span><br/>'
      .t('maior preço').
      '</div>
    </div>' .
    '<div class="col-md-2 col-sm-4 col-xs-12" id="insurance-min">
    <i class="fa lock-icon col-sm-3 col-md-3 col-xs-3"></i>
    <div class="txt col-sm-9 col-md-9 col-xs-9">
      <span class="prefix">R$</span>
      <span class="ctn">'.$insurance_min.'</span><br/>'
      .t('seguro').
      '</div>
    </div>',
       // move .form-item-price to this block
    '#suffix' => '<script>var $moved = jQuery(".form-item-price"); $moved.clone().prependTo("#form-item-price");$moved.remove();</script>'
    );
$form['addition_car'] = array(
  '#type' => 'textarea',
  '#title' => 'Observações adicionais sobre o veículo',
  '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2')),
  '#size' => 3,
  '#element_validate' => array('_validate_textarea'),
  );

$form['list-matches-bid'] = array(
  '#type' => 'item',
  '#title' => t('OFERTAS'),
  '#wrapper_attributes' => array(
    'class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2'),
    ),
  '#markup' => '<div id="related-bids-wrapper"></div>'
         // . list_related_purchase_bids($related_bids, false)
         // . '</div>'
  );
    // location field
$form['location_display'] = array(
  '#type' => 'item',
  '#title' => t('LOCALIZAÇÃO'),
  '#wrapper_attributes' => array(
    'class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2'),
    ),
  '#markup' => '<div id="map-canvas"></div>',
  '#suffix' => '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&&key=AIzaSyBTiT75yZ5OWYHn3w0QEgtqsTgqvWT0Upw&language=en"></script>',
  );
$form['location'] = array(
  '#type' => 'hidden',
  '#title' => 'Location Address',
  );

$form['lng'] = array(
  '#type' => 'hidden',
  '#title' => 'Location Address Lng.',
  );

$form['lat'] = array(
  '#type' => 'hidden',
  '#title' => 'Location Address Lat.',
  );

    // photo field
$form['photo_seller'] = array(
  '#type' => 'textfield',
  '#element_validate' => array('_validate_photos'),
  '#wrapper_attributes' => array(
    'class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2 hidden'),
    ),
  '#validate' => array(
        // jQuery validate
    'required' => array(1, t('Por favor selecione fotos de seu carro.')),
    ),
  );
$block = module_invoke('jquery_file_upload', 'block_view', 0);
$form['photos'] = array(
  '#type' => 'item',
  '#prefix' => '<div class="col-md-12 col-sm-12 col-xs-12 form-item-step-2 form-item form-group"><label>' . t('Fotos do seu carro') . ' * </h4></div>',
  '#wrapper_attributes' => array(
    'class' => array('col-md-12 col-sm-12 col-xs-12 form-item-step-2'),
    ),
  '#markup' => '<div class="container-fluid">'.render($block['content'])."</div>"
  );
    /*$form['photo_seller'] = array(
      '#type' => 'plupload',
      '#title' => t('Fotos do seu carro'),
      '#wrapper_attributes' => array(
        'class' => array('col-md-12 col-sm-12 form-item-step-2'),
      ),
      // '#description' => t('This multi-upload widget uses Plupload library.'),
  '#submit_element' => '#id-of-your-submit-element',
  '#upload_location' => 'public://seller_bids',
  '#autosubmit' => TRUE,
  '#upload_validators' => array(
    'file_validate_extensions' => array('jpg jpeg gif png'),
    ),
  '#plupload_settings' => array(
    'runtimes' => 'html5',
    'chunk_size' => '1mb',
    'dragdrop' => 'true',
    'views' => array(
      'grid' => true,
      'thumbs' => true,
      'active' => 'thumbs'
      )
    ),
  '#element_validate' => array('_validate_photos'),
      // '#required' => TRUE,
  );*/
}
  // END step 2

  // Show "Back to Search page" button [In Step 1]
if ($step == 1 || defined('DISABLE_FORM_STEP')) {
  $form['back-to-search'] = array(
    '#type' => 'button',
    '#prefix' => '<div class="col-md-4 col-md-offset-4 col-sm-12"><a href="'.url('search-car').'">',
    '#suffix' => '</a></div>',
    '#name' => 'back_to_search',
    '#value' => '<span class="fa fa-angle-left"></span>'.t('Voltar'),
    '#attributes' => array(
      'class' => array('btn btn-info pull-right full-width'),
      )
    );
}
  // Show "Advance & Simple Filter" button [In Step 2]
else  {
  if ($form_state['show_advance_filter'] === false) {
      // Show button for Show Advance filter
    $form['advance-search'] = array(
      '#type' => 'submit',
      '#name' => 'show_advance_filter',
      '#prefix' => '<div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12">',
      '#suffix' => '</div>',
      '#value' => t('Pesquisa <b>Avancada</b>') . ' <span class="fa fa-angle-right"></span>',
      '#attributes' => array(
        'class' => array('btn btn-primary full-width'),
        )
      );
  }
}

$submit_msg = $step == 1 && !defined('DISABLE_FORM_STEP')
? t('Procurar') : t('Criar <b>OFERTA</b>');
  // Primary submit
$form['submit'] = array(
  '#type' => 'submit',
  '#name' => 'submit',
  '#prefix' => '<div class="search-button '. ($form_state['show_advance_filter'] && !defined('DISABLE_FORM_STEP') ? 'col-md-offset-8 ' : '') .'col-md-4 col-sm-12 col-xs-12">',
  '#suffix' => '</div>',
  '#value' => $submit_msg . '<span class="fa fa-angle-right"></span>',
  '#attributes' => array(
    'class' => array('btn btn-info full-width'),
    ),
  );

return $form;
}

// function form_create_sale_bid_validate($form, &$form_state){
//   // echo "<pre>"; print_r($form['distance']);die();
//   // var_dump($form_state['values']);die;
//   if ($form['year']['#value'] == '0'){
//     form_set_error('year', t('Por favor, selecione o ano.'));
//   }
// }

function form_create_sale_bid_submit($form, &$form_state)
{
  $current_step = &$form_state['step'];
  $form_values = $form_state['values'];
  $car = &$_SESSION['car'];
  if ($current_step === 1 && !defined('DISABLE_FORM_STEP')) // go to next step
  {
    if ($form_state['clicked_button']['#id'] === 'edit-back-to-search') {
      // die('back to search');
      drupal_goto('search-car');
    }
    if (user_is_logged_in()) {
      $current_step++;
    } else {
      $bidcarros_settings = array(
        'showLoginModal' => true,
        );
      drupal_add_js(array('bidcarros' => $bidcarros_settings), "setting");
      $_SESSION['redirect'] = "seller/create-bid";
      $_SESSION['form-create-sale-bid-state'] = $form_state;
      $_SESSION['form_step'] = $current_step+1;
    }
    $form_state['rebuild'] = TRUE;
    return;
  }
  elseif ($form_state['clicked_button']['#id'] === 'edit-advance-search') {
    $form_state['show_advance_filter'] = true;
    $form_state['rebuild'] = TRUE;
  }
  // Press "Create BID"
  elseif ($form_state['clicked_button']['#id'] === 'edit-submit')
  {
    $_SESSION['form-bids-seller'] = $form_state['values'];
    // // Upload photos
    // if (!empty($form_state['values']['photo_seller'])) {
    // //   $form_state['uploaded_files'] = quotes_file_save_files($form_state['values']['photo_seller']);
    // }
    if (user_is_logged_in())
    {
      global $user;
      $_SESSION['upload_folder'] = $user->uid;
      drupal_goto('seller-info');
    }
    elseif (defined('DISABLE_FORM_STEP'))
    {
      $bidcarros_settings = array(
        'showLoginModal' => true,
        );
      drupal_add_js(array('bidcarros' => $bidcarros_settings), "setting");
      $_SESSION['redirect'] = "seller-info";
      $form_state['rebuild'] = TRUE;
      $_SESSION['upload_folder'] = "0";
    }
  }
  // In other cases, just reload page with form_state
  else {
    $form_state['rebuild'] = TRUE;
  }
}


/**
 * Validation
 */
function _validate_textarea($element, $form, $form_state) {
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  if (drupal_strlen($element['#value']) > 200) {
    form_error($element, t('Comentário não pode ter mais de 200 caracteres, mas é atualmente caracteres.'));
  }
}

function _validate_km_travell($element, $form, $form_state) {
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  $element['#value'] = preg_replace('/[^1-9]/', '', $element['#value']); // Removes special cha
  if(!empty($element['#value'])) {
    if($element['#value'] <= 0  || $element['#value'] >= 9999999 || !is_numeric($element['#value']) ) {
      form_error($element, t('Você deve definir os km percorridos > 0 porque a condição é usado & km percorrida devem um número'));
    }
  }
}

function _validate_price($element, $form, $form_state) {
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  $element['#value'] = preg_replace('/[^0-9]/', '', $element['#value']); // Removes special cha
  if(intval($element['#value']) <= 0  || intval($element['#value']) > 9999999) {
    form_error($element, t('Você deve digitar preço valor correto'));
  }
}

function _validate_enviroment($element, $form, $form_state) {
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  if($element['#value']){
    if(!preg_match('/([A-Z]{3,3})(-)([0-9]{4,4})/', $element['#value'])) {
      form_error($element, t('Você deve digitar registo correcto'));
    }
  }
}

function _validate_photos($element, $form, $form_state) {
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  if(empty($element['#value'])) {
    form_error($element, t('Por favor selecione fotos de seu carro'));
  }
}

function _validate_colors($element, $form, $form_state) {
  // $errors = drupal_get_messages();
  // form_clear_error();
  if ($form['clicked_button']['#id'] !== 'edit-submit') {
    return;
  }
  if(empty($element['#value'])) {
    form_error($element, t('Você deve escolher uma cor'));
  }
}

/**
 * PLUplaod files
 */
function quotes_file_save_files($files) {
  $saved_files = array();
  $timestamp = time();
  $upload_dir = drupal_realpath('public://') . '/seller_bids';
  if (!is_dir($upload_dir)) {
    $contine = mkdir($upload_dir, 0777, true);
    if (!$contine) {
      return;
    }
  }
  if (mkdir($upload_dir . '/' . $timestamp, 0777, true)) {
    $scheme = 'public://seller_bids/' . $timestamp . '/'; // Change to "public://" if you want to move to public files folder.
    foreach ($files as $uploaded_file) {
      if ($uploaded_file['status'] == 'done') {
        $source = $uploaded_file['tmppath'];
        $destination = file_stream_wrapper_uri_normalize($scheme . $uploaded_file['name']);
        $destination = file_unmanaged_move($source, $destination, FILE_EXISTS_RENAME);
        $file = plupload_file_uri_to_object($destination);
        file_save($file);
        // Check the $file object to see if all necessary properties are set. Otherwise, use file_load($file->fid) to populate all of them.
        // $file = file_load($file->fid);
        $saved_files[] = $file;
        $_SESSION['timestamp'] = $timestamp;
      }
    }
  }
  return $saved_files;
}
