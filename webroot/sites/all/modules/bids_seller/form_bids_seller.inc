<?php

	/**
	 * Page callback mapped to the url /bids_seller
	 *
	 * @return array
	 */
	function form_bids_seller($form, &$form_state) {
		if (!isset($_SESSION['car']) || $_SESSION['car'] === false) {
			drupal_goto('search-car');
		}
		$car = $_SESSION['car'];
		drupal_add_js(drupal_get_path('module', 'bids_seller') . '/bids_seller.js', array('group' => JS_THEME));

		$options = bids_seller_first_dropdown_options();

	  $link_logo = get_marca_logo($car->make);

		$form["sale_bid"]["model"] = array(
			'#type' => 'textfield',
			'#title' => t('Modelo'),
			'#default_value' => $car->model,
			'#disabled' => TRUE,
			);

		// $form["sale_bid"]["car"] = array(
		// 	'#type' => 'textfield',
		// 	'#title' => t('Carro'),
		// 	'#default_value' => $car->car,
		// 	'#disabled' => TRUE,
		// 	);

		$form["sale_bid"]["year"] = array(
			'#type' => 'select',
			'#title' => t('Ano'),
			'#options' => $car->years_list,
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
			'#prefix' => "<div class='wrapper-info-car col-xs-12'>",
			);

		$form["sale_bid"]["make"] = array(
	    '#type' => 'item',
	    '#title' => t('Marca'),
	    '#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
	    '#markup' => '<div id="div_logo"><img src="' . $link_logo . '"></div><input disabled="disabled" class="form-control form-text" type="text" value="' . $car->make . '">',
	    '#suffix' => '</div>',
	  	);

		$form['sale_bid']['feature'] = array(
			'#type' => 'item',
			'#markup' => "<p class='label-fieldset'>Indique recursos</p>",
			);

		$form['sale_bid']['body_type'] = array(
			'#type' => 'select',
			'#title' => 'Carroceria',
			'#options' => $options['body_type'],
			);

		$form['sale_bid']['purpose'] = array(
			'#type' => 'select',
			'#title' => 'Necessidade',
			'#options' => $options['purpose'],
			);

		$form['sale_bid']['condition'] = array(
			'#type' => 'select',
			'#title' => 'Estado veiculo',
			'#options' => $options['condition'],
			);

		$form['sale_bid']['km_travell'] = array(
			'#type' => 'textfield',
			'#title' => 'KM rodados',
			'#element_validate' => array('_validate_km_travell'),
			'#default_value' => 0,
			);

		$form['sale_bid']['enviroment'] = array(
			'#type' => 'textfield',
			'#title' => 'Placa do veículo',
			'#element_validate' => array('_validate_enviroment'),
			);

		$form['sale_bid']['optional_extras'] = array(
			'#type' => 'checkboxes',
			'#title' => 'Opcionais / Acessórios',
			'#prefix' => '<div class="group-colour-sheild" >',
			'#options' => $options['optional_extras'],
			);

		$form['sale_bid']['type_of_sheild'] = array(
			'#type' => 'select',
			'#title' => 'Tipo de Blindagem',
			'#options' => $options['type_of_sheild'],
			'#suffix' => '</div>',
			);

		// $form['sale_bid']['sheild_age'] = array(
		// 	'#type' => 'select',
		// 	'#title' => 'Ano Blindagem',
		// 	'#options' => $options['sheild_age'],
		// 	'#suffix' => '</div>',
		// 	);

		$form['sale_bid']['colour'] = array(
			'#type' => 'checkboxes',
			'#title' => 'Selecione a cor do veículo',
			'#options' => $options['colour'],
			'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
			);

		$form['sale_bid']['addition_car'] = array(
			'#type' => 'textarea',
			'#title' => 'Observações adicionais sobre o veículo',
			'#size' => 3,
			'#maxlength' => 200,
			'#element_validate' => array('_validate_textarea'),
			);

		$form['sale_bid']['some_text'] = array(
			'#type' => 'item',
			'#markup' => '<p class="markup-price">Valores</p>',
			);

		$form['sale_bid']['price'] = array(
			'#type' => 'textfield',
			'#title' => 'Minha Oferta',
			'#required' => TRUE,
			'#element_validate' => array('_validate_price'),
			'#wrapper_attributes' => array('class' => array('col-xs-4')),
			'#prefix' => '<div class="col-xs-12">',
			);

		$form['sale_bid']['minium_price'] = array(
			'#type' => 'item',
			'#title' => t('menor preço'),
			'#markup' => "<i class='fa fa-arrow-circle-down fa-2x'></i><span class='price-prefix'>R$  </span>
			<span class='price-value'>" .get_min_price_of_vehicle($car->model_id). "</span>",
			'#description' => 'menor preço',
			'#wrapper_attributes' => array('class' => array('col-xs-4')),
			);

		$form['sale_bid']['maximum_price'] = array(
			'#type' => 'item',
			'#title' => t('maior Preço'),
			'#markup' => "<i class='fa fa-arrow-circle-up fa-2x'></i><span class='price-prefix'>R$  </span>
			<span class='price-value'>" .get_max_price_of_vehicle($car->model_id). "</span>",
			'#description' => 'maior Preço',
			'#wrapper_attributes' => array('class' => array('col-xs-4')),
			'#suffix' => '</div>',
			);

		$form['sale_bid']['location_text'] = array(
			'#type' => 'item',
			'#markup' => '<p class="markup-price">Localizacion</p>',
			);

		$form['sale_bid']['location_display'] = array(
			'#type' => 'item',
			'#markup' => '<div class="canvas_map"><div id="map-canvas"></div></div>',
			);
		$form['sale_bid']['location'] = array(
			'#type' => 'hidden',
			);
		$form['sale_bid']['lng'] = array(
			'#type' => 'hidden',
			);
		$form['sale_bid']['lat'] = array(
			'#type' => 'hidden',
			);
		$form['sale_bid']['photo_seller'] = array(
			'#type' => 'plupload',
			'#title' => t('Fotos do seu carro'),
			'#description' => t('This multi-upload widget uses Plupload library.'),
			'#submit_element' => '#id-of-your-submit-element',
			'#upload_location' => 'public://seller_bids',
			'#autosubmit' => TRUE,
			'#upload_validators' => array(
				'file_validate_extensions' => array('jpg jpeg gif png'),
				'my_custom_file_validator' => array('some validation criteria'),
				),
			'#plupload_settings' => array(
				'runtimes' => 'html5',
				'chunk_size' => '1mb',
				),
			'#required' => TRUE,
			);

		$form['sale_bid']['submit'] = array(
			'#type' => 'submit',
			'#value' => t('PRÓXIMO<span class="fa fa-angle-right"></span>'),
			);

		$form['sale_bid']['script_ggmap'] = array(
			'#type' => 'item',
			'#markup' => '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&&key=AIzaSyBTiT75yZ5OWYHn3w0QEgtqsTgqvWT0Upw&language=en"></script>',
			);

		return $form;
	}

	function _validate_textarea($element, $form, $form_state) {
		if (isset($element['#maxlength']) && drupal_strlen($element['#value']) > $element['#maxlength']) {
			form_error($element, t('!name cannot be longer than %max characters but is currently %length characters long.'));
		}
	}

	function _validate_km_travell($element, $form, $form_state) {
		if(!empty($element['#value'])){
			if( ($form['values']['condition'] == 1  && ($element['#value'] <= 0  && $element['#value'] >= 9999999)) || ($form['values']['condition'] == 0 && $element['#value'] > 0) || !is_numeric($element['#value'])) {
				form_error($element, t('Você deve definir os km percorridos > 0 porque a condição é usado & km percorrida devem um número'));
			}
		}
	}

	function _validate_price($element, $form, $form_state) {
		if(!is_numeric($element['#value'])){
			form_error($element, t('Você deve digitar preço valor correto'));
		}
	}

	function _validate_enviroment($element, $form, $form_state) {
		if($element['#value']){
			if(!preg_match('/([A-Z]{3,3})(-)([0-9]{4,4})/', $element['#value'])) {
				form_error($element, t('Você deve digitar registo correcto'));
			}
		}
	}

	function quotes_file_save_files($files) {
		$saved_files = array();
		$timestamp = time();
		$upload_dir = drupal_realpath('public://') . '/seller_bids';
		if (!is_dir($upload_dir)) {
			$contine = mkdir($upload_dir, 0777, true);
			if (!$contine) {
				return;
			}
		}
		if (mkdir($upload_dir . '/' . $timestamp, 0777, true)) {
		  $scheme = 'public://seller_bids/' . $timestamp . '/'; // Change to "public://" if you want to move to public files folder.
		  foreach ($files as $uploaded_file) {
		  	if ($uploaded_file['status'] == 'done') {
		  		$source = $uploaded_file['tmppath'];
		  		$destination = file_stream_wrapper_uri_normalize($scheme . $uploaded_file['name']);
		  		$destination = file_unmanaged_move($source, $destination, FILE_EXISTS_RENAME);
		  		$file = plupload_file_uri_to_object($destination);
		  		file_save($file);
		      // Check the $file object to see if all necessary properties are set. Otherwise, use file_load($file->fid) to populate all of them.
		      // $file = file_load($file->fid);
		  		$saved_files[] = $file;
		  		$_SESSION['timestamp'] = $timestamp;
		  	}
		  }
		}
		return $saved_files;
	}

	/**
	* hook status messages
	* @param variables
	*/


	/**
	* Submit form bid seller to next seller info page
	*/
	function form_bids_seller_submit($form, &$form_state) {
		$_SESSION['form-bids-seller'] = $form_state['values'];
		if (!empty($form_state['values']['photo_seller'])) {
			$form_state['uploaded_files'] = quotes_file_save_files($form_state['values']['photo_seller']);
		}
		if (user_is_logged_in()) {
			drupal_goto('seller-info');
		} else {
			$form_state['rebuild'] = TRUE;
			$bidcarros_settings = array(
				'showLoginModal' => true,
				);
			drupal_add_js(array('bidcarros' => $bidcarros_settings), "setting");
		  // set flag to redirect this page after logged in
		  // see bidcarros_user_view_page()
			$_SESSION['redirect'] = "seller-info";
		}
	}
