<?php global $user; ?>
<div class="container-fluid">
<?php
  $rowCount = 0;

  foreach ($result_seguro as $item) {
    $seguro = $item->seguro;
    if (isset($item->valor_min) && isset($item->valor_max)) {
      if ($item->valor_min == $item->valor_max) {
        $market_min = $item->valor_min * 95/100;
        $market_max = $item->valor_max * 105/100;
      } else {
        $market_min = $item->valor_min;
        $market_max = $item->valor_max;
      }
    } else {
      $market_min = $item->valor * 95/100;
      $market_max = $item->valor * 105/100;
    }
  }

  foreach ($result_price as $item) {
    $price_min = $item->min_price;
    $price_max = $item->max_price;
  }
?>
<?php foreach ($result_buy_bid as $item) : ?>
  <?php if (isset($item->buyer_id)) { ?>
    <div class="row row-item">
      <div class="col-md-3 col-sm-3 clear-md">
        <b class="text-truncate"><?php print ucwords($item->buyer_name); ?></b>
      </div>
      <div class="col-md-4 col-sm-2">
        <?php
          if (isset($item->city)) {
            print $item->city;
          } else {
            $buyer_loc = extra_field_block_view($item->buyer_id, 'field_data_field_address', 'field_address_value');
            print $buyer_loc;
          }
        ?>
      </div>
      <div class="col-md-2 col-sm-2">
        <?php print get_date_diff(date('Y-m-d H:i:s'), $item->expired_at); ?>
      </div>
      <div class="col-md-2 col-sm-3 col-xs-8 bid-rate">
        <?php $rank = rand(1,5); ?>
        <?php for ($i=0;$i<$rank;$i++) {
          print '<i class="fa fa-star"></i>';
        } ?>
        <?php for ($i=0;$i<5-$rank;$i++) {
          print '<i class="fa fa-star-o"></i>';
        } ?>
      </div>
      <div class="col-md-1 col-sm-2 col-xs-4 menu-action">
        <a href="<?php print url("user/{$user->uid}/bid-details/{$item->id}"); ?>">
          <i class="fa fa-ellipsis-v"></i></a>
      </div>
    </div>
    <?php
      $rowCount++;
    ?>
  <?php } ?>
<?php endforeach; ?>
<?php if ($rowCount == 0) : ?>
  <div class="row">
    <div class="col-md-12">
      <?php print t('Deixe os potenciais compradores saibam mais sobre seu carro.'); ?>
    </div>
  </div>
<?php endif; ?>
</div>
<?php
  $seguro = is_numeric($seguro) && $seguro > 0 ? number_format($seguro, 0, ',', '.') : "-";
  $price_min = is_numeric($price_min) && $price_min > 0 ? number_format($price_min, 0, ',', '.') : "-";
  $price_max = is_numeric($price_max) && $price_max > 0 ? number_format($price_max, 0, ',', '.') : "-";
  $market_min = is_numeric($market_min) && $market_min > 0 ? number_format($market_min, 0, ',', '.') : "-";
  $market_max = is_numeric($market_max) && $market_max > 0 ? number_format($market_max, 0, ',', '.') : "-";
?>
<input type="hidden" id="seguro" value="<?php @print $seguro; ?>" />
<input type="hidden" id="minPrice" value="<?php @print $price_min; ?>" />
<input type="hidden" id="maxPrice" value="<?php @print $price_max; ?>" />
<input type="hidden" id="minMarket" value="<?php @print $market_min; ?>" />
<input type="hidden" id="maxMarket" value="<?php @print $market_max; ?>" />
