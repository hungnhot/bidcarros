<?php print $rendered; ?>

<div class="modal fade" id="map-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Localização do Carro</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 form-type-textfield form-item-name form-item form-group">
            <label for="address-cep">CEP</span></label>
            <input class="form-control form-text" id="address-cep" name="address-cep">
          </div>
          <div class="col-xs-12 form-type-textfield form-item-name form-item form-group">
            <label for="address-street">Endereço</span></label>
            <input class="form-control form-text" id="address-street" name="address-street">
          </div>
          <div class="col-xs-6 form-type-textfield form-item-name form-item form-group">
            <label for="address-street_number">Número</span></label>
            <input class="form-control form-text" id="address-street_number" name="address-street_number">
          </div>
          <div class="col-xs-6 form-type-textfield form-item-name form-item form-group">
            <label for="address-complemento">Complemento</span></label>
            <input class="form-control form-text" id="address-complemento" name="address-complemento">
          </div>
          <div class="col-xs-12 form-type-textfield form-item-name form-item form-group">
            <label for="address-bairro">Bairro</span></label>
            <input class="form-control form-text" id="address-bairro" name="address-bairro">
          </div>
          <div class="col-xs-12 form-type-textfield form-item-name form-item form-group">
            <label for="address-city">Cidade</span></label>
            <input class="form-control form-text" id="address-city" name="address-city">
          </div>
          <div class="col-xs-12 form-type-textfield form-item-name form-item form-group">
            <label for="address-estado">Estado</label>
            <input class="form-control form-text" id="address-estado" name="address-estado">
          </div>
          <div class="col-xs-3 form-group pull-right">
            <a href='#' class="btn btn-info form-control" id="edit_address">
              OK
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>