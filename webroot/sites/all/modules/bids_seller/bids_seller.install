<?php

/**
 * Implements hook_schema().
 *
 * Defines the database tables used by this module.
 * Remember that the easiest way to create the code for hook_schema is with
 * the @link http://drupal.org/project/schema schema module @endlink
 *
 * @see hook_schema()
 */
function bids_seller_schema() {
  $schema['tbl_sale_bids'] = array(
   'description' => 'Store Bids of Seller',
   'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Sale Bid Id',
      ),
      'seller_id' => array(
       'type' => 'int',
       'not null' => TRUE,
       'description' => 'Id of Seller that create this bid',
      ),
      'vehicle_id' => array(
       'type' => 'varchar',
       'not null' => TRUE,
       'length' => 20,
       'description' => 'Id of Car model',
      ),
      'vehicle_year' => array(
       'type' => 'varchar',
       'not null' => TRUE,
       'length' => 20,
       'description' => 'Year of Car model',
      ),
      'body_type' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'Body Type',
      ),
      'purpose' => array(
       'type' => 'varchar',
       'length' => '255',
       'description' => 'Purpose of Seller',
      ),
      'km_travelled' => array(
       'type' => 'int',
       'description' => 'KM that car have travelled for used car',
      ),
      'patent' => array(
       'type' => 'varchar',
       'length' => '45',
       'description' => 'Patent of Seller for his sale car',
      ),
      'vehicle_status' => array( // change from `condition` to `vehicle_status` to avoid SQL error
       'type' => 'varchar',
       'length' => '45',
       'description' => 'New or Used',
      ),
      'registration' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'Car Registration Number',
      ),
      'air_conditioning' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have air-condition?',
      ),
      'airbag' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have air-bag?',
      ),
      'power_windows' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have windows?',
      ),
      'sunroof' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have sunroof?',
      ),
      'brakes' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have brake?',
      ),
      'shield' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Does this car have shield?',
      ),
      'shield_type' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'Which type of shield does this car have?',
      ),
      'shield_year' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'How old is the shield?',
      ),
      'color' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'Color of the car',
      ),
      'comment' => array(
       'type' => 'text',
       'description' => 'Additional information',
      ),
      'initial_sale_value' => array(
       'type' => 'float',
       'description' => 'offer value of seller',
       'size' => 'big',
      ),
      'location' => array(
       'type' => 'varchar',
       'length' => '100',
       'description' => 'Car location',
      ),
      'lng' => array(
       'type' => 'float',
       'description' => 'Car location longtitude',
      ),
      'lat' => array(
       'type' => 'float',
       'description' => 'Car location latitude',
      ),
      'created_at' => array(
       'type' => 'datetime',
       'mysql_type' => 'datetime',
       'description' => 'Created date',
      ),
      'updated_at' => array(
       'type' => 'datetime',
       'mysql_type' => 'datetime',
       'description' => 'Updated date',
      ),
      'expired_at' => array(
       'type' => 'datetime',
       'mysql_type' => 'datetime',
       'description' => 'Expiration date',
      ),
      'sale_bid_status' => array(
       'type' => 'int',
       'default' => 0,
       'description' => 'Sale Bid Status',
      ),
      'close_reason' => array(
       'type' => 'text',
       'description' => 'Closed Reason',
      ),
    ),
   'indexes' => array(
      'sale_bids_idx'      => array('vehicle_id', 'location', 'initial_sale_value', 'sale_bid_status'),
      'seller_idx'         => array('seller_id'),
    ),
    'foreign keys' => array(
      'bid_vehicle' => array(
        'table' => 'modelo',
        'columns' => array('vehicle_id' => 'id'),
       ),
      'bid_author' => array(
        'table' => 'users',
        'columns' => array('seller_id' => 'id'),
       ),
     ),
    'primary key' => array('id'),
     );

  return $schema;
}

/**
  * Implement hook_enable()
  */
function bids_seller_enable() {
  //Check if table exists, if not install the schema.
  if(db_table_exists('tbl_sale_bids') == FALSE) {
    drupal_install_schema('bids_seller');
  }
}
