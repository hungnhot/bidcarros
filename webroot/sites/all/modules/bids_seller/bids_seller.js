is_sale_page = true;

(function ($) {
  $(document).ready(function() {
    if (typeof Drupal.settings.bidcarros != 'undefined') {
      if (Drupal.settings.bidcarros.showLoginModal) {
        $('#login-modal').modal('show');
      }
    }
    if (typeof google !== "undefined") {
      jQuery('#map-canvas').show();
      initialize_map();
    }

    // $('select#edit-year').change(function(e) {
    //   get_total_related_bids();
    // });
edit_cep_field($('#address-cep'));
edit_license_plate($('input[id="edit-enviroment"]'));
format_edit_price_and_km_travel($('input[id^="edit-price"]'));
format_edit_price_and_km_travel($('input[id^="edit-my-offer"]'));
format_edit_price_and_km_travel($('input[id^="edit-km-travell"]'));

$('select#edit-year, #edit-color input').change(function(e) {
  get_related_purchase_bids();
});

$('#edit-seller-image').change(function(){
  readURL(this);
});

$('input[id^="edit-color"]').change(function(event) {
  $('#edit-color-textfield-hidden').val($('input[id^="edit-color"]:checked').length);
});

function readURL(input) {
  if (input.files){
    $.each(input.files,function(i,e){
      $(".form-type-file label[for='edit-seller-image']").append("<div class='image-preview-seller_"+ i +"'><img id='preview_"+ i +"' src='' width='500' alt='your image'/></div>");
      var reader = new FileReader();
      reader.onload = function (e) {
        var parent = $('.image-preview-seller_'+ i);
        parent.find('#preview_'+ i).attr('src', e.target.result);
            // parent.find('#preview').bind("click",function(){
            //   parent.find('#preview').click();
            // })
  };
  reader.readAsDataURL(input.files[i]);
});
  }
}
var image = jQuery('.user-picture a img').attr('src');
jQuery('.image-preview img').attr('src',image);

/* config the image field */
    // colorImage.each(function(){
    //   var selectDivColor = jQuery(this).find('div[class^="color-image"]');
    //   var divColorToAfter = jQuery(this).find('input');
    //   divColorToAfter.before(selectDivColor);
    // });

/* append a icon check into modelo select */
var modeloSelect = jQuery('#form-bids-seller .form-item-model');
var iconCheck = '<i class="fa fa-check-circle fa-2x"></i>';
modeloSelect.append(iconCheck);

/* config the field of km travell in bids seller page */
var kmRodados = jQuery('form .form-item-km-travell input');
var condition = jQuery('form .form-item-condition select');

condition.change(function(){
      if (condition.val() == 1) { // Used
        kmRodados.removeAttr('disabled');
      }else{
        // kmRodados.attr('disabled', 'disabled');
        // kmRodados.val(0);
      }
    });
    if (condition.val() == 0) { // New
      // kmRodados.attr('disabled', 'disabled');
      kmRodados.val(0);
    }

    /* make km travell field became a number field */
    // jQuery('form #edit-km-travell').attr('type', 'number').addClass('spinner');
    // $('#edit-km-travell').focusout(function(event) {
    //   $(this).val(parseInt(0 + $(this).val()));
    // });
    // BID-253 KM Rodados
    /*if ($('.spinner #edit-km-travell').length > 0) {
      $('.spinner .btn:first-of-type').on('click', function() {
        if ($('.spinner input').is(':disabled')) { return; }
        var value = parseInt($('.spinner input').val(), 10) + 1;
        $('.spinner input').val( value <= 9999999 ? value : 9999999 ) ;
      });
      $('.spinner .btn:last-of-type').on('click', function() {
        if ($('.spinner input').is(':disabled')) { return; }
        var value = parseInt($('.spinner input').val(), 10) - 1;
        $('.spinner input').val( value >= 0 ? value : 0 ); //set min value = 0
      });
}*/

/* countdown_time */
var started_at = parseInt($('#edit-started-at label').text());
var expired_at = parseInt($('#edit-expired-at label').text());
var date_now_server = parseInt($('#edit-date-now label').text());

countdown_time(started_at, expired_at, date_now_server);

    // Close BID
    option_close_check();
    $('button[name="show-close-bid"]').on('click', function(){
      $('#close-modal').modal('show');
    });

    $('button[name="show-reason"]').on('click', function(e){
      $('#reason-modal').modal('show');
    });

    $('button[id="close-reason-form"]').on('click', function(e){
      $('#reason-modal').modal('hide');
    });

    // Auto load ofertas list after page load
    var url = window.location.href;
    if ( url.indexOf('seller/create-bid') > 0 ) {
      if ($('#related-bids-wrapper').length > 0) {
        get_related_purchase_bids();
      }
    }
  });

  // http://drupal.stackexchange.com/questions/109731/how-to-limit-number-of-files-uploaded-with-plupload-integrtion-module
  Drupal.plupload = Drupal.plupload || {};
  /**
   * Attaches the Plupload behavior to each Plupload form element.
   */
   Drupal.behaviors.download_plupload = {
    attach: function (context, settings) {
      $(".plupload-element", context).once('download-plupload-init', function () {
        var add_button = $(this).find('.plupload_button.plupload_add');
        var uploader = $(this).pluploadQueue();
        uploader.bind('QueueChanged', function() {
          var local_uploader_element = this;
          // Remove files exceeding the cardinality setting.
          if (local_uploader_element.files.length >= 5) {
            var i = 0;
            for (i=0;i<local_uploader_element.files.length;i++) {
              if (i >= 5) {
                local_uploader_element.removeFile(uploader.files[i]);
              }
            }
            add_button.hide();
          }
          else {
            add_button.show();
          }
        });
      });
    }
  };

  /**
   * For new Sale-BID flow
   * @author  lbngoc
   */

   function get_total_related_bids() {
    // $('#edit-submit').attr('disabled', 'disabled');
    var url = baseURL + '/seller/get-related-bids?';
    $.ajax({
      url: url,
      data: {model_year: $('#edit-year').val()},
    })
    .done(function(response) {
      var numOfBID = $(response).find('div.row-item').length;
      // display num-of-bid
      if (numOfBID < 10) {
        numOfBID = "00" + numOfBID;
      } else if (numOfBID < 100) {
        numOfBID = "0" + numOfBID;
      }
      $('#edit-num-of-seller').find('.ctn').text(numOfBID);
    });

  }

  function get_related_purchase_bids() {
    var url = baseURL + '/seller/get-related-bids?';
    var model_year = $('#edit-year').val();
    var colors = $('#edit-color input:checked');
    var color;
    // more filter...
    url += model_year && model_year != '0' ? 'model_year=' + encodeURIComponent(model_year) + '&' : '';
    if (colors.length > 0) {
      color = new Array(colors.length);
      colors.each(function(index, el) {
        color[index] = $(el).val();
      });
      url += 'color=' + encodeURIComponent(color.join()) + '&';
    }
    // done, get ajax callback
    $('#related-bids-wrapper').load(url, function( response, status, xhr ) {
      // console.log(status);
      if ( status == "success" ) {
        var numOfBID = $(response).find('div.row-item').length;
        if (numOfBID < 10) {
          numOfBID = "00" + numOfBID;
        } else if (numOfBID < 100) {
          numOfBID = "0" + numOfBID;
        }
        $('#edit-num-of-seller').find('.ctn').text(numOfBID);
        var insurance_DOM = $(this);
        var minPrice = insurance_DOM.find("#minPrice").attr('value');
        var maxPrice = insurance_DOM.find("#maxPrice").attr('value');
        var insurance = insurance_DOM.find("#seguro").attr('value');
        var marker_min = insurance_DOM.find("#minMarket").attr('value');
        var marker_max = insurance_DOM.find("#maxMarket").attr('value');
        $('#price-min').find('.ctn').text(minPrice);
        $('#price-max').find('.ctn').text(maxPrice);
        $('#insurance-min').find('.ctn').text(insurance);
        $('.minn').find('.ctn').text(marker_min);
        $('.max').find('.ctn').text(marker_max);
      }
    });
}

})(jQuery);

/* Show Address form in Sale Bid Create page */
function showAddressForm(address){
  console.log(address.postal_code.length);
  jQuery('#map-modal').modal('show');
  if(address.postal_code.length == 9){
    jQuery('#address-cep').val(address.postal_code).change();
  }else{
    jQuery('#address-cep').val('');
  }
  jQuery('#address-street').val(address.route);
  jQuery('#address-street_number').val(address.street_number);
  jQuery('#address-bairro').val(address.neighborhood);
  jQuery('#address-city').val(address.locality);
  jQuery('#address-estado').val(address.administrative_area_level_1);

  validate_modal();
  // if(validate_modal()){
  //   change_cep(jQuery('#address-cep'));
  // }

  jQuery('#edit_address').click(function(){
    var new_address = getChosenAddress();
    jQuery("input[name^='location']").val(new_address);
    jQuery('#map-modal').modal('hide');
    getAddressOnMap({address: new_address}, function(address){
      jQuery("input[name^='lng']").val(address.geometry.location.lng());
      jQuery("input[name^='lat']").val(address.geometry.location.lat());
      placeMarker(address.geometry.location);
      map.setCenter(address.geometry.location);
    });
    return false;
  });
}

function validate_modal(){
  var cep = jQuery('#map-modal').find('#address-cep');
  cep.blur(function(){
    console.log(cep.val());
    if(cep.val().length < 9){
      cep.val('');
    }else{
      setTimeout(function(e){
        fetch_area_sale_bid(cep.val());
      },0)
    }
  });
}
function change_cep(cep){
  cep.on('change', function(e) {
    setTimeout(function(e){
      fetch_area_sale_bid(cep.val());
    },0)
  });
}

function fetch_area_sale_bid(zip){
  // var a = "05709-010"; // CEP test  Rua Ubajara
  if(zip.length == 9){
    var zipcode = zip.replace('-','');
    jQuery.getJSON("http://viacep.com.br/ws/"+ zipcode +"/json",function(data,status){
      if(status == "success" && !data.erro){
        jQuery('#address-estado').val(data.uf).change();
        jQuery('#address-bairro').val(data.bairro).change();
        jQuery('#address-complemento').val(data.complemento).change();
        jQuery('#address-city').val(data.localidade).change();
        jQuery('#address-street').val(data.logradouro).change();
      }else{
        jQuery('#map-modal').find('#address-cep').val('');
      }
    });
  }
}

function getChosenAddress(){
  var street = jQuery('#address-street').val();
  var number = jQuery('#address-street_number').val();
  var city = jQuery('#address-city').val();
  var state = jQuery('#address-estado').val();

  return [number + street, state, city].filter(Boolean).join(',');
}

function countdown_time(started_at, expired_at, date_now_server) {

  var days, hours, minutes, seconds;
  // get tag element
  var day = jQuery('div.day span:first-child');
  var hour = jQuery('div.hour span:first-child');
  var min = jQuery('div.min span:first-child');
  var sec = jQuery('div.sec span:first-child');

  progress = jQuery('#edit-time-progress');
  progress.attr('aria-valuemin', started_at);
  progress.attr('aria-valuemax', expired_at);

  var current_date = date_now_server;
  var seconds_left = (expired_at - current_date) / 1000;

  if (expired_at <= current_date) {
    jQuery('button[name="show-close-bid-expired"]').attr("disabled", "disabled");
  }

  if (date_now_server < expired_at && seconds_left > 0) {
    var timeId = setInterval(function () {
      current_date += 1000;
      seconds_left = (expired_at - current_date) / 1000;
      var percent = ((current_date - started_at) / (expired_at - started_at)) * 100;

      // do some time calculations
      days = parseInt(seconds_left / 86400);
      seconds_left = seconds_left % 86400;

      hours = parseInt(seconds_left / 3600);
      seconds_left = seconds_left % 3600;

      minutes = parseInt(seconds_left / 60);
      seconds = parseInt(seconds_left % 60);

      if (seconds_left <= 0) {
        jQuery('button[name="show-close-bid"]').attr("disabled", "disabled");
        clearInterval(timeId);
      }

      day.text(days < 10 ? '0' + days : days);
      hour.text(hours < 10 ? '0' + hours : hours);
      min.text(minutes < 10 ? '0' + minutes : minutes);
      sec.text(seconds < 10 ? '0' + seconds : seconds);

      progress.attr('aria-valuenow', current_date);
      progress.css('width', percent + '%');

    }, 1000);
  }
}

function option_close_check(){
  jQuery("#option_5").on('click', function(){
    jQuery('textarea').attr('disabled', false);
  });

  jQuery("#option_1, #option_2, #option_3, #option_4").on('click', function(){
    jQuery('textarea').attr('disabled', true);
  });
}


// Input license plate (Placa do veículo)
function format_license_plate(o,f){
  var v_obj = o;
  var v_fun = f;
  setTimeout(exec_license_plate(v_obj, v_fun),1);
}

function exec_license_plate(v_obj, v_fun){
  v_obj.value = v_fun;
}

function license_plate(v){

  v = v.replace(/(\w{3})(\d{3})/,"$1-$2");

  return v;
}

function edit_license_plate(data) {
  data.on('keypress', function(e) {
    format_license_plate(this,license_plate(this.value));
  });

  data.on('blur', function(e) {
    clearTimeout();
  });
}

function format_edit_price_and_km_travel(value) {
  value.on('keypress', function(e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
      jQuery("#errmsg").html("Apenas dígitos").show().fadeOut("slow");
      return false;
    }
    mascaraMutuario(this,price_KM_travellSetInterval(this.value));
  });

  value.on('blur', function(e) {
    clearTimeout();
  });

  value.on('paste', function(e) {
    var self = this;
    setTimeout(function(e) {
      format_price_and_km_travell(value);
    }, 0);
  });
}

function format_price_and_km_travell(value) {
  var val = value.val().replace(/[^a-zA-Z0-9]/g, "");
  var result = val ;
  switch (result.length) {
    case 7:
      // result = result.replace(/[^a-zA-Z0-9]/g, "");
      result = [result.slice(0, 1), '.', result.slice(1)].join('');
      result = [result.slice(0, 5), '.', result.slice(5)].join('');
      km_travell.val(result);
      break;
      case 6:
      result = [result.slice(0, 3), '.', result.slice(3)].join('');
      km_travell.val(result);
      break;
      case 5:
      result = [result.slice(0, 2), '.', result.slice(2)].join('');
      km_travell.val(result);
      break;
      case 4:
      result = [result.slice(0, 1), '.', result.slice(1)].join('');
      km_travell.val(result);
      break;
      default:
      break;
    }
  }

  function price_KM_travellSetInterval(v) {
    v = v.replace(/\D/g,"");
    switch (v.length) {
      case 7:
      v = v.replace(/(\d{1})(\d)/,"$1.$2");
      v = v.replace(/(\d{3})(\d)/,"$1.$2");
      break;
      case 6:
      v = v.replace(/(\d{1})(\d)/,"$1.$2");
      v = v.replace(/(\d{3})(\d)/,"$1.$2");
      break;
      case 5:
      v = v.replace(/(\d{3})(\d)/,"$1.$2");
      break;
      case 4:
      v = v.replace(/(\d{2})(\d)/,"$1.$2");
      break;
      case 3:
      v = v.replace(/(\d{1})(\d)/,"$1.$2");
      break;
      default:
      break;
    }
    return v;
  }

