<?php
$seller_uid = $form['variable_seller_info']['#value'];

print render($form['title_bid_detail']);

print render($form['logo']);

print render($form['car']);

print render($form['year']);

print render($form['km_travell']);

print render($form['minium_price_text']);

print render($form['gallery']);

print render($form['vehicle']);

print render($form['registration']);

print render($form['bid_detail_color']);

// if (count($form['optional_extras']['#value']) > 0) {
  print render($form['optional_extras']);
// }

print render($form['observations']);

if ($user->uid == $seller_uid) {
	print render($form['market_values_text']);

	print render($form['my_offer']);

	print render($form['text_offers_no_bid']);

	print render($form['minium_price']);

	print render($form['maximum_price']);

	print render($form['market_value']);

	print render($form['market_value_min']);

	print render($form['market_value_line']);

	print render($form['market_value_max']);

	print render($form['arrow-line-max-min']);

}else {
	print render($form['date_create_bid_seller']);

	print render($form['denounce']);

	print render($form['seller_info_text']);

	print render($form['seller_avatar']);

	print render($form['seller_name']);

	print render($form['seller_certification']);

	print render($form['telephone']);

	print render($form['mail_seller']);
}

print render($form['location_display']);

// print render($form['location']);

print render($form['location']);

print render($form['lat']);

print render($form['lng']);

print(render($form['list-matches-bid']));

print render($form['script_ggmap']);

if ($user->uid != $seller_uid) {
	print render($form['distance_buyer_seller']);
} else {
	print render($form['time-expired']);

	print render($form['time-progress']);

	print render($form['started_at']);

	print render($form['expired_at']);

  print render($form['date_now']);

	print render($form['submit']);

	print render($form['close']);

  print render($form['button_back_bid_detail']);
}
print render($form['form_build_id']);
print render($form['form_token']);
print render($form['form_id']);
?>


<!-- Modal Option close -->
<div class="modal fade" id="close-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Encerrar OFERTA</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div><b>Indique os motivos</b></div>
          <div class="form-radios">
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" checked="checked" id="option_1" class="form-radio" value="Realizei a venda no BIDCARROS">
              <label for="option_1">Realizei a venda no BIDCARROS</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_2" class="form-radio" value="Realizei a venda fora do BIDCARROS">
              <label for="option_2">Realizei a venda fora do BIDCARROS</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_3" class="form-radio" value="Desisti da venda">
              <label for="option_3">Desisti da venda</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_4" class="form-radio" value="Não recebi ofertas">
              <label for="option_4">Não recebi ofertas</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_5" class="form-radio" value="Other">
              <label for="option_5">Outro motivo</label>
            </div>
            <div class="col-xs-12">
              <textarea name="textarea_reason" rows="4" cols="70" disabled="disabled"></textarea>
            </div>
          </div>

          <div class="col-xs-12 form-group close-bid">
           <?php print render($form['close_submit']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reason-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Razão</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="reason-text"><b><?php echo $form['reason']['#value']; ?></b></div>

          <div class="col-xs-12 form-group close-reason">
            <button class="btn btn-info form-control btn btn-default form-submit" id="close-reason-form" name="op" value="ENCERRAR" type="button">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
