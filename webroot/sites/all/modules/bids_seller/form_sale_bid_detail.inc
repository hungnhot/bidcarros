<?php
function form_sale_bid_detail($form, &$form_state) {
	global $user;
	$dropdown_options = bids_seller_first_dropdown_options();

	$id = arg(3);

	$bid = get_base_info('tbl_sale_bids', 'id', $id);

	$seller = get_base_info('users', 'uid', $bid->seller_id);

	$fid = extra_field_block_view($seller->uid, 'field_data_field_photo', 'field_photo_fid');

	$photo = file_load($fid);
	$car = get_info('tbl_modelo', 'tbl_marca', 'id', $bid->vehicle_id);

	$buyer_lat = extra_field_block_view($user->uid, 'field_data_field_lat', 'field_lat_value');

	$buyer_lng = extra_field_block_view($user->uid, 'field_data_field_lng', 'field_lng_value');

	$distance = number_format(get_distance($bid->lat, $bid->lng, $buyer_lat, $buyer_lng), 1, ',','');

	$link_logo = get_marca_logo($car->marca_name);

	drupal_add_js(drupal_get_path('module', 'bids_seller') . '/bids_seller.js', array('group' => JS_THEME));

	drupal_add_js(drupal_get_path('module', 'bids_seller') . '/bid_details.js', array('group' => JS_THEME));

	$form['reason'] = array(
		'#type' => 'item',
		'#value' => $bid->close_reason,
		);

	$form['title_bid_detail'] = array(
		'#markup' => '<h2 class="title">OFERTAS/DETALHES</h2>',
		);

	if ($no_logo) {
		$form['logo'] = array(
			'#type' => 'item',
			'#prefix' => "<div class='info-car col-md-12 col-sm-12 col-xs-12'>",
			'#markup' => '<img src="' . $link_logo . '" class="no-logo">',
			'#wrapper_attributes' => array('class' => array('col-md-2 col-sm-2 no-logo')),
			);
	} else {
		$form['logo'] = array(
			'#type' => 'item',
			'#prefix' => "<div class='info-car col-md-12 col-sm-12 col-xs-12'>",
			'#markup' => '<img src="' . $link_logo . '" class="img-responsive full-width">',
			'#wrapper_attributes' => array('class' => array('col-md-2 col-sm-2')),
			);
	}

	$form['car'] = array(
		'#type' => 'item',
		'#title' => t('Marca'),
		'#markup' => "<span>" . $car->marca_name . "</span>",
		'#wrapper_attributes' => array('class' => array('col-md-2 col-sm-2 col-xs-12')),
			// '#description' => 'maior Preço',
		);

	$year = $bid->vehicle_year == "0" || $bid->vehicle_year == ""  ? "Todos" : $bid->vehicle_year;

	$form['year'] = array(
		'#type' => 'item',
		'#title' => t('Ano'),
		'#markup' => "<span>".$year ."</span>",
		'#wrapper_attributes' => array('class' => array('col-md-2 col-sm-2 col-xs-12')),
		);

	$form['km_travell'] = array(
		'#type' => 'item',
		'#title' => t('KM rodados'),
		'#markup' => "<span>".number_format($bid->km_travelled, 0, ',', '.') ."</span>",
		'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3 col-xs-12')),
		);

	$form['minium_price_text'] = array(
		'#type' => 'item',
		'#markup' => "<i class='fa fa-arrow-circle-down fa-2x col-xs-1 col-md-1 col-sm-1'></i><span class='price-prefix col-xs-2 col-sm-2 col-md-2'> R$ </span>
		<span class='price-value col-xs-9 col-sm-9 col-md-9'>" .number_format($bid->initial_sale_value, 0, ',', '.'). "</span>",
		'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3 col-xs-12')),
		'#suffix' => '</div>',
		);
	// Gallery
	$form['gallery'] = array(
		'#type' => 'item',
		'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
		'#markup' => print_gallery_of_sell_bid($id),
		);

	// End gallery
	$form['vehicle'] = array(
		'#type' => 'item',
		'#title' => t('Veiculo'),
		'#markup' => "<span>".$car->nome."</span>",
		'#prefix' => "<div class='detail-car col-md-12 col-sm-12 col-xs-12'><div class='col-md-12 col-sm-12 col-xs-12 first-div-car'>",
		'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-12 col-xs-12')),
		);
	global $user;

	if($user->uid == $seller->uid) {
		$form['registration'] = array(
			'#type' => 'item',
			'#title' => t('Placa do veículo'),
			'#markup' => "<span>".$bid->registration."</span>",
			'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-12 col-xs-12')),
			);
	}

	$form['bid_detail_color'] = array(
		'#type' => 'item',
		'#title' => t('Cor'),
		'#markup' => "<span>".$bid->color."</span>",
		'#suffix' => '</div>',
		'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-12 col-xs-12')),
		);

	// $form['fuel'] = array(
	// 	'#type' => 'item',
	// 	'#title' => t('Combustível'),
	// 	'#markup' => t('<span>Flex (Petrol or Ethanol)</span>'),
	// 	'#prefix' => "<div class='col-md-12 col-sm-12 first-div-car'>",
	// 	'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
	// 	);

	$form['body_type'] = array(
		'#type' => 'item',
		'#title' => t('Carroceria'),
		'#markup' => "<span>".$bid->body_type."</span>",
		'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-6')),
		);

	// $form['gear'] = array(
	// 	'#type' => 'item',
	// 	'#title' => t('Cambio'),
	// 	'#markup' => t('<span>Manual</span>'),
	// 	'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-6')),
	// 	'#suffix' => "</div>",
	// 	);

	$form['purpose'] = array(
		'#type' => 'item',
		'#title' => t('Necessidad'),
		'#markup' => "<span>".$bid->purpose."</span>",
		'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
		);

	$optional_extras_values = array();
	$optional_extras_col = $dropdown_options['optional_extras_col'];
	foreach ($optional_extras_col as $key => $value) {
		if ($bid->$value == 1) {
			$optional_extras_values[] = $key;
		}
	}
	$form['optional_extras'] = array(
		'#type' => 'checkboxes',
    // '#title' => 'Opcionais / Acessórios',
		'#wrapper_attributes' => array('class' => array('group-colour-sheild col-md-12 col-sm-12')),
		'#prefix' => '<div class="col-md-12 col-sm-12"><label for="edit-optional-extras">'.t('Opcionais / Acessórios').'</label></div>',
		'#options' => $dropdown_options['optional_extras'],
		'#options_wrapper_attributes' => $dropdown_options['optional_extras_wrapper_attributes'],
		'#attributes' => array('class' => array('col-md-12 col-sm-12')),
		'#default_value' => $optional_extras_values,
		'#disabled' => TRUE
		);

	$form['observations'] = array(
		'#type' => 'textarea',
		'#title' => t('Observações do vendedor'),
		'#value' => $bid->comment,
		'#disabled' => 'disabled',
		'#size' => 3,
		'#suffix' => '</div>',
		'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
		);

		/**************
		* For Seller
		***************/
		$form['market_values_text'] = array(
			'#prefix' => "<div class='seller-view col-md-12 col-sm-12 col-xs-12'>",
			'#markup' => "<p class='col-md-12 col-sm-12 col-xs-12 label-fieldset'>".t('Valores SELECIONADOS')."</p>",
			);
		if ($bid->expired_at <= date("Y-m-d H:i:s") ) {
			$form['my_offer'] = array(
				'#type' => 'textfield',
				'#title' => t('Minha Oferta'),
				'#default_value' => number_format($bid->initial_sale_value, 0, ',', '.'),
				'#wrapper_attributes' => array('class' => array('col-xs-12 col-sm-3 ')),
				'#element_validate' => array('_validate_price'),
				'#prefix' => '<div class="wrapper-price-max-min col-xs-12 col-md-12 col-sm-12">',
				'#field_prefix' => '<span class="prefix-input">R$|</span>',
				'#maxlength' => 9,
				"#disabled" => true
				);
		} else {
			$form['my_offer'] = array(
				'#type' => 'textfield',
				'#title' => t('Minha Oferta'),
				'#default_value' => number_format($bid->initial_sale_value, 0, ',', '.'),
				'#wrapper_attributes' => array('class' => array('col-xs-12 col-sm-3 ')),
				'#element_validate' => array('_validate_price'),
				'#prefix' => '<div class="wrapper-price-max-min col-xs-12 col-md-12 col-sm-12">',
				'#field_prefix' => '<span class="prefix-input">R$|</span>',
				'#maxlength' => 9,
				);
		}


		$form['text_offers_no_bid'] = array(
			'#markup' => "<span class='col-xs-12 offer-label'>".t('Ofertas no BIDCARROS')."</span>",
			'#prefix' => '<div class="col-xs-12 col-sm-9 col-md-9 wrapper-max-min-value">',
			);
		$related_bids = get_matches_purchase_bids($bid);
		$form['minium_price'] = array(
			'#type' => 'item',
			'#markup' => "<i class='fa fa-arrow-circle-down fa-3x col-sm-2 col-md-2 col-xs-2'></i><div class='col-sm-10 col-md-10 col-xs-10'><span class='price-prefix'>R$</span><span class='price-value'>-</span><span class='title-price-max-min'>".t('menor preço')."</span></div>",
			'#wrapper_attributes' => array('class' => array('col-sm-6 col-md-6 col-xs-12')),
			);

		$form['maximum_price'] = array(
			'#type' => 'item',
			'#markup' => "<i class='fa fa-arrow-circle-up fa-3x col-md-2 col-sm-2 col-xs-2'></i>
			<div class='col-sm-10 col-md-10 col-xs-10 pull-left'><span class='price-prefix'>R$</span>
				<span class='price-value'>-</span>
				<span class='title-price-max-min'>".t('maior preço')."</span></div>",
				'#wrapper_attributes' => array('class' => array('col-xs-12 col-sm-6 col-md-6')),
				'#suffix' => '</div></div>',
				);

		$form['market_value'] = array(
			'#markup' => "<p class='col-md-12 col-sm-12 col-xs-12 label-fieldset text-black'>".t('Valores de mercado')."</p>",
			);

		$form['market_value_min'] = array(
			'#type' => 'item',
			'#markup' => "<span class='price-prefix'>R$  </span>
			<span class='price-value'>" .$price_min. "</span>",
			'#wrapper_attributes' => array('class' => array('col-md-5 col-sm-5 col-xs-12')),
			'#prefix' => "<div class='price-market col-md-10 col-sm-10'>",
			);

		$form['market_value_line'] = array(
			'#markup' => "<span class='col-md-2 col-sm-2 hidden-xs dot-line'></span>",
			);

		$form['market_value_max'] = array(
			'#type' => 'item',
			'#markup' => "<span class='price-prefix'>R$  </span>
			<span class='price-value'>-</span>",
			'#wrapper_attributes' => array('class' => array('col-md-5 col-sm-5 col-xs-12')),
			'#suffix' => '</div></div>',
			);
		$form['arrow-line-max-min'] = array(
			'#markup' => '<div class="line-arrow-max-min col-xs-12">
			<i class="fa fa-arrow-left"></i>
			<span class="line-max-min"></span>
			<i class="fa fa-arrow-right"></i></div>',
			);

		$_SESSION['started_at'] = strtotime($bid->created_at) * 1000;
		$_SESSION['expired_at'] = strtotime($bid->expired_at) * 1000;
		$_SESSION['date_now'] = strtotime(date("Y-m-d H:i:s")) * 1000;

		$form['started_at'] = array(
			'#type' => 'item',
			'#title' => $_SESSION['started_at'],
			);

		$form['expired_at'] = array(
			'#type' => 'item',
			'#title' => $_SESSION['expired_at'],
			);

		$form['date_now'] = array(
			'#type' => 'item',
			'#title' => $_SESSION['date_now'],
			);

		unset($_SESSION['started_at']);
		unset($_SESSION['expired_at']);
		unset($_SESSION['date_now']);

		$form['time-expired'] = array(
			'#type' => 'item',
			'#prefix' => '<div class="form-time col-md-12 col-sm-12 col-xs-12"><div class="time-information col-xs-12"><div class="expired-text col-lg-4 col-md-12 col-sm-12">'.t('Seu BID expira em:').'</div>',
			'#suffix' => '</div>',
			'#markup' => '<div class="day col-xs-3"><span>00</span><span> -</span><span>dias</span></div>
			<div class="hour col-xs-3"><span>00</span><span>:</span><span>hr</span></div>
			<div class="min col-xs-3"><span>00</span><span>:</span><span>min</span></div>
			<div class="sec col-xs-3"><span>00</span><span></span><span>sec</span></div>',
			'#wrapper_attributes' => array('class' => array('col-lg-8 col-md-12 col-sm-12')),
			);

		$form['time-progress'] = array(
			'#type' => 'item',
			'#prefix' => '<div class="progress progress-striped col-md-12 col-sm-12">',
			'#suffix' => '</div><div class="start-at col-md-12 col-sm-12">Cadastro realizado dia ' . date("d/m/Y", strtotime($bid->created_at)) . ' as '. date("H", strtotime($bid->created_at)) . 'h' . date("i", strtotime($bid->created_at)) . '.</div></div>',
			'#wrapper_attributes' => array(
				'class' => array('progress-bar active col-md-12 col-sm-12'),
				'role' => 'progressbar',
				),
			);

		//submit
		// $form['submit'] = array(
		// 	'#type' => 'submit',
		// 	'#value' => t('Atualizar BID') . '<span class="fa fa-angle-right"></span>',
		// 	'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
		// 	'#attributes' => array(
		// 		'class' => array('btn btn-info pull-right full-width'),
		// 		),
		// 	);
		if ( $bid->expired_at <= date("Y-m-d H:i:s")) {
			$form['submit'] = array(
				'#type' => 'submit',
				'#value' => t('Atualizar <b>Oferta</b>') . '<span class="fa fa-angle-right"></span>',
				'#attributes' => array(
					'class' => array('col-md-5 col-sm-5 col-xs-12 btn btn-info pull-left'),
					),
				'#disabled' =>true
				);
		} else {
			$form['submit'] = array(
				'#type' => 'submit',
				'#value' => t('Atualizar <b>Oferta</b>') . '<span class="fa fa-angle-right"></span>',
				'#attributes' => array(
					'class' => array('col-md-5 col-sm-5 col-xs-12 btn btn-info pull-left'),
					),
				);
		}

		$form['close_submit'] = array(
			'#type' => 'submit',
			'#value' => t('ACEITAR'),
			'#attributes' => array(
				'class' => array('btn btn-info form-control'),
				'id'    => array('close-bid'),
				),
			);
		if ($bid->sale_bid_status) {
			if (date("Y-m-d H:i:s") >= $bid->expired_at) {
				$form['close'] = array(
					'#type' => 'button',
					'#attributes' => array(
						'class' => array('col-md-5 col-sm-5 col-xs-12 btn btn-info pull-right'),
						'name' => array('show-close-bid-expired'),
						),
					'#value' => t('ENCERRAR ') . '<b>OFERTA</b>' . '<span class="fa fa-angle-right"></span>',
					);
			} else {
				$form['close'] = array(
					'#type' => 'button',
					'#attributes' => array(
						'class' => array('col-md-5 col-sm-5 col-xs-12 btn btn-info pull-right'),
						'name' => array('show-close-bid'),
						),
					'#value' => t('ENCERRAR ') . '<b>OFERTA</b>' . '<span class="fa fa-angle-right"></span>',
					);
			}
		} else {
			$form['close'] = array(
				'#type' => 'button',
				'#attributes' => array(
					'class' => array('col-md-5 col-sm-5 col-xs-12 btn btn-info pull-right'),
					'name' => array('show-reason'),
					),
				'#value' => t('<b>OFERTA</b> ENCERRADA ') . '<span class="fa fa-angle-right"></span>',
				);
		}

		$form['button_back_bid_detail'] = array(
			'#markup' => "<a id='edit-submit' class='col-md-5 col-sm-5 col-xs-12 btn btn-info pull-left' onclick='window.history.go(-1); return false;' style='margin-top: 0;padding-right: 1.8em;padding-left: 0px;text-align: right;'><span class='fa fa-angle-left' style='position: relative;float: left;left: 5px;padding-left: 10%;'></span>".t('Voltar')."</a>",
			);

		/**************
		*	End For Seller
		***************/

		/**************
		*	For Buyer
		***************/
		$form['date_create_bid_seller'] = array(
			'#type' => 'item',
			'#title' => t('Data do anúncio'),
			'#markup' => "<span>". date('d/m/Y', strtotime($bid->created_at)) . "</span>",
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6 pull-right')),
			'#prefix' => '<div class="col-md-12 col-sm-12">',
			'#suffix' => '</div>',
			);

		$form['denounce'] = array(
			'#type' => 'item',
			'#markup' => "<span>denunciar <i class='fa fa-exclamation-circle'></i></span>",
			'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3 pull-right')),
			'#prefix' => '<div class="col-md-12 col-sm-12">',
			'#suffix' => '</div>',
			);


		$form['seller_info_text'] = array(
			'#markup' => "<p class='col-md-12 col-sm-12 label-fieldset'>Vendedor</p>",
			'#prefix' => '<div class="buyer-view col-md-12 col-sm-12">',
			);

		$form['seller_avatar'] = array(
			'#type' => 'item',
			'#markup' => "<img src='".file_create_url(file_load(extra_field_block_view($seller->uid, 'field_data_field_photo', 'field_photo_fid'))->uri)."' />",
			'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3')),
			);

		$form['seller_name'] = array(
			'#type' => 'item',
			'#title' => t("Nome"),
			// '#markup' => "<span>". extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value'). "</span>",
			'#markup' => "<span>".extra_field_block_view($seller->uid, 'field_data_field_name', 'field_name_value')."</span>",
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
			'#prefix' => "<div class='info-wrapper col-md-9 col-sm-9'><div class='name-certification-wrapper col-md-12 col-sm-12'>",
			);

		$form['seller_certification'] = array(
			'#type' => 'item',
			'#title' => t("Vendedor certificado"),
			'#markup' => '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>',
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
			'#suffix' => '</div>',
			);

		$form['telephone'] = array(
			'#type' => 'item',
			'#title' => t('Telefone'),
			'#markup' => "<span>".extra_field_block_view($seller->uid, 'field_data_field_phone', 'field_phone_value')."</span>",
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
			'#prefix' => "<div class='phone-mail-wrapper col-md-12 col-sm-12'>",
			);

		$form['mail_seller'] = array(
			'#type' => 'item',
			'#title' => t('E-mail'),
			'#markup' => "<span>".$seller->mail."</span>",
			'#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
			'#suffix' => '</div></div></div>',
			);

		/**************
		*	End For Buyer
		***************/
		$form['list-matches-bid'] = array(
			'#type' => 'item',
			'#title' => t('BIDs'),
			'#wrapper_attributes' => array(
				'class' => array('col-md-12 col-sm-12 form-item-step-2 hidden'),
				),
			'#markup' => '<div id="related-bids-wrapper">'
			. print_list_purchase_matches_bid($related_bids)
			. '</div>'
			);

		$form['location_display'] = array(
			'#markup' => '<div class="canvas_map col-md-12 col-sm-12 col-xs-12">
			<div id="map-canvas"></div>
		</div>',
		);
		$form['distance_buyer_seller'] = array(
			'#type' => 'item',
			'#markup' =>  "<img src='" .base_path() . path_to_theme() . '/distance.png' . "' /> <span class='col-xs-8 col-sm-8 col-md-8'>" . $distance . " KM</span>",
			'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 text-center')),
			);

		$form['location'] = array(
			'#type' => 'hidden',
			);
		$form['lat'] = array(
			'#type' => 'hidden',
			'#value' => $bid->lat,
			);
		$form['lng'] = array(
			'#type' => 'hidden',
			'#value' => $bid->lng,
			);

		$form['variable_seller_info'] = array(
			'#type' => 'hidden',
			'#value' => $seller->uid,
			);

		$form['script_ggmap'] = array(
			'#markup' => '<script type="text/javascript"
			src="https://maps.googleapis.com/maps/api/js?sensor=false&&key=AIzaSyBTiT75yZ5OWYHn3w0QEgtqsTgqvWT0Upw&language=en">
		</script>');

		return $form;
	}

	/**
	 * hook submit sale bid detail form
	 */

	function form_sale_bid_detail_submit($form, &$form_state) {
		$id = strval(arg(3));

		if ($form_state['clicked_button']['#id'] == 'edit-submit') {
			$entity = entity_load_single('sale_bid', $id);
			if ($entity !== false) {
				$entity->initial_sale_value = floatval(preg_replace('/[^\d]/', '', $form['my_offer']['#value']));
				if ($entity->save()) {
					send_mail_change_price($entity);
					drupal_set_message("Os valores foram atualizados.");
				} else {
					drupal_set_message("Não é possível atualizar este campo. Por favor tente mais tarde!");
				}
			}

		} elseif ($form_state['clicked_button']['#id'] == 'edit-close-submit') {

			$reason = "";
			if (isset($_POST['option_close'])) {
				if ($_POST['option_close'] == "Other") {
					if ( !(trim($_POST['textarea_reason'])) ){
						form_set_error('empty_reason', t('Please input reason'));
					} else {
						$reason = $_POST['textarea_reason'];
					}
				} else {
					$reason = $_POST['option_close'];
				}

				if (!empty($reason)) {
					$sale_bid_id = arg(3);
					close_sale_bid($sale_bid_id, $reason);
					drupal_goto('user/sale-bids');
				}
			} else {
				form_set_error('empty_reason', t('Please choose reason'));
			}
		}

		// $initial_sale_value = db_update('tbl_sale_bids')

		// ->fields(array(
		// 	'initial_sale_value' => $form['my_offer']['#value'],
		// 	))
		// ->condition('id', $id)
		// ->execute();
	}

	/**
	* validate function price
	**/
	function _validate_price($element, $form, $form_state) {
		$errors = drupal_get_messages();
		form_clear_error();
		if ($form['clicked_button']['#id'] !== 'edit-submit') {
			return;
		}
  $element['#value'] = preg_replace('/[^0-9]/', '', $element['#value']); // Removes special cha
  if(intval($element['#value']) <= 0  || intval($element['#value']) > 9999999) {
  	form_error($element, t('Você deve digitar preço valor correto'));
  }
}

function print_list_purchase_matches_bid($result) {
	$result_buy_bid = $result['result_buy_bid'];
	$result_seguro = $result['result_seguro'];
	$result_price = $result['result_price'];
	$car = $_SESSION['car'];
	ob_start();
	require realpath(dirname(__FILE__)) . '/' . 'table_related_bids.tpl.php';
	$rendered = ob_get_clean();
	return $rendered;
}

function print_gallery_of_sell_bid($sell_bid_id)
{
	$upload_uri = "public://seller_bids/{$sell_bid_id}";
	$upload_path = drupal_realpath($upload_uri);
	$allowed_ext = "{*.jpg,*.JPG,*.png,*.PNG,*.gif,*.GIF}";
	$img_url = file_create_url($upload_uri);
	$result = glob($upload_path . '/' . $allowed_ext, GLOB_BRACE);
	if (count($result) == 0)
		return '';
	ob_start();
	require realpath(dirname(__FILE__)) . '/' . 'gallery_sell_bid.tpl.php';
	$rendered = ob_get_clean();
	return $rendered;
}

function get_base_info($table, $field, $value){
	$query = "SELECT * FROM {{$table}} WHERE {$field} = {$value}";
	$result = db_query($query)->fetchObject();
	return $result;
}

function get_info($table, $table_join, $field, $value){
	$query = "SELECT a.*, b.nome AS marca_name FROM {$table} a LEFT JOIN {$table_join} b ON a.marca = b.id WHERE a.{$field} = '{$value}'";
	$result = db_query($query)->fetchObject();
	return $result;
}
