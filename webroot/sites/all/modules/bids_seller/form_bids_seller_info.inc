<?php
	function form_bids_seller_info($form, &$form_state, $account) {
		global $user;
		// $form['seller_detail'] = array(
		// 	'#type' => 'item',
		// 	'#markup' => "<p class='label-fieldset'>Detalhes do vendedor</p>",
		// 	);

		$user = user_load($user->uid);

  	if (strlen(trim(extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value'))) > 0){
        $user_name = extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value');
    }else{
      $user_name = $user->name;
    }

		$form['username'] = array(
			'#type' => 'textfield',
			'#title' => 'Nome',
			'#disabled' => 'disabled',
			'#default_value' => $user_name,
			'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 col-xs-12')),
			"#prefix" => "<div class='col-xs-12 col-sm-9 col-md-9' id='seler_info-wrapper'>",
			);

		$form['email'] = array(
			'#type' => 'textfield',
			'#title' => 'E-mail',
			'#disabled' => 'disabled',
			'#default_value' => $user->mail,
			'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 col-xs-12')),
			);

		$form['telephone'] = array(
			'#type' => 'textfield',
			'#title' => 'Telefone',
			'#disabled' => 'disabled',
			'#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 col-xs-12')),
			'#suffix' => '</div>',
			'#default_value' => $user->field_phone['und']['0']['value'],
			);

		$form['seller_avatar'] = array(
			'#type' => 'item',
			'#markup' => "<img src='".file_create_url(file_load(extra_field_block_view($user->uid, 'field_data_field_photo', 'field_photo_fid'))->uri)."' />",
			'#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3 col-xs-12')),
			);

		$form['edit'] = array(
			'#type' => 'submit',
			'#button_type' => 'button',
			'#prefix' => '<div class="col-md-4 col-md-offset-4 col-sm-6 col-xs-12">',
			'#value' => t('Editar Detalhe<span class="fa fa-angle-right"></span>'),
			'#submit' => array('custom_submit_for_this_button'),
			'#attributes' =>array('class'=> array('btn btn-info pull-right full-width btn btn-default form-submit')),
			'#suffix' => '</div>',
			);

		$form['submit'] = array(
			'#prefix' => '<div class="search-button col-md-4 col-sm-6 col-xs-12">',
			'#type' => 'submit',
			'#value' => t('Concluir <b>OFERTA</b>') .'<span class="fa fa-angle-right"></span>',
			'#suffix' => '</div>',
			'#attributes' =>array('class'=> array('submit-info btn-info')),
			);

		return $form;
	}

	/**
 * Create a BID
 */
function form_bids_seller_info_submit($form, &$form_state) {
	// var_dump($_SESSION['form-bids-seller']);die;
	global $user, $car, $options_value, $form_values;
	$form_values = isset($_SESSION['form-bids-seller']) ?
		$_SESSION['form-bids-seller'] : NULL;
	if ($form_values === NULL) {
		drupal_set_message("BID já foi salvo", $type = 'warning');
		drupal_goto('search-car');
	}
	// var_dump($form_values);die;
	$timestamp = isset($_SESSION['timestamp']) ? $_SESSION['timestamp'] : NULL;
	// var_dump($_SESSION['car']);die;
	$options_value = bids_seller_first_dropdown_options();
	// Parsing all form data to entity
	$entity = entity_create('sale_bid', array('seller_id' => $user->uid));
	$entity->vehicle_id = $_SESSION['car']->model_id;
	$entity->vehicle_year = $form_values['year'];
	// $entity->body_type = get_option_name_from_value('body_type');
	// $entity->purpose = get_option_name_from_value('purpose');
	// $entity->km_travelled = intval($form_values['km_travell']);
	$entity->km_travelled = floatval(preg_replace('/[^0-9]/', '', $form_values['km_travell']));
	// $entity->patent = $form_values['patent'];
	$entity->vehicle_status = get_option_name_from_value('condition');
	$entity->registration = $form_values['enviroment'];
	// var_dump($form_values['optional_extras']);die;
	$entity->air_conditioning = $form_values['optional_extras'][0] === 0 ? 0 : 1;
	$entity->airbag = $form_values['optional_extras'][1] === 0 ? 0 : 1;
	$entity->power_windows = $form_values['optional_extras'][2] === 0 ? 0 : 1;
	$entity->sunroof = $form_values['optional_extras'][3] === 0 ? 0 : 1;
	$entity->brakes = $form_values['optional_extras'][4] === 0 ? 0 : 1;
	$entity->shield = $form_values['optional_extras'][5] === 0 ? 0 : 1;
	// $entity->shield_type = $form_values['type_of_sheild'];
	// $entity->shield_year = get_option_name_from_value('sheild_age');
  // if (is_array($form_values['color'])) {
  	// $form_values['color'] = array_filter( $form_values['color'] );
    // $entity->color = implode(',', $form_values['color']);
  // }
  $entity->color = $form_values['color'];
	$entity->comment = $form_values['addition_car'];
	$entity->initial_sale_value = floatval(preg_replace('/[^0-9]/', '', $form_values['price']));
	$entity->location = $form_values['location'];
  $entity->lat = $form_values['lat'] != 0 ? $form_values['lat'] : -23.586168;
  $entity->lng = $form_values['lng'] != 0 ? $form_values['lng'] : -46.632378;
	$entity->created_at = date('Y-m-d H:i:s');
	$entity->updated_at = date('Y-m-d H:i:s');
  $date = date_add(date_create(), date_interval_create_from_date_string('30 days'));
	$entity->expired_at = $date->format('Y-m-d H:i:s');
	$entity->sale_bid_status = 1;
	// $scheme = 'public://seller_bids/' . $timestamp;
	$scheme_source = 'public://file_uploads/' . $_SESSION['upload_folder'] . '/';
	$scheme_target = 'public://seller_bids/';
	if ($entity->save()) {
		// $msg = "O lance venda foi criado"; //
		$msg = "Cadastro realizado dia ".date('d/m/Y')." as ".date("H:i").".";
		drupal_set_message($msg, $type = 'status');
		// Photos field
		// Move all files from temporary to seller_bids folder
		$upload_dir = drupal_realpath($scheme_target . '/' . $entity->id);
	  if (!is_dir($upload_dir)) {
	    $contine = mkdir($upload_dir, 0777, true);
	    if ($contine) {
	      $photo_files = explode(",", $form_values['photo_seller']);
				foreach ($photo_files as $key => $photo) {
					rename($scheme_source . $photo, $scheme_target . $entity->id . '/' . $photo);
				}
	    }
	  }
		// rename($scheme ,'public://seller_bids/' . $entity->id );
		// Update matches table
		// see get_related_purchase_bids_by_filter()
    $purchase_bids = variable_get('purchase_bids_matches_list', array());
    // TODO: BID-278
    // var_dump($distance);die;
    // echo "<pre>"; print_r($purchase_bids);
 		foreach ($purchase_bids as $purchase_bid) {
 		  $buyer_lat = extra_field_block_view($purchase_bid->buyer_id, 'field_data_field_lat', 'field_lat_value');
	    $buyer_lng = extra_field_block_view($purchase_bid->buyer_id, 'field_data_field_lng', 'field_lng_value');
	    $distance = get_distance($buyer_lat, $buyer_lng, $entity->lat, $entity->lng);
	    //Debug for tbl match bib
	    // var_dump('buyer_lat + buyer_lng: '. $buyer_lat . ' ' . $buyer_lng. '<br/>');
	    // var_dump('entity_lat + entity_lng: '.$entity->lat. ' '.$entity->lng. '<br/>');
	    // var_dump('distance: ' . $distance. '<br/>');
	    // var_dump('distance_buyer: ' .$purchase_bid->distance. "</br>");
	    if ($distance <= $purchase_bid->distance) {
	      $matches_entity =
        	entity_create('matches_bid', array('sale_bid_id' => $entity->id));
	      $matches_entity->purchase_bid_id = $purchase_bid->id;
	      $matches_entity->save();
	      // var_dump($matches_entity);
	      // TODO Send notification email if needed
	    }
	    //End debug for tbl match bid
	    // die;
		}
		// Clear all Session data
		unset($_SESSION['purchase_bid_matches_list']);
		unset($_SESSION['form-bids-seller']);
		unset($_SESSION['car']);
		// When user creates a BID, next view must be "Meus BIDs" (not the finder).
    // drupal_goto("user/{$user->uid}/sale-bid-detail/{$entity->id}");
		send_mail_relation($entity);
    drupal_goto("user/sale-bids");
	}

}

/*******
* Function to back edit sale bid
*******/
function custom_submit_for_this_button() {
	global $user;
	$_SESSION['flag_seller_info'] = 'seller-info';
	drupal_goto('user/'.$user->uid.'/edit');
}
