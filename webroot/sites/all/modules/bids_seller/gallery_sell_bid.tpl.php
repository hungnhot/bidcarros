<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-bottom:20px; overflow: hidden">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php for ($i=0; $i<count($result); $i++) : ?>
    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>"></li>
  <?php endfor; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php for ($i=0; $i<count($result); $i++) : ?>
    <?php $item = $result[$i]; ?>
    <div style="height:350px; overflow:hidden" class="item<?php echo ($i==0 ? ' active' : ''); ?>">
      <img style="max-width:100%;max-height:100%;width:auto;height:auto;display:block;margin:0 auto" src="<?php echo $img_url .'/'. basename($item); ?>" />
    </div>
    <?php endfor; ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><?php print t('Previous'); ?></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only"><?php print t('Next'); ?></span>
  </a>
</div>
