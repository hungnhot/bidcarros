/**
 * @file
 *  Localization file for Plupload's strings.
 */

// Add translations.
plupload.addI18n({
  "Select files" : Drupal.t("Select files"),
  "Add files to the upload queue and click the start button." : Drupal.t("Add files to the upload queue and click the start button."),
  "Filename" : Drupal.t("Nome Do Arquivo"),
  "Status" : Drupal.t("Estado"),
  "Size" : Drupal.t("Tamanho"),
  "Add files" : Drupal.t("Adicionar arquivos"),
  "Stop current upload" : Drupal.t("Stop current upload"),
  "Start uploading queue" : Drupal.t("Start uploading queue"),
  "Uploaded %d/%d files": Drupal.t("Carregado %d/%d arquivos"),
  "N/A" : Drupal.t("N/A"),
  "Drag files here." : Drupal.t("Drag files here."),
  "File extension error.": Drupal.t("File extension error."),
  "File size error.": Drupal.t("File size error."),
  "Init error.": Drupal.t("Init error."),
  "HTTP Error.": Drupal.t("HTTP Error."),
  "Security error.": Drupal.t("Security error."),
  "Generic error.": Drupal.t("Generic error."),
  "IO error.": Drupal.t("IO error."),
  "Start upload" : Drupal.t("Iniciar o upload"),
  "Stop upload" : Drupal.t("Stop upload"),
  "%d files queued" : Drupal.t("%d files queued"),
  "Drag files here." : Drupal.t("Arraste arquivos aqui."),
  "Start upload" : Drupal.t("Iniciar Carregar"),
  "%d files queued" : Drupal.t("%d files queued"),
  "File: %s" : Drupal.t("File: %s"),
  "Close" : Drupal.t("Close"),
  "Using runtime: " : Drupal.t("Using runtime: "),
  "File: %f, size: %s, max file size: %m" : Drupal.t("File: %f, size: %s, max file size: %m"),
  "Upload element accepts only %d file(s) at a time. Extra files were stripped." : Drupal.t("Upload element accepts only %d file(s) at a time. Extra files were stripped."),
  "Upload URL might be wrong or doesn\"t exist" : Drupal.t("Upload URL might be wrong or doesn\"t exist"),
  "Error: File too large: " : Drupal.t("Error: File too large: "),
  "Error: Invalid file extension: " : Drupal.t("Error: Invalid file extension: "),
  "File count error." : Drupal.t("File count error.")
});
