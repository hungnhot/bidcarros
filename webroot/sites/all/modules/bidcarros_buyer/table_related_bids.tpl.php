<?php global $user; ?>
<?php
  $car_price_min     = is_numeric($car->price_min) ? number_format($car->price_min, 0, ',', '.') : $car->price_min;
  $car_price_max     = is_numeric($car->price_max) ? number_format($car->price_max, 0, ',', '.') : $car->price_max;

  // Filter result by distance
  $check_distance = false;
  $sale_bid_matches_list = array();
  if (isset($_GET['distance'])) {
    // filter by location
    $distance_limit = intval($_GET['distance']);
    $check_distance = ($distance_limit < 100); // 0 is unlimit
    if (user_is_logged_in()) {
      // Get lat, lng of current user in database
      $buyer_lat = extra_field_block_view($user->uid, 'field_data_field_lat', 'field_lat_value');
      $buyer_lng = extra_field_block_view($user->uid, 'field_data_field_lng', 'field_lng_value');
    } else {
      // Get lat, lng of current user by HTML5 Geolocation
      // Or set default to Sao Paulo
      $buyer_lat = isset($_GET['lat']) ? floatval($_GET['lat']) : -23.6132239;
      $buyer_lng = isset($_GET['lng']) ? floatval($_GET['lng']) : -46.6189885;
    }
  }
?>
<div class="container-fluid">
<?php
  $rowCount = 0;
  $price_min = 0;
  $price_max = 0;
?>
<?php
  $num_rows = count($result_sale_bid);
  $counts = 1;
  foreach ($result_seguro as $item) {
    $seguro = $item->seguro;
    if (isset($item->valor_min) && isset($item->valor_max)) {
      if ($item->valor_min == $item->valor_max) {
        $market_min = $item->valor_min * 95/100;
        $market_max = $item->valor_max * 105/100;
      } else {
        $market_min = $item->valor_min;
        $market_max = $item->valor_max;
      }
    } else {
      $market_min = $item->valor * 95/100;
      $market_max = $item->valor * 105/100;
    }
  }
?>
<?php foreach ($result_sale_bid as $item) : ?>
  <?php if ($check_distance) { /** check distance **/
    if (empty($item->lat) || empty($item->lng)) {
      $item->lat = extra_field_block_view($item->seller_id, 'field_data_field_lat', 'field_lat_value');
      $item->lng = extra_field_block_view($item->seller_id, 'field_data_field_lng', 'field_lng_value');
    }
    // get seller lat, lng of seller
    $distance = get_distance($buyer_lat, $buyer_lng, $item->lat, $item->lng);
    if ($distance > $distance_limit) {
      continue;
    } else {
      $sale_bid_matches_list[] = $item;
    }
  } ?>
  <?php
    // calculate Min./Max. price of result items
    if ($price_min == 0 && $price_max == 0) {
      $price_max = $item->initial_sale_value;
      $price_min = $item->initial_sale_value;
    }

    if ($item->initial_sale_value < $price_max && $item->initial_sale_value > 0) {
      $price_min = $item->initial_sale_value;
    }

  ?>
  <?php if (isset($item->seller_id)) { ?>
    <div class="row row-item">
      <div class="col-md-2 col-sm-2 bid-price">
        <i class="fa fa-arrow-circle-down"></i>
        <span class="prefix"><?php print t('R$'); ?></span>
        <span class="ctn"><?php print number_format($item->initial_sale_value, 0, ',', '.'); ?></span>
      </div>
      <div class="col-md-2 col-sm-2 up-and-down">
        <span class="down"><i class="fa fa-arrow-down"></i><?php print t('R$') . $car_price_min; ?> </span>
        <span class="up"><i class="fa fa-arrow-up"></i><?php print t('R$') . $car_price_max; ?> </span>
      </div>
      <div class="col-md-2 col-sm-1">
        <b class="_text-truncate"><?php print ucwords($item->seller_name); ?></b>
      </div>
      <div class="col-md-2 col-sm-2">
        <?php print $item->location; ?>
      </div>
      <div class="col-md-2 col-sm-2">
        <?php print get_date_diff(date('Y-m-d H:i:s'), $item->expired_at); ?>
      </div>
      <div class="col-md-1 col-sm-2 col-xs-8 bid-rate">
        <?php $rank = rand(1,5); ?>
        <?php for ($i=0;$i<$rank;$i++) {
          print '<i class="fa fa-star"></i>';
        } ?>
        <?php for ($i=0;$i<5-$rank;$i++) {
          print '<i class="fa fa-star-o"></i>';
        } ?>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-4 menu-action">
        <a href="<?php print url("user/{$user->uid}/sale-bid-detail/{$item->id}"); ?>">
          <i class="fa fa-ellipsis-v"></i></a>
      </div>
    </div>
    <?php
      $rowCount++;
      $counts++;
    ?>
  <?php } ?>
<?php endforeach; ?>

<?php if ($rowCount == 0) : ?>
  <div class="row">
    <div class="col-md-12">
      <?php
        print t('Nenhum resultado encontrado.');
        $price_max = 0;
        $price_min = 0;
      ?>
    </div>
  </div>
<?php else : ?>
<?php
 // Save matches list to SESSION for using when create Purchase BID successfully
 if ($check_distance) {
  // Re-calculate min - max price
  $min = 0;
  $max = 0;
  foreach ($sale_bid_matches_list as $key => $value) {
    if ($min == 0 && $max == 0) {
      $min = $value;
      $max = $value;
    }
    // if ($value < $max && $value > 0) {
    //   $min = $value;
    // }
  }
  // $price_min = $min;
  // $price_max = $max;
  variable_del('sale_bid_matches_list');
  variable_set('sale_bid_matches_list', $sale_bid_matches_list);
 }
?>
<?php endif; ?>
</div>
<?php
  $seguro = is_numeric($seguro) && $seguro > 0 ? number_format($seguro, 0, ',', '.') : "-";
  $price_min = is_numeric($price_min) && $price_min > 0 ? number_format($price_min, 0, ',', '.') : "-";
  $price_max = is_numeric($price_max) && $price_max > 0 ? number_format($price_max, 0, ',', '.') : "-";
  $market_min = is_numeric($market_min) && $market_min > 0 ? number_format($market_min, 0, ',', '.') : "-";
  $market_max = is_numeric($market_max) && $market_max > 0 ? number_format($market_max, 0, ',', '.') : "-";
?>
<input type="hidden" id="minPrice" value="<?php @print $price_min; ?>" />
<input type="hidden" id="maxPrice" value="<?php @print $price_max; ?>" />
<input type="hidden" id="seguro" value="<?php @print $seguro; ?>" />
<input type="hidden" id="minMarket" value="<?php @print $market_min; ?>" />
<input type="hidden" id="maxMarket" value="<?php @print $market_max; ?>" />
