<?php global $user; ?>
<div class="container-fluid">
  <?php $rowCount = 0; ?>

  <?php
  $sum = 0;
  foreach ($result as $item) {
    if (isset($item->initial_sale_value)) {
      $sum += $item->initial_sale_value;
    }
  }
  $avg = $sum / count($result);
  ?>

  <?php foreach ($result as $item) : ?>
    <div class="row row-item" id="match-bid-<?php print $item->matches_id; ?>">
      <div class="col-md-4 col-sm-4 col-xs-12 bid-price clear-md">
        <?php if ($item->initial_sale_value < $avg) { ?>
        <i class="fa fa-arrow-circle-down"></i>
        <?php } else { ?>
        <i class="fa fa-arrow-circle-up"></i>
        <?php } ?>
        <span class="prefix"><?php print t('R$'); ?></span>
        <span class="ctn"><?php print number_format($item->initial_sale_value, 0, ',', '.'); ?></span>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 bid-name clear-md">
        <?php if(strlen(trim($item->seller_name)) > 0 ) : ?>
          <b><?php print ucwords($item->seller_name); ?></b>
        <?php else : ?>
          <b><?php print ucwords($item->user_name); ?></b>
        <?php endif ?>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 bid-location clear-md">
        <b><?php print ucwords($item->city); ?></b>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 bid-rate">
        <?php $rank = rand(1,5); ?>
        <?php for ($i=0;$i<$rank;$i++) {
          print '<i class="fa fa-star"></i>';
        } ?>
        <?php for ($i=0;$i<5-$rank;$i++) {
          print '<i class="fa fa-star-o"></i>';
        } ?>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-12 menu-action remove-bid">
        <a href="#" data-href="<?php print url("buyer/delete-matches-bid/{$item->matches_id}"); ?>" data-id="<?php print $item->matches_id; ?>" data-toggle="modal" data-target="#confirm-delete">
          <i class="fa fa-trash-o"></i></a>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-12 menu-action detail-bid">
          <a href="<?php print url("user/{$user->uid}/sale-bid-detail/{$item->id}"); ?>">
            <i class="fa fa-ellipsis-v"></i></a>
          </div>
        </div>
        <?php $rowCount++; ?>
      <?php endforeach; ?>
      <?php if ($rowCount == 0) : ?>
        <div class="row">
          <div class="col-md-12">
            <?php print t('Nenhum resultado encontrado.'); ?>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Confirmar exclusão</h4>
          </div>

          <div class="modal-body">
            <p>Você tem certeza?</p>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-danger btn-ok">Excluir</a>
          </div>
        </div>
      </div>
    </div>
    <script>
      jQuery('#confirm-delete').on('show.bs.modal', function(e) {
        var delete_id = jQuery(e.relatedTarget).data('id');
        var delete_url = jQuery(e.relatedTarget).data('href');
        jQuery(this).find('.btn-ok').click(function(e) {
          var request = jQuery.ajax({
            url: delete_url,
          });
          request.done(function(msg) {
            if (msg.success) {
              location.reload();
            }
          });
          jQuery('#confirm-delete').modal('hide');
        });
      });
    </script>