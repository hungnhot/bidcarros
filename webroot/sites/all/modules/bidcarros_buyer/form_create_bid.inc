<?php

function form_create_bid_options()
{
  $options = array();
  // list year
  // for ($i = date('Y'); $i >= 1900 ; $i--)
  // {
  //   $options['years'][$i] = $i;
  // }
  // list of body
  $options['body_types'] = array('Hatchback', 'Minivan', 'Sedan', 'Perua / SW', 'Conversivel', 'Carro de Colecionador', 'Cupe', 'Picape', 'SUV', 'Van / Utilitario', 'Buggy');
  $options['body_types'] = drupal_map_assoc($options['body_types']);
  //list of needed types
  $options['need_types'] = array('Familiar Pequeno', 'Familiar Médio', 'Familiar Grande', 'Off Road', 'Spotivo', 'Carro de Colecionador', 'Urbano', 'Comercial', 'Lazer', 'Adaptado para perssoas com deficiencia');
  $options['need_types'] = drupal_map_assoc($options['need_types']);
  // list of colors
  $options['colors'] = array(
    'Preto' => t('preto'),
    'Prata' => t('prata'),
    'Branco' => t('branco'),
    'Outro' => t('outra cor')
  );
  // attributes mapping with each color item
  $options['colors_wrapper_attributes'] = array(
    'Preto' => array(
      'class' => array('col-md-3 col-sm-3 col-xs-6 item-color-black')
    ),
    'Prata' => array(
      'class' => array('col-md-3 col-sm-3 col-xs-6 item-color-gray')
    ),
    'Branco' => array(
      'class' => array('col-md-3 col-sm-3 col-xs-6 item-color-white')
    ),
    'Outro' => array(
      'class' => array('col-md-3 col-sm-3 col-xs-6 item-color-other')
    ),
  );
  return $options;
}

function form_create_bid($form, &$form_state)
{
  $form['#client_validation'] = TRUE;
  $form['validate_container'] = array(
    '#type' => 'item',
    '#markup' => '<div id="form-create-bid-error" class="alert alert-block alert-danger hidden"><a class="close" data-dismiss="alert" href="#">×</a><ul></ul></div>'
  );
  /*if (isset($_SESSION['form-create-purchase-bid-state']) && user_is_logged_in()) {
    $form_state = array_merge($form_state, $_SESSION['form-create-purchase-bid-state']);
    unset($_SESSION['form-create-purchase-bid-state']);
    $form['#token'] = FALSE;
  }*/
  if (!isset($_SESSION['car']) || $_SESSION['car'] === false) {
    drupal_goto('<front>');
  }
  $car = $_SESSION['car'];
  drupal_add_js(drupal_get_path('module', 'bidcarros_buyer') .
    '/bidcarros_buyer.js', array('group' => JS_THEME));

  $options = form_create_bid_options();
  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    if (isset($_SESSION['form_step'])) {
      $form_state['step'] = intval($_SESSION['form_step']);
      unset($_SESSION['form_step']);
    } else {
      $form_state['step'] = 1;
    }
  }
  $step = $form_state['step'];
  // Show advance filter or not
  if (empty($form_state['show_advance_filter'])) {
    $form_state['show_advance_filter'] = isset($car->select_from_homepage);
  }
  /************ Form Step 1 **************/
  // row 1
  $form['model_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Modelo'),
    '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
    '#field_suffix' => '<i class="fa fa-check-circle fa-2x"></i>',
    '#default_value' => $car->model,
    // '#attributes' => array(
    //   'disabled' => 'disabled'
    // ),
    '#disabled' => TRUE,
  );
  // row 2
  // $form['car'] = array(
  //   '#type' => 'textfield',
  //   '#title' => t('Carro'),
  //   '#wrapper_attributes' => array('class' => array('col-md-4 col-sm-6')),
  //   '#default_value' => $car->car,
  //   '#disabled' => TRUE,
  // );

  $form['make'] = array(
    '#type' => 'textfield',
    '#title' => t('Marca'),
    '#wrapper_attributes' => array('class' => array('col-md-4 col-sm-4')),
    '#attributes' => array(
      'style' => array('background-image: url("'.get_marca_logo($car->make).'")'),
    ),
    '#default_value' => isset($car->make) ? $car->make : '',
    '#disabled' => TRUE,
  );

  $options_ano = array( 0 => '- Todos -');
  $ano_select = $car->years_list;
  foreach ($ano_select as $key => $value) {
    $options_ano[$key] = $value;
  }

  $form['year'] = array(
    '#type' => 'select',
    '#title' => t('Ano'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-4 col-sm-4'),
    ),
    '#options' => isset($car->years_list) ? $options_ano : array(),
    '#default_value' => 0,
    // '#required' => true,
    // '#attributes' => array(
    //   // jQuery validate
    //   'required' => true,
    //   'notEqual' => "0",
    //   'title' => t('Por favor, selecione o ano.')
    // ),
  );

  $form['num_of_seller'] = array(
    '#type' => 'item',
    '#title' => t('Ofertas disponíveis'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-4 col-sm-4'),
    ),
    '#markup' => '<i class="fa fa-map-marker"></i><div class="ctn"></div>',
  );

  /************ Form Step 2 **************/
  if ( ($step > 1 && user_is_logged_in()) || isset($car->select_from_homepage) )
  {
    if ($form_state['show_advance_filter']) {
      drupal_add_js(drupal_get_path('theme', 'bidcarros') .
        '/js/bootstrap-slider.js', array('group' => JS_THEME));

      $default_colors = array('Preto', 'Prata', 'Branco', 'Outro');
      // hidden color for validation
      $form['color_textfield_hidden'] = array(
        '#type' => 'textfield',
        '#wrapper_attributes' => array(
          'class' => array('col-md-12 col-sm-12 form-item-step-2 hidden'),
        ),
        '#validate' => array(
          'required' => array(1),
          'notEqual' => array(0, t('Por favor, selecione pelo menos uma cor.'))
        ),
        "#default_value" => count($default_colors),
      );
      $form['color'] = array(
        '#type' => 'checkboxes',
        '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 form-item-step-2')),
        '#title' => t('Selecione a cor do veículo'),
        '#options' => $options['colors'],
        '#options_wrapper_attributes' => $options['colors_wrapper_attributes'],
        // '#default_value' => end(array_keys($options['colors'])),
        '#required' => TRUE,
        "#default_value" => $default_colors,
      );


      $form['distance'] = array(
        '#type' => 'textfield',
        '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12 form-item-step-2')),
        '#title' => t('Selecione a distância máxima'),
        '#attributes' => array(
          'data-slider-id' => 'distanceSlider',
          'data-slider-handle' => 'square',
          'data-slider-min' => 0,
          'data-slider-max' => 100,
          'data-slider-step' => 5,
          'data-slider-value' => 100
        ),
        '#suffix' =>
          '<div class="col-md-6 col-xs-6">0 Km</div><div class="col-md-6 col-xs-6 text-right">100 Km</div>' .
          '<script>jQuery("#edit-distance").slider({tooltip:"always",formatter: function(value){return value + " Km";}});</script>',
        '#required' => TRUE,
        '#default_value' => 100
      );
    }

    $price_popular = is_numeric($car->price_popular) ? number_format($car->price_popular, 0, ',', '.') : $car->price_popular;
    $price_popular_min = is_numeric($car->price_popular) ? number_format($car->price_popular * 95 / 100, 0, ',', '.') : $car->price_popular;
    $price_popular_max = is_numeric($car->price_popular) ? number_format($car->price_popular * 105 / 100, 0, ',', '.') : $car->price_popular;
    $price_min     = is_numeric($car->price_min) ? number_format($car->price_min, 0, ',', '.') : $car->price_min;
    $price_max     = is_numeric($car->price_max) ? number_format($car->price_max, 0, ',', '.') : $car->price_max;
    $insurance_min = get_seguro_price_of_vehicle($car->model_id)->min_seguro;

    $insurance_min = is_numeric($insurance_min) && $insurance_min > 0 ? number_format($insurance_min, 0, ',', '.') : "-";

    $form['vehicle_price_info'] = array(
      '#type' => 'item',
      '#title' => t('Valores BIDCARROS'),
      '#wrapper_attributes' => array(
        'class' => array('col-md-12 col-sm-12 form-item-step-2'),
      ),
      '#markup' => '<div id="segurar-price" class="col-md-3 col-sm-3 col-xs-12">
                      <div class="min txt col-xs-12 col-sm-5 col-md-5">
                        <span class="prefix  col-sm-2 col-md-2">R$</span>
                        <span class="ctn col-sm-10 col-md-10">'.$price_popular_min.'</span>
                      </div>
                      <div id="line-seperate" class="col-xs-12 col-sm-2 col-md-2 text-center">-</div>
                      <div class="max txt col-xs-12 col-sm-5 col-md-5">
                        <span class="prefix col-sm-2 col-md-2">R$</span>
                        <span class="ctn col-sm-10 col-md-10">'.$price_popular_max.'</span>
                      </div>
                      <div class="txt-desc">'.t('Valores de mercado').'</div>
                    </div>' .
                   '<div class="col-md-3 col-sm-3 col-xs-12" id="price-min">
                      <i class="fa fa-arrow-circle-down col-md-3 col-sm-3"></i>
                      <div class="txt col-sm-12 col-md-9 col-sm-9">
                        <span class="prefix">R$</span>
                        <span class="ctn">'.$price_min.'</span><br/>'
                        .t('menor preço').
                      '</div>
                    </div>' .
                    '<div class="col-md-3 col-sm-3 col-xs-12" id="price-max">
                      <i class="fa fa-arrow-circle-up col-sm-3 col-md-3"></i>
                      <div class="txt col-sm-9 col-md-9">
                        <span class="prefix">R$</span>
                        <span class="ctn">'.$price_max.'</span><br/>'
                        .t('maior preço').
                      '</div>
                    </div>' .
                   '<div class="col-md-3 col-sm-3 col-xs-12" id="insurance-min">
                      <i class="fa lock-icon col-md-3 col-sm-3 col-xs-3"></i>
                      <div class="txt col-sm-9 col-md-9 col-xs-9">
                        <span class="prefix">R$</span>
                        <span class="ctn">'.$insurance_min.'</span><br/>'
                        .t('seguro').
                      '</div>
                    </div>'
    );
    // ******** Disable by BID 181 ***********/
    $form['list-matches-bid'] = array(
      '#type' => 'item',
      '#title' => t('OFERTAS'),
      '#wrapper_attributes' => array(
        'class' => array('col-md-12 col-sm-12 form-item-step-2 hidden'),
      ),
      '#markup' => '<div id="related-bids-wrapper">' . '</div>'
         // . list_related_sell_bids($related_bids, false)
         // . '</div>'
    );
    // ******** end === BID 181 ***********/
  }

  // Show "Back to Search page" button [In Step 1]
  if ($step == 1 || isset($car->select_from_homepage)) {
    global $base_url;
    $form['back-to-search'] = array(
      '#type' => 'button',
      '#prefix' => '<div class="back-to-search col-md-4 col-md-offset-4 col-sm-6 col-xs-12"><a href="'.$base_url.'">',
      '#suffix' => '</a></div>',
      '#value' => '<span class="fa fa-angle-left"></span>' . t('Voltar'),
      '#attributes' => array(
        'class' => array('btn btn-info full-width'),
      )
    );
  }
  // Show "Advance & Simple Filter" button [In Step 2]
  else {
    if ($form_state['show_advance_filter'] === false) {
      // Show button for Show Advance filter
      $form['advance-search'] = array(
        '#type' => 'submit',
        '#prefix' => '<div class="advance-search col-md-4 col-md-offset-4 col-sm-6 col-xs-12">',
        '#suffix' => '</div>',
        '#value' => t('Pesquisa <b>Avancada</b>') . ' <span class="fa fa-angle-right"></span>',
        '#attributes' => array(
          'class' => array('btn btn-primary full-width'),
        )
      );
    }
  }

  $submit_msg = $step == 1 && !isset($car->select_from_homepage)
                ? t('Procurar') : t('Criar <b>BID</b>');
  // Primary submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#prefix' => '<div class="search-button '. ($form_state['show_advance_filter'] && !isset($car->select_from_homepage) ? 'col-md-offset-8 ' : '') .'col-md-4 col-sm-6 col-xs-12 pull-right">',
    '#suffix' => '</div>',
    '#value' => $submit_msg . '<span class="fa fa-angle-right"></span>',
    '#attributes' => array(
      'class' => array('btn btn-info full-width'),
    ),
  );

  return $form;
}

function form_create_bid_submit($form, &$form_state)
{
  $current_step = &$form_state['step'];
  $form_values = $form_state['values'];
  $car = $_SESSION['car'];
  if ($current_step === 1 && !isset($car->select_from_homepage)) // go to next step
  {
    // if (user_is_logged_in()) {
    //   $current_step++;
    // } else {
    //   $bidcarros_settings = array(
    //     'showLoginModal' => true,
    //   );
    //   drupal_add_js(array('bidcarros' => $bidcarros_settings), "setting");
    //   $_SESSION['redirect'] = "buyer/create-bid";
    //   $_SESSION['form-create-purchase-bid-state'] = $form_state;
    //   $_SESSION['form_step'] = $current_step + 1;
    // }
    $current_step++;
    return;
  }
  elseif ($form_state['clicked_button']['#id'] === 'edit-advance-search') {
    $form_state['show_advance_filter'] = true;
  }
  // Preee "Create BID"
  elseif ($form_state['clicked_button']['#id'] === 'edit-submit')
  {
    if (user_is_logged_in()) {
      form_create_bid_save_values($form_values);
    } else {
      $bidcarros_settings = array(
        'showLoginModal' => true,
      );
      drupal_add_js(array('bidcarros' => $bidcarros_settings), "setting");
      $_SESSION['form-create-bid-values'] = $form_values;
      $_SESSION['redirect'] = "buyer/create-bid/save-form";
    }
  }
  $form_state['rebuild'] = TRUE;
  return;
}

// function form_create_bid_validate($form, &$form_state){
//   // echo "<pre>"; print_r($form['year']['#value']);die();
//   if($form['year']['#value'] == '0'){
//     form_set_error('year', t('Por favor, selecione o ano.'));
//   }
// }
function form_create_bid_save_values($form_values) {
  // Create Purchase BID
  global $user;
  $car = $_SESSION['car'];
  $entity = entity_create('purchase_bid', array('buyer_id' => $user->uid));
  $entity->vehicle_id = $car->model_id;
  $entity->vehicle_year = $form_values['year'];
  if (isset($form_values['body_type'])) {
    $entity->body_type = $form_values['body_type'];
  }

  if (isset($form_values['need_type'])) {
    $entity->purpose = $form_values['need_type'];
  }

  if (is_array($form_values['color'])) {
    $form_values['color'] = array_filter( $form_values['color'] );
    $entity->color = implode(',', $form_values['color']);
  }

  if (isset($form_values['distance'])) {
    $entity->distance = $form_values['distance'] < 100 ? $form_values['distance'] : 1000000000;
  }

  $entity->created_at = date('Y-m-d H:i:s');
  $entity->updated_at = date('Y-m-d H:i:s');
  $date = date_add(date_create(), date_interval_create_from_date_string('7 days'));
  $entity->expired_at = $date->format('Y-m-d H:i:s');
  $entity->bid_status = 1;
  // Save to DB
  if ($entity->save()) {
    $msg = t("O lance de compra foi criado");
    drupal_set_message($msg, $type = 'status');
    // Update matches table
    // see get_related_sell_bids()
    $related_bids = variable_get('sale_bid_matches_list', array());
    foreach ($related_bids as $sale_bid) {
      $matches_entity =
        entity_create('matches_bid', array('purchase_bid_id' => $entity->id));
      $matches_entity->sale_bid_id = $sale_bid->id;
      $matches_entity->save();
    }
    // Clear all Session data
    unset($_SESSION['car']);
    unset($_SESSION['form_step']);
    variable_del('sale_bid_matches_list');
    // Go to next page
    drupal_goto("user/purchase-bids");
  }
}
