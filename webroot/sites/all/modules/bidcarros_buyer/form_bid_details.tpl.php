<?php
global $user;
$buyer_id = $form['buyer_id']['#value'];
if($user->uid == $buyer_id) {
  print render($form['started_at']);

  print render($form['expired_at']);

  print render($form['date_now']);

  print render($form['menu_bid']);

  print render($form['logo']);

  print render($form['vehicle']);

  print render($form['year']);

  print render($form['brand']);

  print render($form['time-expired']);

  print render($form['time-progress']);

  print render($form['num_of_seller']);

  print render($form['ctn_label']);

  print render($form['close']);

  print render($form['vehicle_price_info']);

  print render($form['list-matches-bid']);

  print render($form['introduce']);

  print render($form['back_button']);

  print render($form['form_build_id']);

  print render($form['form_id']);

  print render($form['form_token']);
} else {
  drupal_goto('user/'.$user->uid.'/edit');
}
?>

<!-- Modal Option close -->
<div class="modal fade" id="close-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Encerrar BID</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div><b>Indique os motivos</b></div>
          <div class="form-radios">
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" checked="checked" id="option_1" class="form-radio" value="Realizei a compra no BIDCARROS">
              <label for="option_1">Realizei a compra no BIDCARROS</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_2" class="form-radio" value="Realizei a compra fora do BIDCARROS">
              <label for="option_2">Realizei a compra fora do BIDCARROS</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_3" class="form-radio" value="Não quero comprar">
              <label for="option_3">Não quero comprar</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_4" class="form-radio" value="Não existe ofertas">
              <label for="option_4">Não existe ofertas</label>
            </div>
            <div class="col-xs-12 form-type-radio form-item-color form-item radio">
              <input type="radio" name="option_close" id="option_5" class="form-radio" value="Other">
              <label for="option_5">Outro motivo</label>
            </div>
            <div class="col-xs-12">
              <textarea name="textarea_reason" rows="4" cols="70" disabled="disabled"></textarea>
            </div>
          </div>

          <div class="col-xs-12 form-group close-bid">
           <?php print render($form['submit']); ?>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>

<div class="modal fade" id="reason-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Razão</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div id="reason-text"><b><?php echo $form['reason']['#value']; ?></b></div>

          <div class="col-xs-12 form-group close-reason">
            <button class="btn btn-info form-control btn btn-default form-submit" id="close-reason-form" name="op" value="ACEITAR" type="button">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
  <?php
  $modelo = str_replace(' ', '+', trim($form['vehicle']['#markup']));
  ?>
  <iframe src="http://www.bidcarros.com/BidAdvisorModelo.php?query=<?php echo substr($modelo, 12, -7); ?>" frameborder="0" width="100%" height="990"></iframe>
</div>
