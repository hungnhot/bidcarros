var current_lat = 0;
var current_lng = 0;
(function($){
  function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
  }
  function showPosition(position) {
    current_lat = position.coords.latitude;
    current_lng = position.coords.longitude;
    get_related_seller_bids();
  }

  function get_total_related_bids() {
    // $('#edit-submit').attr('disabled', 'disabled');
    var url = baseURL + '/buyer/get-related-bids?';
    $.ajax({
      url: url,
      data: {model_year: $('#edit-year').val()},
    })
    .done(function(response) {
      var numOfBID = $(response).find('div.row-item').length;
      // set status for submit function
      if (numOfBID > 0) {
        $('#edit-submit').removeAttr('disabled');
      }
      // display num-of-bid
      if (numOfBID < 10) {
        numOfBID = "00" + numOfBID;
      } else if (numOfBID < 100) {
        numOfBID = "0" + numOfBID;
      }
      $('#edit-num-of-seller').find('.ctn').text(numOfBID);
    });
  }

  function get_related_seller_bids() {
    var url = baseURL + '/buyer/get-related-bids?';
    var distance = $('#edit-distance').val();
    // var body_type = $('#edit-body-type').val();
    // var purpose = $('#edit-need-type').val();
    var model_year = $('#edit-year').val();
    var colors = $('#edit-color input:checked');
    var color;
    // more filter...
    url += model_year && model_year != 0 ? 'model_year=' + encodeURIComponent(model_year) + '&' : '';
    url += distance ? 'distance=' + distance + '&' : '';
    // url += body_type ? 'body_type=' + encodeURIComponent(body_type) + '&' : '';
    // url += purpose ? 'purpose=' + encodeURIComponent(purpose) + '&' : '';
    if (colors.length > 0) {
      color = new Array(colors.length);
      colors.each(function(index, el) {
        color[index] = $(el).val();
      });
      url += 'color=' + encodeURIComponent(color.join()) + '&';
    }
    if (current_lat != 0 && current_lng != 0) {
      url += 'lat=' + current_lat + '&lng=' + current_lng;
    }
    // done, get ajax callback
    // $('#edit-submit').attr('disabled', 'disabled');
    $('#related-bids-wrapper').load(url, function( response, status, xhr ) {
      if ( status == "success" ) {
        var numOfBID = $(response).find('div.row-item').length;
        if (numOfBID < 10) {
          numOfBID = "00" + numOfBID;
        } else if (numOfBID < 100) {
          numOfBID = "0" + numOfBID;
        }
        $('#num-of-bid').find('.ctn').text(numOfBID);
        $('#edit-num-of-seller').find('.ctn').text(numOfBID);
        // re-set min/max from result
        var insurance_DOM = $(this);
        var minPrice = insurance_DOM.find("#minPrice").attr('value');
        var maxPrice = insurance_DOM.find("#maxPrice").attr('value');
        var insurance = insurance_DOM.find("#seguro").attr('value');
        var marker_min = insurance_DOM.find("#minMarket").attr('value');
        var marker_max = insurance_DOM.find("#maxMarket").attr('value');
        $('#price-min').find('.ctn').text(minPrice);
        $('#price-max').find('.ctn').text(maxPrice);
        $('#insurance-min').find('.ctn').text(insurance);
        $('.min').find('.ctn').text(marker_min);
        $('.max').find('.ctn').text(marker_max);
        //
        if (numOfBID > 0) {
          $('#edit-submit').removeAttr('disabled');
        }
      }
    });
  }

  jQuery(document).ready(function($) {
    option_close_check();

    var started_at = parseInt($('#edit-started-at label').text());
    var expired_at = parseInt($('#edit-expired-at label').text());
    var date_now_server = parseInt($('#edit-date-now label').text());

    countdown_time(started_at, expired_at, date_now_server);

    if (typeof Drupal.settings.bidcarros != 'undefined') {
      if (Drupal.settings.bidcarros.showLoginModal) {
        $('#login-modal').modal('show');
      }
    }
    var $distanceSlider = $('#edit-distance');
    if ($distanceSlider) {
      $distanceSlider.on('slideStop', function(e) {
        get_related_seller_bids();
      });
    }
    $('#edit-color input, select#edit-body-type, select#edit-need-type').change(function(e) {
      get_related_seller_bids();
    });
    $('select#edit-year').change(function(e) {
      if ($('#related-bids-wrapper').length > 0) {
        get_related_seller_bids();
      } else {
        get_total_related_bids();
      }
    });

    $('input[id^="edit-color"]').change(function(event) {
      $('#edit-color-textfield-hidden').val($('input[id^="edit-color"]:checked').length);
    });

    $('div#close-button button[id^="edit-close"]').on('click', function(){
      $('#close-modal').modal('show');
    });

    $('div#reason-button button[id^="edit-close"]').on('click', function(e){
      e.preventDefault();
      $('#reason-modal').modal('show');
    });

    $('button[id="close-reason-form"]').on('click', function(e){
      $('#reason-modal').modal('hide');
    });

    // Auto load ofertas list after page load
    var url = window.location.href;
    if (url.indexOf('buyer/create-bid') > 0
      && $('#related-bids-wrapper').length > 0) {
      get_related_seller_bids();
    }

    // Get HTML5 location if user is not logged in
    if ($('#login-modal').length > 0 && $('#edit-distance').length > 0) {
      getLocation();
    }
  });

  function countdown_time(started_at, expired_at, date_now_server) {

    var days, hours, minutes, seconds;
    // get tag element
    var day = jQuery('div.day span:first-child');
    var hour = jQuery('div.hour span:first-child');
    var min = jQuery('div.min span:first-child');
    var sec = jQuery('div.sec span:first-child');

    progress = jQuery('#edit-time-progress');
    progress.attr('aria-valuemin', started_at);
    progress.attr('aria-valuemax', expired_at);

    var current_date = date_now_server;
    var seconds_left = (expired_at - current_date) / 1000;

    if (expired_at <= current_date) {
      jQuery('div#close-button-expired button#edit-close').attr("disabled", "disabled");
    }

    if (date_now_server < expired_at && seconds_left > 0) {
      var timeId = setInterval(function () {
        current_date += 1000;
        seconds_left = (expired_at - current_date) / 1000;
        var percent = ((current_date - started_at) / (expired_at - started_at)) * 100;

        // do some time calculations
        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;

        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;

        minutes = parseInt(seconds_left / 60);
        seconds = parseInt(seconds_left % 60);

        if (seconds_left <= 0) {
          jQuery('div#close-button button#edit-close').attr("disabled", "disabled");
          clearInterval(timeId);
        }

        day.text(days < 10 ? '0' + days : days);
        hour.text(hours < 10 ? '0' + hours : hours);
        min.text(minutes < 10 ? '0' + minutes : minutes);
        sec.text(seconds < 10 ? '0' + seconds : seconds);

        progress.attr('aria-valuenow', current_date);
        progress.css('width', percent + '%');

      }, 1000);
    }
  }

  function option_close_check(){
    jQuery("#option_5").on('click', function(){
      jQuery('textarea').attr('disabled', false);
    });

    jQuery("#option_1, #option_2, #option_3, #option_4").on('click', function(){
      jQuery('textarea').attr('disabled', true);
    });
  }
})(jQuery);
