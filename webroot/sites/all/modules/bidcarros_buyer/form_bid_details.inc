<?php

function form_bid_details($form, &$form_state) {

  if (!isset($_SESSION['complete_profile_checked'])) {
    drupal_goto('<front>');
  }
  $purchase_id = arg(3);

  $purchase_bid = get_purchase_bid_by_id($purchase_id);
  $car = get_car_info_by_id($purchase_bid->vehicle_id);
  $car->price_popular = get_popular_price_of_vehicle($car->model_id);
  $car->price_min = get_min_price_of_vehicle($car->model_id);
  $car->price_max = get_max_price_of_vehicle($car->model_id);
  $_SESSION['car'] = $car;

  $matches = get_num_matches($purchase_id);
  $min_max = get_min_max_list($purchase_id);
  $list_matches = get_matches_sale_list($purchase_id);

  // echo "<pre>";
  // print_r($purchase_bid);

  $_SESSION['started_at'] = strtotime($purchase_bid->created_at) * 1000;
  $_SESSION['expired_at'] = strtotime($purchase_bid->expired_at) * 1000;
  $_SESSION['date_now'] = strtotime(date("Y-m-d H:i:s")) * 1000;

  drupal_add_js(drupal_get_path('module', 'bidcarros_buyer') .
    '/bidcarros_buyer.js', array('group' => JS_THEME));
  $form['buyer_id'] = array(
    '#type' => 'hidden',
    '#value' => $purchase_bid->buyer_id,
    );

  $form['started_at'] = array(
    '#type' => 'item',
    '#title' => $_SESSION['started_at'],
  );

  $form['expired_at'] = array(
    '#type' => 'item',
    '#title' => $_SESSION['expired_at'],
  );

  $form['date_now'] = array(
    '#type' => 'item',
    '#title' => $_SESSION['date_now'],
  );

  $form['reason'] = array(
    '#type' => 'item',
    '#value' => $purchase_bid->close_reason,
  );

  unset($_SESSION['started_at']);
  unset($_SESSION['expired_at']);
  unset($_SESSION['date_now']);

  $form['menu_bid'] = array(
    '#type' => 'item',
    '#title' => t('Meus BID’s / Detalhes'),
    '#prefix' => '<div class="title-container col-md-12 col-sm-12">',
    '#wrapper_attributes' => array(
      'class' => array('col-md-12 col-sm-12'),
    ),
    '#suffix' => '</div>',
  );

  global $base_url;
  $no_logo = false;
  $logo = drupal_get_path('theme', 'bidcarros') . '/img/Marcas/' . str_replace(' ', '', $car->make) .'.jpg';
  if (!file_exists($logo)) {
    $logo = drupal_get_path('theme', 'bidcarros') . '/img/logo.png';
    $no_logo = true;
  }
  $link_logo = $base_url . '/' . $logo;
  if ($no_logo) {
    $form['logo'] = array(
      '#type' => 'item',
      '#markup' => '<img src="' . $link_logo . '" class="no-logo">',
      '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
      '#prefix' => "<div class='wrapper-first-div col-md-12 col-sm-12'>"
    );
  } else {
    $form['logo'] = array(
      '#type' => 'item',
      '#markup' => '<img src="' . $link_logo . '">',
      '#wrapper_attributes' => array('class' => array('col-md-12 col-sm-12')),
      '#prefix' => "<div class='wrapper-first-div col-md-12 col-sm-12'>"
    );
  }

  $form['vehicle'] = array(
    '#type' => 'item',
    '#title' => t('Veículo'),
    '#markup' => '<br /><span>' .$car->model. '</span>',
    '#wrapper_attributes' => array('class' => array('col-md-6 col-sm-6')),
  );

  $year = $purchase_bid->vehicle_year == "0" || $purchase_bid->vehicle_year == ""  ? "Todos" : $purchase_bid->vehicle_year ;
  $form['year'] = array(
    '#type' => 'item',
    '#title' => t('Ano'),
    '#markup' => '<br /><span>' . $year . '</span>',
    '#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3')),
  );

  $form['brand'] = array(
    '#type' => 'item',
    '#title' => t('Marca'),
    '#markup' => '<br /><span>'.$car->make.'</span>',
    '#wrapper_attributes' => array('class' => array('col-md-3 col-sm-3')),
  );

  $form['time-expired'] = array(
    '#type' => 'item',
    '#prefix' => '<div class="form-time col-md-12 col-sm-12"><div class="time-information col-xs-12"><div class="expired-text col-lg-4 col-md-12 col-sm-12">'.t('Seu BID expira em').':</div>',
    '#suffix' => '</div>',
    '#markup' => '<div class="day col-xs-3"><span>00</span><span> -</span><span>dias</span></div>
                  <div class="hour col-xs-3"><span>00</span><span>:</span><span>hr</span></div>
                  <div class="min col-xs-3"><span>00</span><span>:</span><span>min</span></div>
                  <div class="sec col-xs-3"><span>00</span><span></span><span>sec</span></div>',
    '#wrapper_attributes' => array('class' => array('col-lg-8 col-md-12 col-sm-12')),
  );

  $form['time-progress'] = array(
    '#type' => 'item',
    '#prefix' => '<div class="progress progress-striped col-md-12 col-sm-12">',
    '#suffix' => '</div><div class="start-at col-md-12 col-sm-12">Cadastro realizado dia ' . date("d/m/Y", strtotime($purchase_bid->created_at)) . ' as '. date("H", strtotime($purchase_bid->created_at)) . 'h' . date("i", strtotime($purchase_bid->created_at)) . '.</div></div>',
    '#wrapper_attributes' => array(
      'class' => array('progress-bar active col-md-12 col-sm-12'),
      'role' => 'progressbar',
    ),
  );

  $form['num_of_seller'] = array(
    '#type' => 'item',
    '#prefix' => '<div class="offer-close col-md-12 col-sm-12 col-xs-12"><div class="offer_available col-md-7 col-sm-12 col-xs-12">',
    '#markup' => '<i class="fa fa-map-marker"></i><div class="ctn">' . sprintf("%03d", $matches->number_match) . '</div>',
    '#wrapper_attributes' => array('class' => array('col-md-6 col-sm-12 col-xs-12')),
  );

  $form['ctn_label'] = array(
    '#type' => 'item',
    '#title' => t('Ofertas disponíveis'),
    '#suffix' => '</div>',
    '#wrapper_attributes' => array('class' => array('col-md-6 col-sm-12 col-xs-12')),
  );

  if ($purchase_bid->bid_status) {
    if (date("Y-m-d H:i:s") >= $purchase_bid->expired_at) {
      $form['close'] = array(
        '#type' => 'button',
        '#prefix' => '<div class="close-button col-md-5 col-sm-12 col-xs-12" id="close-button-expired">',
        '#suffix' => '</div></div></div>',
        '#value' => t('ENCERRAR ') . '<b>BID</b>' . '<span class="fa fa-angle-right"></span>',
      );
    } else {
      $form['close'] = array(
        '#type' => 'button',
        '#prefix' => '<div class="close-button col-md-5 col-sm-12 col-xs-12" id="close-button">',
        '#suffix' => '</div></div></div>',
        '#value' => t('ENCERRAR ') . '<b>BID</b>' . '<span class="fa fa-angle-right"></span>',
      );
    }
  } else {
    $form['close'] = array(
      '#type' => 'button',
      '#prefix' => '<div class="close-button show-reason col-md-5 col-sm-12 col-xs-12" id="reason-button">',
      '#suffix' => '</div></div></div>',
      '#value' => t('<b>BID</b> Fechado ') . '<span class="fa fa-angle-right"></span>',
    );
  }

  $form['vehicle_price_info'] = array(
    '#type' => 'item',
    '#title' => t('Valores BID CARROS'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-12 col-sm-12 col-xs-12'),
    ),
    '#markup' => '<div class="min-price col-md-6 col-sm-6">
                    <i class="fa fa-arrow-circle-down"></i>
                    <div class="txt">
                      <span class="prefix">R$</span>
                      <span class="ctn">'.number_format($min_max->min, 0, ',', '.').'</span><br/>'
                      .t('menor preço').'
                    </div>
                  </div>' .
                 '<div class="max-price col-md-6 col-sm-6">
                    <i class="fa fa-arrow-circle-up"></i>
                    <div class="txt">
                      <span class="prefix">R$</span>
                      <span class="ctn">'.number_format($min_max->max, 0, ',', '.').'</span><br/>'
                      .t('maior preço').'
                    </div>
                  </div>'
  );

  // $related_bids = get_related_sell_bids();
  $form['list-matches-bid'] = array(
    '#type' => 'item',
    '#title' => t('OFERTAS'),
    '#prefix' => '<div class="offer-list col-md-12 col-sm-12 col-xs-12">',
    '#wrapper_attributes' => array(
      'class' => array('col-md-12 col-sm-12'),
    ),
    '#markup' => '<div id="related-bids-wrapper">'
       . print_list_matches_bid($list_matches)
       . '</div>',
  );

  $form['introduce'] = array(
    '#type' => 'item',
    '#title' => t('Clique sobre os três pontos para detalhes'),
    '#wrapper_attributes' => array(
      'class' => array('col-md-12 col-sm-12'),
      'style' => array('top:20px')
    ),
    '#suffix' => '</div>',
  );

  $form['back_button'] = array(
    '#type' => 'item',
    // '#markup' => '<a class="button-close" onclick="window.history.go(-1); return false;">Voltar</a>',
    '#markup' => "<a id='edit-submit' class='col-md-5 col-sm-5 col-xs-12 btn btn-info pull-left' onclick='window.history.go(-1); return false;' style='margin: 1em auto;padding-right: 1.8em;padding-left: 0px;text-align: right;font-size: 1.5em; text-transform: uppercase'><span class='fa fa-angle-left' style='position: relative;float: left;left: 5px;padding-left: 10%;'></span>".t('Voltar')."</a>",
  );

  $form['submit'] = array(
    '#type' => 'submit',
      '#value' => t('ACEITAR'),
      '#attributes' => array(
       'class' => array('btn btn-info form-control'),
       'id'    => array('close-bid'),
       ),
    );

  return $form;
}

function print_list_matches_bid($result) {
  $car = $_SESSION['car'];
  ob_start();
  require realpath(dirname(__FILE__)) . '/' . 'table_matches_bid.tpl.php';
  $rendered = ob_get_clean();
  return $rendered;
}
