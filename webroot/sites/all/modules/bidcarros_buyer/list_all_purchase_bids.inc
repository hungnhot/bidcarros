<?php
function list_all_purchase_bids() {
  $item_per_page = 10;
  $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
  $bids_list = get_all_purchase_bids($current_page, $item_per_page, $total);
  $total_page = ceil($total / $item_per_page);
  // pager_default_initialize($total, $limit, $element = 0);
  return theme('list_all_purchase_bids', array(
    'bids_list' => $bids_list,
    'current_page' => $current_page,
    'total_page' => $total_page
  ));
}

function get_all_purchase_bids($current_page = 1, $limit = 10, &$total) {
  global $user;
  $filter = array(
    ':buyer_id' => $user->uid,
    // ':bid_status' => 1,
  );
  $where_condition = "1";
  foreach ($filter as $key => $value)
  {
    $where_condition .= " AND " .substr($key, 1). " = {$key}";
  }
  $query = "SELECT a.* , b.nome AS model_name , COUNT(c.sale_bid_id) AS matches_sale_bids, d.nome as marca
    FROM {tbl_purchase_bids} a
    LEFT JOIN tbl_modelo b ON a.vehicle_id = b.id
    LEFT JOIN {tbl_matches_bid} c ON a.id = c.purchase_bid_id
    LEFT JOIN tbl_marca d ON b.marca = d.id
    WHERE {$where_condition}
    GROUP BY a.id";
  // $result = db_query($query, $filter);
  $start = ($current_page-1) * $limit;
  $count_query = "SELECT COUNT(id) FROM {tbl_purchase_bids} WHERE {$where_condition}";
  $total = db_query($count_query, $filter)->fetchField();

  $query_items = db_query_range($query, $start, $limit, $filter);
  return $query_items->fetchAll();
}
