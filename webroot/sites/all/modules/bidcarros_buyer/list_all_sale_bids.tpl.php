<?php global $user; ?>
<div id="list-all-sale-bids">
  <div class="container-fluid">
    <div class="text-right">
      <strong><?php print t('Clique sobre os três pontos para detalhes'); ?></strong>
    </div>
    <h2 class="title">
      <?php print t('Minhas Ofertas'); ?>
    </h2>
    <div class="row heading">
      <div class="col-md-1 col-sm-1 col-xs-4 bid-thumbnail" style="padding: 0!important">
        &nbsp;<?php print t('Marca'); ?>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-8 bid-title clear-md-right">
        <?php print t('Veiculo'); ?>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-4 bid-price-title" style="padding: 0!important; text-align: left !important; ">
        <?php print t('Ofertas'); ?>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-4 bid-expired" style="text-align: left !important; ">
        <?php print t('Expira'); ?>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-6 up-and-down clear-md-right">
        <?php print t('Preço menor'); ?><br/>
        <?php print t('Preço maior'); ?>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-2">&nbsp;</div>
    </div>
  <?php if (count($bids_list) > 0) : ?>
    <?php foreach ($bids_list as $item) : ?>
    <?php
      $price_min = get_min_price_of_vehicle($item->vehicle_id);
      $price_max = get_max_price_of_vehicle($item->vehicle_id);
      if (is_numeric($price_min)) {
        $price_min = number_format($price_min, 0, ',', '.');
      }
      if (is_numeric($price_max)) {
        $price_max = number_format($price_max, 0, ',', '.');
      }
      $link_logo = get_marca_logo($item->marca);
    ?>
    <div class="row table-heading">
      <div class="col-md-1 col-sm-1 col-xs-12 bid-thumbnail" >
        <img src="<?php echo $link_logo; ?>" class="img-responsive" width="100%" />
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 bid-title clear-md-right" >
        <?php
          $name = substr($item->model_name, 0, 11);
          $name .= ($name != $item->model_name ? '...' : '');
          print "<span title='{$item->model_name}'>{$name}</span>";
        ?>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 bid-price">
        <i class="fa fa-arrow-circle-down"></i>
        <span class="prefix"><?php print t('R$'); ?></span>
        <span class="ctn"><?php print number_format($item->initial_sale_value, 0, ',', '.'); ?></span>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 bid-expired">
        <?php
          print (date("Y-m-d H:i:s") >= $item->expired_at || $item->sale_bid_status == 0) ? "<span>".t('Fechado')."</span>" : get_date_diff(date('Y-m-d H:i:s'), $item->expired_at);
        ?>
      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 up-and-down clear-md-right">
        <span class="down"><i class="fa fa-arrow-down"></i><?php print t('R$') .' '. $price_min; ?> </span>
        <span class="up"><i class="fa fa-arrow-up"></i><?php print t('R$') .' '. $price_max; ?> </span>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-12 menu-action">
        <a href="<?php print url("user/{$user->uid}/sale-bid-detail/{$item->id}"); ?>">
          <i class="fa fa-ellipsis-v"></i></a>
      </div>
    </div>
    <?php endforeach; ?>
    <div class="row no-border paging">
      <div class="col-md-6 visible-md visible-lg"></div>
      <div class="col-md-3 col-xs-6">
        <a href="<?php print '?page='.($current_page-1) ?>" class="btn btn-info full-width prev<?php if ($current_page<=1) print ' disabled'; ?>">
          <span class="fa fa-angle-left"></span>
          <?php print t('VOLTAR'); ?>
        </a>
      </div>
      <div class="col-md-3 col-xs-6">
        <a href="<?php print '?page='.($current_page+1) ?>"class="btn btn-info full-width next<?php if ($current_page>=$total_page) print ' disabled'; ?>">
          <?php print t('PRÓXIMO'); ?>
          <span class="fa fa-angle-right"></span>
        </a>
      </div>
    </div>
  <?php else : ?>
    <div class="row">
      <div class="col-md-12">
        <?php print t('Nenhum resultado encontrado.'); ?>
      </div>
    </div>
  <?php endif; ?>
  </div>
</div>
