<?php global $user; ?>
<div id="list-all-purchase-bids">
  <div class="container-fluid">
    <div class="text-right">
      <strong><?php print t('Clique sobre os três pontos para detalhes'); ?></strong>
    </div>
    <h2 class="title">
      <?php print t("Meus BID’s"); ?>
    </h2>
    <div class="row heading">
      <div class="col-md-1 col-sm-1 bid-thumbnail">
        <?php print t('Marca'); ?>
      </div>
      <div class="col-md-4 col-sm-4 bid-title clear-md-right">
        <?php print t('Veiculo'); ?>
      </div>
      <div class="col-md-2 col-sm-2 related-bid-count">
        <?php print t('Ofertas'); ?>
      </div>
      <div class="col-md-2 col-sm-2 bid-expired">
        <?php print t('Expira'); ?>
      </div>
      <div class="col-md-2 col-sm-2 up-and-down clear-md-right">
        <?php print t('Preço menor'); ?><br/>
        <?php print t('Preço maior'); ?>
      </div>
      <div class="col-md-1 col-sm-1">&nbsp;</div>
    </div>
    <?php if (count($bids_list) > 0) : ?>
      <?php foreach ($bids_list as $item) : ?>
        <?php
        $max_min = get_min_max_list($item->id);
        $price_min = $max_min->min;
        $price_max = $max_min->max;
        if (is_numeric($price_min)) {
          $price_min = number_format($price_min, 0, ',', '.');
        } else {
          $price_min = 'N/A';
        }
        if (is_numeric($price_max)) {
          $price_max = number_format($price_max, 0, ',', '.');
        } else {
          $price_max = 'N/A';
        }

        $link_logo = get_marca_logo($item->marca);

        ?>
        <div class="row">
          <div class="col-md-1 col-sm-1 bid-thumbnail col-xs-12">
            <img src="<?php echo $link_logo; ?>" class="img-responsive" width="100%" />
          </div>
          <div class="col-md-4 col-sm-4 bid-title clear-md-right col-xs-12">
            <?php
              $name = substr($item->model_name, 0, 11);
              $name .= ($name != $item->model_name ? '...' : '');
              print "<span title='{$item->model_name}'>{$name}</span>";
            ?>
          </div>
          <div class="col-md-2 col-sm-2 related-bid-count col-xs-12">
            <i class="fa fa-map-marker"></i>
            <span class="ctn"><?php print sprintf('%03d', $item->matches_sale_bids); ?></span>
          </div>
          <div class="col-md-2 col-sm-2 bid-expired col-xs-12">
            <?php
              print (date("Y-m-d H:i:s") >= $item->expired_at || $item->bid_status == 0) ? "<span>".t('Fechado')."</span>" : get_date_diff(date('Y-m-d H:i:s'), $item->expired_at);
            ?>
          </div>
          <div class="col-md-2 col-sm-2 up-and-down clear-md-right col-xs-12">
            <span class="down"><i class="fa fa-arrow-down"></i><?php print t('R$') .' '. $price_min; ?> </span>
            <span class="up"><i class="fa fa-arrow-up"></i><?php print t('R$') .' '. $price_max; ?> </span>
          </div>
          <div class="col-md-1 col-sm-1 menu-action col-xs-12">
            <?php if($user->uid != $item->buyer_id) {?>
            <a href="<?php print url("user/purchase-bids"); ?>">
            <?php } else { ?>
            <a href="<?php print url("user/{$user->uid}/bid-details/{$item->id}"); ?>">
            <?php } ?>
            <i class="fa fa-ellipsis-v"></i>
            </a>
          </div>
        </div>
      <?php endforeach; ?>
      <div class="row no-border paging">
        <div class="col-md-6 visible-md visible-lg"></div>
        <div class="col-md-3 col-xs-6">
          <a href="<?php print '?page='.($current_page-1) ?>" class="btn btn-info full-width prev<?php if ($current_page<=1) print ' disabled'; ?>">
            <span class="fa fa-angle-left"></span>
            <?php print t('VOLTAR'); ?>
          </a>
        </div>
        <div class="col-md-3 col-xs-6">
          <a href="<?php print '?page='.($current_page+1) ?>"class="btn btn-info full-width next<?php if ($current_page>=$total_page) print ' disabled'; ?>">
            <?php print t('PRÓXIMO'); ?>
            <span class="fa fa-angle-right"></span>
          </a>
        </div>
      </div>
    <?php else : ?>
      <div class="row">
        <div class="col-md-12">
          <?php print t('Nenhum resultado encontrado.'); ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
