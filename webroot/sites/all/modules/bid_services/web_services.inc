<?php
/**
  * Execute Web Service Call
  */


function bid_services_execute_web_services_call() {
  //get base url
  // global $base_url;

  // //get user
  // global $user;

  // // start retrieve POST on the supplied url
  // $url = $base_url.'/web_services';
  // Get the response from client
  if(!isset($_POST['xml_request'])) {
    get_error();
  }else{
    // Start building XML
    $response = $_POST['xml_request'];
    // Make sure the response is understood as XML
    header('Content-Type: text/xml');
    // Send response back

    // get response to DOM
    $dom = new DOMDocument();
    $dom->loadXML($response);
    $dom = $dom->saveXML();
    // Finish XML
    // load element as XML form DOM
    $xml_response = simplexml_load_string($dom);
    // verify_user($xml_response);
    $uid = verify_user($xml_response);
    if($uid){
      import_sale_bid_from_xml($xml_response, $uid);
    }
  }
}

function import_sale_bid_from_xml($xml, $uid){
   /**
  * @param Name : user name to authentication
  * @param Password: password of user name to authentication
  * @var $count : to set how many sale bid was created
  **/
  $count = 0;
  $status_upload = array() ;
  $bid_error = array();
  $message = "";
  foreach($xml->SaleBids->SaleBid as $abs => $attr) {
		$name = $attr->Modelo;
		$year = $attr->Ano;

		// Validate data
		$arr = array();
		$import_car = validate_car_name($name, $year);
		// print_r($import_car);die;
		$valid_value = validate_value(intval($attr->Initial_sale_value),'Initial_sale_value');
		$comment = validate_comment($attr->Comment,'Comment');
		$estado = validate_vehicle($attr->EstadoVeiculo,'EstadoVeiculo');
		$KM = validate_km(intval($attr->KMrodados),'KMrodados');
		$placa = validate_placa($attr->PlacadoVeiculo,'PlacadoVeiculo');

		$air_condition = validate_check($attr->Air_conditioning,'Air_conditioning');
		$airbag =validate_check($attr->Airbag,'Airbag');
		$power = validate_check($attr->Power_windows,'Power_windows');
		$sunroof = validate_check($attr->Sunroof,'Sunroof');
		$brakes = validate_check($attr->Brakes,'Brakes');
		$shield = validate_check($attr->Shield_type,'Shield');
		$color = validate_color($attr->Color,'Color');
		$lat = validate_lat($attr->Lat,'Lat');
		$lng = validate_lng($attr->Lng,'Lng');

		array_push($arr,$valid_value,$comment,$estado,$KM,$placa,$air_condition,$airbag,$power,$sunroof,$brakes,$shield,$color,$lat,$lng);
		$status = check_data_validate($arr);
    // *******************

   	$price_value = intval($attr->Initial_sale_value);

	   if($status && $import_car[0] == true) {
	      $entity = entity_create('sale_bid', array('seller_id' => $uid));
	      $entity->vehicle_id = $import_car[1]->modelo;
	      $entity->vehicle_year = $import_car[1]->name_ano;
	        // $entity->body_type = get_option_name_from_value('body_type');
	        // $entity->purpose = get_option_name_from_value('purpose');
	        // $entity->km_travelled = intval($form_values['km_travell']);
	      $entity->km_travelled = floatval(preg_replace('/[^0-9]/', '', $attr->KMrodados));
	        // $entity->patent = $form_values['patent'];
	      $entity->vehicle_status = $attr->EstadoVeiculo;
	      $entity->registration = $attr->PlacadoVeiculo;
	        // var_dump($form_values['optional_extras']);die;
	      $entity->air_conditioning = $attr->Air_conditioning;
	      $entity->airbag = $attr->Airbag;
	      $entity->power_windows = $attr->Power_windows ;
	      $entity->sunroof = $attr->Sunroof ;
	      $entity->brakes = $attr->Brakes;
	      $entity->shield = $attr->Shield_type;
	      // $entity->shield_type = $form_values['type_of_sheild'];
	      // $entity->shield_year = get_option_name_from_value('sheild_age');
	      // $form_values['color'] = $attr->Color;
	      $entity->color = $attr->Color;
	      $entity->comment = $attr->Comment;
	      $entity->initial_sale_value = $price_value;
	      $entity->location = $attr->Location;
	      $entity->lat = $attr->Lat;
	      $entity->lng = $attr->Lng;
	      $entity->created_at = date('Y-m-d H:i:s');
	      $entity->updated_at = date('Y-m-d H:i:s');
	      $date = date_add(date_create(), date_interval_create_from_date_string('30 days'));
	      $entity->expired_at = $date->format('Y-m-d H:i:s');
	      $entity->sale_bid_status = 1;

     		if ($entity->save()) {
	      	$temp = get_image($entity->id,$attr->Images);
	        array_push($status_upload ,$temp);

	        if(count($temp[1]) == $temp[0] || $temp[0] == 0){
	        		$count++;
	        		array_push($bid_error,$entity->id);
	        }else{
	        		$count ++;
	        }
	     }
	  }elseif ($status == false){
  			$key = return_data_invalid($arr);
	     $message .= respone_format_xml(FALSE,get_error_element($arr[$key][1],$arr[$key][2]));
	  }elseif ($import_car[0] == false){
	  		$message .= respone_format_xml(FALSE,get_error_element($import_car[1],$import_car[2]));
	  }
  }
  if($count > 0) {
  		if(count($bid_error) > 0){
  			remove_bids_error($bid_error);
  		}
		$status = 0;
		$mes ="";
  		foreach ($status_upload as $key => $value) {
  			# code...
  			if(count($value[1]) == 0 && $value[0] > 0){
  				$status = TRUE;
  				$mes .= get_upload_success();

	  		}elseif(count($value[1]) ==  $value[0] || $value[0] == 0){
  				$status = FALSE;
  				$mes .= get_error_element("Images",$value[1][0]);

	  		}else{
	  			$img ="";
	    		foreach ($value[1] as $key => $value2) {
	    			$img .= get_error_element("img",$value2);
	    		}
	    		$img .= get_upload_success();

	    		$status = TRUE;
  				$mes .= $img;

	  		}
  		}
  		$message .= respone_format_xml($status,$mes);
  }
  return_xml($message);
}

function check_data_validate($arr){
	foreach ($arr as $key => $value) {
		# code...
		if($value != 1){
			return false;
		}
	}
	return true;
}

function return_data_invalid ($arr){
	foreach ($arr as $key => $value) {
		# code...
		if($value != 1){
			return $key;
		}
	}
}

function get_image($bid,$xml){
  // print_r($bid);

  $json = json_encode($xml->Img);
  $array_image = json_decode($json,TRUE);

  $image_count = 0;
  $value_img_error = array();

  $scheme_target = 'public://seller_bids/'.$bid;
  $upload_dir = drupal_realpath($scheme_target);
  if (!is_dir($upload_dir)) {
    	$folder_upload = mkdir($scheme_target, 0777, true);
    	if($folder_upload){
	     foreach ($array_image as $key=>$value) {
	     		$image_count ++;
	        if($key <= 4 && getimagesize($value)){
		        grab_image($bid,$key,$value,$scheme_target);
	        }else{
	        		array_push($value_img_error,$value);
	        }
	     }
    	}
  }
  return array($image_count,$value_img_error);
}

function grab_image($bid,$key,$url_image,$path){
	$userImage = $bid."_".$key.".png";
	$ch = curl_init($url_image);

	$fp = fopen($path.'/'.$userImage, 'wb');

	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	$result = curl_exec($ch);
	curl_close($ch);
	fclose($fp);
}

function validate_lat($val,$name){
	preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $val,$matches);
	// print_r($matches);die;
	if($matches){
		return true;
	}else{
		return array(false,$name,$val);
	}
}

function validate_lng($val,$name){
	preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $val,$matches);
	// print_r($matches);die;
	if($matches){
		return true;
	}else{
		return array(false,$name,$val);
	}
}
function  validate_color ($values,$name){
	$trim = trim($values);
	$text = strtolower($trim);
	// $array = explode(",", $text );
	// foreach ($array as $key => $value) {
		# code...
		if($text && ($text == "preto" || $text == "prata" || $text == "branco" || $text == "outro")){
			return true;
		}else{
			return array(false,$name,$values);
		}
	// }
}
function validate_check($value,$name){
	
	$values = intval($value);
	if($values == 1 || $values == 0){
		return true;
	}else{
		return array(false,$name,$values);
	}
}

function validate_placa($value,$name){
	$trim = trim($value);
	preg_match('/(?P<name>\w+)-(?P<digit>\d+)/', $trim, $matches);
	if($matches && is_string($matches[1]) && strlen((string)$matches[1]) == 3 && is_numeric($matches[2]) &&  strlen((string)$matches[2]) ==4 ){
		return true;
	}else{
		return array(false,$name,$value);
	}
}

function validate_km($value,$name){
	if( is_numeric($value) && strlen((string)$value) <= 7){
	    return true;
  }else{
    return array(false,$name,$value);
  }
}

function validate_value($value,$name){
  if($value && is_numeric($value) && strlen((string)$value) <= 7){
    return true;
  }else{
    return array(false,$name,$value);
  }
}

function validate_vehicle($value,$name){
	$trim = trim($value);
	$text = strtolower($trim);
	$status = ucfirst($text);
	if ($status == "Novo" || $status == "Usado"){
		return true;
	}else{
		return array(false,$name,$value);
	}
}

function verify_user($xml){
  $mail = $xml->Mail;
  // $name = $xml->Name;
  $password = (string)$xml->Password;
  $account = find_user_object($mail);
  $message = "";
   if(!empty($account)){
    $uid= user_authenticate($account[0]->name,$password);
	   if(!empty($uid)){
	      return $uid;
	   }else{
	    	 $message .= respone_format_xml(FALSE,get_error_element('Mail or Password', 'incorrect',0));
	   }
  }else{
   	$message .= respone_format_xml(FALSE,get_error_element('Mail','no present',0));
  }
  return_xml($message);
}

function get_upload_success($count) {
  $count = $count;
  $message = "<Message> Cadastro realizado dia " .date('d/m/Y')." as ".date("H:i")."</Message>";
  return $message;
}

function get_error(){
  $response = "<Error>Wrong operation argument</Error>";
  return $response;
}

function get_error_element($element,$content){
  $response = " <Notice> [".$element."]  => ".$content ."</Notice>";
  return $response;
}

function respone_format_xml($status,$content){
	if ($status){
		$status = "Success";
	}else{
		$status = "Failed";
	}
  $response = "<Status>".$status."</Status> ".$content;

  	return $response;
}

function return_xml($message){
	header('Content-Type: text/xml');
	$response = "<?xml version='1.0' encoding='UTF-8'?><XmlResponse>$message</XmlResponse>";
  	echo $response;
}

function validate_car_name($name, $year){
	$trim = trim($year);
	$text = strtolower($trim);
	if (strlen($text) > 0){
		$data = $text."%";
	}else{
		return array(FALSE,'Ano',$year);
	}
  $query1 ="SELECT n.id  AS id_modelo ,n.nome, a.id, a.nome AS name_ano,a.modelo
                FROM tbl_modelo n
                INNER JOIN tbl_ano_modelo a ON n.id = a.modelo
                WHERE n.nome = :name";
  $result1 =  db_query($query1,array(':name' => $name))->fetchObject();
  if ($result1){
  		$query2 = "SELECT * FROM($query1) b  WHERE b.name_ano like :year" ;
     	$result2 = db_query($query2,array(':name' => $name,':year' => $data))->fetchObject();   
     	if($result2){
     		return array(TRUE,$result2);
     	}else{
     		return array(FALSE,'Ano',$year);
     	}     
  }else{
  		return array(FALSE,'Modelo',$name);
  }
}

function validate_comment($comment){
  if(strlen($comment) > 200){
    return false;
  }else{
    return true;
  }
}

function find_user_object($mail){
  $query = "SELECT * FROM {users} u LEFT JOIN {users_roles} r on u.uid = r.uid WHERE u.mail = :email and r.rid = 5 OR r.rid = 1";
  $result = db_query($query,array(':email' => $mail))->fetchAll();
  return $result;
}

function remove_bids_error($array){
	$newarray = implode(",", $array);
	$query ="DELETE FROM {tbl_sale_bids} WHERE id IN ({$newarray})";
	db_query($query);
}
