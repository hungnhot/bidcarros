<?php

function form_search_car($form, &$form_state) {
  drupal_add_js(drupal_get_path('module', 'bidcarros_search') . '/bidcarros_search.js', array('group' => JS_THEME));
  $form['keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('QUAL CARRO OU MOTO ESTÁ VENDENDO'),
    '#autocomplete_path' => 'search-car/autocomplete',
    '#attributes' => array('class' => array('auto_submit')),
    '#theme_wrappers' => array('bidcarros_search_textfield'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#class' => 'hidden',
  );
  return $form;
}

function _bidcarros_search_theme() {
  return array(
    'bidcarros_search_textfield' => array(
      'render element' => 'element',
      'file' => 'form_search_car.inc',
    ),
  );
}

function theme_bidcarros_search_textfield($variables)
{
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
  ));
  // _form_set_class($element, array('form-text'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $attributes['title'][] = 'autocomplete';
    $output = '<div class="input-group">' . $output . '<span class="input-group-addon">' . _bootstrap_icon('search') . '</span></div>';
    if ($element['#title']) {
      $output = '<label for="' . $element['#attributes']['id'] . '">' . $element['#title'] . '</label>' . $output;
    }
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  return $output . $extra;
}

function form_search_car_validate($form, &$form_state) {
  // validate
  $keyword = $form_state['values']['keyword'];
  if (empty($keyword)) {
    form_set_error('keyword', t('Por favor, verifique a sua entrada.'));
  }
}

function form_search_car_submit($form, &$form_state) {
  $keyword = $form_state['values']['keyword'];
  $car = get_car_info_by_name($keyword);
  if ($car === false) {
    form_set_error('keyword', t('Nenhum resultado encontrado.'));
  } else {
    $car->years_list = get_model_years_list($car->model_id);
    if (count($car->years_list) > 0) {
      $car->model_year = array_values($car->years_list)[0];;
    }
    $car->price_popular = get_popular_price_of_vehicle($car->model_id, $car->model_year);
    $car->price_min = get_min_price_of_vehicle($car->model_id);
    $car->price_max = get_max_price_of_vehicle($car->model_id);
    $_SESSION['car'] = $car;
    // var_dump($car);die;
    // check user is buyer or seller
    // see bidcarros_search_preprocess_page()
    if (is_search_car_page($page_type)) {
      switch ($page_type) {
        case 'seller':
          drupal_goto("seller/create-bid"); //old page: bids-seller
          break;

        default: // is buyer
          drupal_goto("buyer/create-bid");
          break;
      }
    }
    // end
  }
}
