<?php include 'templates/header.tpl.php' ?>
<?php //<!-- NGOCLB ?>
<div id="search-car" class="full-width bg-transparent">
  <div class="container">
    <div class="col-md-12 text-center">
      <h2><?php print t('Escolha seu carro + Crie seu BID = Faça o melhor negócio'); ?></h2>
      <hr class="hr" />
    </div>
  </div>
  <div class="full-width bg-overlay">
    <div class="container">
      <div class="col-md-12">
        <?php
        $search_results_html = <<<EOF
          <div class="row wrapper">
            <div class="col-md-3 price-with-icon" id="num_of_offer">
              <i class="fa fa-map-marker"></i><span class="ctn"></span>
            </div>
            <div class="col-md-3 price-with-icon" id="min_offer_value">
              <i class="fa fa-arrow-circle-down"></i><span class="ctn"></span>
            </div>
            <div class="col-md-3 price-with-icon" id="max_offer_value">
              <i class="fa fa-arrow-circle-up"></i><span class="ctn"></span>
            </div>
            <div class="col-md-3 price-with-icon" id="avg_seguro_value">
              <i class="fa fa-map-marker lock-icon"></i>
              <span class="ctn"></span>
              <i class="fa fa-caret-up"></i>
            </div>
          </div>
EOF;
          module_load_include('inc', 'bidcarros_search', 'form_search_car');
          $form = drupal_get_form('form_search_car');
          $form['keyword']['#title'] = t('QUAL CARRO OU MOTO ESTÁ PROCURANDO');
          $form['search_results'] = array(
            '#type' => 'item',
            '#wrapper_attributes' => array(
              'id' => array('search-results'),
            ),
            '#markup' => $search_results_html
          );
          print drupal_render($form);
        ?>
      </div>
    </div>
  </div>
</div>
<div class="facebook-share-wrapper">
  <!-- Facebook Javascript SDK -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=435304826489657";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <!-- Facebook Share Button -->
  <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div>
</div>
<?php //NGOCLB --> ?>
<div id="content" class="content">
  <!-- Tainb area coding -->
  <div class="container-fluid full-width no-padding">
    <div id="cataloge-carros-wrapper" class="col-xs-12 col-sm-12 col-md-12">
      <div class="cataloge-introduce col-xs-12 col-sm-4 col-md-4">
        <h2>BIDADVISOR</h2>
        <span>
          Lançamentos, notícias, comparativos e avaliações, tudo o que você quer saber sobre carros e motos
        </span>
      </div>
      <div class="cataloge-slideshow col-xs-12 col-sm-4 col-md-4">
        <div class="line-triangles_dark"></div>
        <h3 class="">BID ADVISOR</h3>
        <hr class="hr"></hr>
        <p>Reviews</p>
        <div class="iframe_wrapper">
          <iframe src="http://www.bidcarros.com/BidAdvisor.php" width="100%" height="300" scrolling="yes" frameBorder="0"></iframe>
        </div>
      </div>
    </div>
  </div>
  <!-- End Tainb area coding -->
  <div id="total-bid-section" class="full-width bg-navy">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 text-center">
          <h1 class="title">
            <?php
            $total_bid = get_total_bids();
            print $total_bid < 1000
            ? str_pad($total_bid, 3, '0', STR_PAD_LEFT)
            : number_format($total_bid, 0, ',', '.'); ?>
          </h1>
          <p><?php print t('bids cadastrados'); ?></p>
          <hr class="hr" />
        </div>
      </div>
    </div>
  </div>
  <!-- Tainb area coding  -->
  <div id="introductory-area" class="col-xs-12 col-sm-12 col-md-12">
    <div class="image-mobile-device-wrapper col-xs-12 col-sm-6 col-md-6">
      <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/smartphone.png' . "' class='img-responsive' /> " ?>
    </div>
    <div class="introduction-text-wrapper col-xs-12 col-sm-6 col-md-5">
      <h2>SOBRE O BID CARROS</h2>
      <p>Você deseja ‘aquele’ carro. Sonha com ele. Já sabe o modelo, a cor, o ano, e até os opcionais que são ‘obrigatórios’.</p>
      <p>Procura em diferentes sites, jornais, lojas e concessionárias, mas quando encontra não é o valor que gostaria de pagar. Ou não é exatamente o que quer. Para finalizar a compra, então, fica ainda mais difícil. Como negociar?</p>
      <p>Será que você vai pagar o valor mais justo? E se negociar um pouco mais?</p>
      <p>O BidCarros veio para mudar estas regras e facilitar a vida de quem compra e também de quem vende.</p>
      <p>Afinal, a melhor negociação é aquela que é boa para todos.</p>
    </div>
  </div>

  <div id="device-mobile" class="col-xs-12 col-sm-12 col-md-12">
    <div class="line-2-triangles-green"></div>
    <div class="computing-image col-xs-12 col-sm-3 -col-md-3">
      <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/icon_01.png' . "' /> " ?>
    </div>

    <div class="qrcode-wrapper col-xs-12 col-sm-6 -col-md-6">
      <!-- <p>Baixe o BidCarros no seu iPhone ou Android e comece a usar ja.<br/>Acesse esta pagina do seu smartphone ou use o QR Code.</p> -->
      <p>Em Breve<br/>Escolha seu carro + Crie seu BID = Faça o melhor negócio com o nosso App.</p>
      <div class="iphone-wrapper col-xs-12 col-md-6">
        <!-- <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/QR_code_01.png' . "' /> " ?> -->
        <?php echo "<img class='icon-app-mobile' src='" .  base_path() .  path_to_theme() . '/img/app_store_icon.png' . "' /> " ?>
      </div>
      <div class="android-wrapper col-xs-12 col-md-6">
        <!-- <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/QR_code_02.png' . "' /> " ?> -->
        <?php echo "<img class='icon-app-mobile' src='" .  base_path() .  path_to_theme() . '/img/google_play_icon.png' . "' /> " ?>
      </div>
    </div>

    <div class="mobile-touch col-xs-12 col-sm-3 col-md-3">
      <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/icon_02.png' . "' /> " ?>
    </div>

    <div class="logo-partner col-xs-12 col-sm-12">
      <?php echo "<img src='" .  base_path() .  path_to_theme() . '/img/4_logos.png' . "' />" ?>
      <ul>
        <li class="col-xs-1  col-sm-1 col-md-1"></li>
        <li class="col-xs-4  col-sm-4 col-md-4"><a href="http://www.estadao.com.br/jornal-do-carro" target="_blank"></a></li>
        <li class="col-xs-1  col-sm-1 col-md-1"></li>
        <li class="col-xs-2  col-sm-2 col-md-2"><a href="http://jovempanfm.bol.uol.com.br" target="_blank"></a></li>
        <li class="col-xs-1  col-sm-1 col-md-1"></li>
        <li class="col-xs-3  col-sm-3 col-md-3"><a href="https://www.segurar.com" target="_blank"></a></li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php print drupal_get_path('theme', 'bidcarros'); ?>/js/homepage.js"></script>
<?php include 'templates/footer.tpl.php' ?>
