<?php

/**
 * @file
 * template.php
 */
// clear all cache - enable when needed
// drupal_flush_all_caches();

function bidcarros_preprocess_page(&$variables) {

  if (drupal_is_front_page()) {
    unset($_SESSION['redirect']);
  }
  // Footer nav
  // $variables['footer_nav'] = menu_tree(variable_get('menu_main_links_source', 'menu-footer'));
  // $variables['footer_nav']['#theme_wrappers'] = array('menu_tree__default');

  // To see available templates for this page simply do the following in your themes template.php
  // echo '<pre>'; var_dump($variables['theme_hook_suggestions']); echo '</pre>';
}

function bidcarros_menu_tree__default(&$variables) {
  return '<ul class="navbar navbar-default">' . $variables['tree'] . '</ul>';
}

function bidcarros_js_alter(&$javascript) {
  // Remove un-used JS file in homepage
  if (drupal_is_front_page()) {
    // unset($javascript[drupal_get_path('module', 'bidcarros_search').'/bidcarros_search.js']);
    unset($javascript[drupal_get_path('theme', 'bidcarros').'/js/bid_ggmap.js']);
    unset($javascript[drupal_get_path('theme', 'bidcarros').'/js/load_state_cities.js']);
  }
}

function bidcarros_theme()
{
  $items = array();
  // create custom user-login.tpl.php
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bidcarros') . '/templates',
    'template' => 'user_login',
    'preprocess functions' => array(
      'bidcarros_preprocess_user_login'
      ),
    );

  $items['user_profile_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bidcarros') . '/templates',
    'template' => 'user-profile-edit',
    'preprocess functions' => array(
      'bidcarros_preprocess_user_profile_edit'
      ),
    );

  $items['user_register_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bidcarros') . '/templates',
    'template' => 'user_register',
    );

  $items['user_pass'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bidcarros') . '/templates',
    'template' => 'reset_password',
    );

  $items['contact_site_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'bidcarros') . '/templates',
    'template' => 'contact_site',
    );

  return $items;
}

function bidcarros_preprocess_user_login(&$variables) {
  $variables['form_title'] = t('Fazer login');
  $variables['rendered'] = drupal_render_children($variables['form']);
}

function bidcarros_preprocess_user_profile_edit(&$variables) {
  $variables['rendered'] = drupal_render_children($variables['form']);
}

// using hook_form_alter to customize form field

/**
 * Customize User menu
 */
function user_menu_alter(&$items)
{
  // My account page
  $items['user/%user']['page callback'] = 'bidcarros_user_view_page';
  return $items;
}

function bidcarros_form_user_profile_form_alter($form, &$form_state) {
  // Redirect to other URL if exist session
  if (isset($_SESSION['redirect']) && user_is_logged_in()) {
    $redirect_to = $_SESSION['redirect'];
    // clear flag when redirect
    unset($_SESSION['redirect']);
    drupal_goto($redirect_to);
  }
}

function bidcarros_user_view_page()
{
  // Check Url to redirect when user logged in
  global $user;
  if (isset($_SESSION['redirect'])) {
    $redirect_to = $_SESSION['redirect'];
    // clear flag when redirect
    unset($_SESSION['redirect']);
    drupal_goto($redirect_to);
  }
  else {
    drupal_goto("user/{$user->uid}/edit");
  }

}

function bidcarros_preprocess_user_register_form(&$variables)
{
  $variables['rendered'] = drupal_render_children($variables['form']);
}

function bidcarros_preprocess_user_pass(&$variables)
{
  $variables['rendered'] = drupal_render_children($variables['form']);
}

function bidcarros_form_contact_site_form_alter(&$form, &$form_state) {
  drupal_set_title('FALE CONOSCO');
}

function bidcarros_form_alter(&$form, &$form_state, $form_id) {
  $cep_max_length = 9;
  $cpf_cpnj_max_length = 18;
  $phone_max_length = 15;

  if ($form['#id'] == 'user-register-form') {
    // Set Field's Title
    $form['field_name']['und']['0']['value']['#title'] = t('Nome');
    $form['field_photo']['und']['0']['#title'] = t('Subir foto de perfil');
    $form['field_cpf_cpnj']['und']['0']['value']['#title'] = t('CPF/CPNJ');
    $form['field_cpf_cpnj']['und']['0']['value']['#maxlength'] = $cpf_cpnj_max_length;
    $form['field_cep']['und']['0']['value']['#title'] = t('CEP');
    $form['field_cep']['und']['0']['value']['#maxlength'] = $cep_max_length;
    $form['field_address']['und']['0']['value']['#title'] = t('Endereço');
    $form['field_number']['und']['0']['value']['#title'] = t('Número');
    $form['field_complement']['und']['0']['value']['#title'] = t('Complemento');
    $form['field_state']['und']['#title'] = t('Estado');
    $form['field_city']['und']['#title'] = t('Cidade');
    $form['field_phone']['und']['0']['value']['#title'] = t('Telefone');
    $form['field_phone']['und']['0']['value']['#maxlength'] = $phone_max_length;
    $form['actions']['submit']['#attributes'] = array(
      'class' => array('btn btn-info full-width btn btn-success')
      );
    $form['actions']['submit']['#value'] = t('Prosseguir') . "<span class='fa fa-angle-right'></span>";
    $form['#validate'][] = 'register_validate';
    $form['#submit'][] = 'register_submit';

  } elseif ($form['#id'] == 'user-profile-form') {
    // Hide contact form field
    $form['contact']['#access'] = FALSE;

    $form['account']['mail']['#title'] = 'E-mail';
    $form['field_cpf_cpnj']['und']['0']['value']['#maxlength'] = $cpf_cpnj_max_length;
    $form['field_cep']['und']['0']['value']['#maxlength'] = $cep_max_length;
    $form['field_phone']['und']['0']['value']['#maxlength'] = $phone_max_length;
    $form['#validate'][] = 'profile_validate';
    $form['#submit'][] = 'profile_submit';

  } elseif ($form['#id'] == 'user-pass') {
    $form['name']['#title'] = t('Seu email');
    $form['actions']['submit']['#value'] = t('ENVIAR') . "<span class='fa fa-angle-right'></span>";
    $form['#submit'][] = 'reset_password_submit';

  } elseif ($form['#id'] == 'complete-profile-form') {
    $form['field_cpf_cpnj']['und']['0']['value']['#maxlength'] = $cpf_cpnj_max_length;
    $form['field_cep']['und']['0']['value']['#maxlength'] = $cep_max_length;
    $form['field_phone']['und']['0']['value']['maxlength'] = $phone_max_length;
    $form['#validate'][] = 'complete_profile_validate';

  } elseif ($form['#id'] == 'change-pwd-page-form') {
    $form['#submit'][] = 'change_password_submit';
  } elseif ($form['#id'] == 'pfff-login') {
    $form['field_cpf_cpnj']['und']['0']['value']['#maxlength'] = $cpf_cpnj_max_length;
    $form['field_cep']['und']['0']['value']['#maxlength'] = $cep_max_length;
    $form['field_phone']['und']['0']['value']['#maxlength'] = $phone_max_length;
    $form['#validate'][] = 'facebook_login_validate';
  } elseif ($form['#id'] == 'contact-site-form') {

    $form['name']['#title'] = t('Nome');
    $form['mail']['#title'] = 'E-mail';
    $form['message']['#title'] = t('Comentário');
    $form['actions']['submit']['#value'] = t('Enviar mensagem') . '<span class="fa fa-angle-right"></span>';

    $form['name']['#default_value'] = t('');
    $form['mail']['#default_value'] = t('');
    $form['subject']['#value'] = t('FALE CONOSCO');

    $form['name']['#required'] = 1;
    $form['mail']['#required'] = 1;
    $form['message']['#required'] = 1;
    $form['subject']['#required'] = 0;

    $form['#validate'][] = 'contact_validate';
    $form['#submit'][] = 'contact_submit';

  } elseif ($form['#id'] == 'webform-client-form-23') { // Contact form

    $form['submitted']['e_mail']['#default_value'] = t('');
    $form['actions']['submit']['#value'] = t('Enviar mensagem') . '<span class="fa fa-angle-right"></span>';

  } elseif ($form['#id'] == 'webform-client-form-24') { // Advertising form
    $form['actions']['submit']['#value'] = t('Enviar mensagem') . '<span class="fa fa-angle-right"></span>';
  }
  // } elseif ($form['#id'] == 'form-create-sale-bid') {
  //   $form['#validate'][] = 'create_sale_bid_validate';
  // }

  if ($form['#id'] == 'user-register-form' || $form['#id'] == 'user-profile-form') {
    $form['field_lat']['und']['0']['value']['#maxlength'] = 20;
    $form['field_lng']['und']['0']['value']['#maxlength'] = 20;
  }
}

function change_password_submit($form, &$form_state) {
  global $user;
  if (isset($_SESSION['pass_reset_' . $user->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $user->uid]))
  {
    global $user;
    drupal_goto("user/{$user->uid}/edit");
  }
}

function reset_password_submit($form,&$form_state){
  form_set_error('No delete');
}

function profile_submit($form, &$form_state) {
  drupal_get_messages(); // Clear the messages
  drupal_set_message('As alterações foram salvas.') ;

  global $user;
  if (isset($_SESSION['flag_seller_info'])) {
    $redirect_to = $_SESSION['flag_seller_info'];
    // clear flag when redirect
    unset($_SESSION['flag_seller_info']);
    // redirect to custom page
    // if ($_SESSION['form-bids-seller']) {
    //   drupal_goto('seller_info');
    // }
    drupal_goto($redirect_to);
  }
  else {
    drupal_goto("user/{$user->uid}/edit");
  }
}

function facebook_login_validate($form, &$form_state) {
  $errors = drupal_get_messages();
  form_clear_error();

  $cpf_cpnj = $form['field_cpf_cpnj']['und']['0']['value']['#value'];
  $cep = $form['field_cep']['und']['0']['value']['#value'];
  $address = $form['field_address']['und']['0']['value']['#value'];
  $number = $form['field_number']['und']['0']['value']['#value'];
  $state = $form['field_state']['und']['#value'];
  $city = $form['field_city']['und']['#value'];
  // $phone = $form['field_phone']['und']['0']['value']['#value'];

  validate_cpf_cnpj($cpf_cpnj);
  validate_cep($cep);
  validate_location($address, $number, $state, $city);
  // validate_phone($phone);
}

function profile_validate($form, &$form_state) {
  $errors = drupal_get_messages();
  form_clear_error();

  $cpf_cpnj = $form['field_cpf_cpnj']['und']['0']['value']['#value'];
  $cep = $form['field_cep']['und']['0']['value']['#value'];
  $address = $form['field_address']['und']['0']['value']['#value'];
  $number = $form['field_number']['und']['0']['value']['#value'];
  $state = $form['field_state']['und']['#value'];
  $city = $form['field_city']['und']['#value'];
  $phone = $form['field_phone']['und']['0']['value']['#value'];
  // echo "<pre>"; print_r($form['field_phone']);die;
  validate_cpf_cnpj($cpf_cpnj);
  validate_cep($cep);
  validate_location($address, $number, $state, $city);
  validate_phone($phone);

  if (isset($errors['error'])) {
    foreach ($errors['error'] as $err) {
      if ($err == t('CPF / CNPJ field must have either 11 or 14 digits.')) {
        form_set_error('field_cpf_cpnj', t('Campo CPF / CNPJ deve ter 11 ou 14 dígitos.'));
      } elseif ($err ==t('CNPJ field does not allow a sequence of the same number.')) {
        form_set_error('field_cpf_cpnj', t('Campo CNPJ não permite uma sequência do mesmo número.'));
      } elseif ($err ==t('CPF field does not allow a sequence of the same number.')) {
        form_set_error('field_cpf_cpnj', t('Campo CPF não permite uma sequência do mesmo número.'));
      } elseif ($err ==t('CNPJ number you have entered is invalid.')) {
        form_set_error('field_cpf_cpnj', t('CNPJ você digitou é inválido.'));
      } elseif ($err ==t('CPF number you have entered is invalid.')) {
        form_set_error('field_cpf_cpnj', t('CPF você digitou é inválido.'));
      }
    }
  }
}

function register_validate($form, &$form_state) {
  $errors = drupal_get_messages();
  form_clear_error();

  $cpf_cpnj = $form['field_cpf_cpnj']['und']['0']['value']['#value'];
  $cep = $form['field_cep']['und']['0']['value']['#value'];
  $address = $form['field_address']['und']['0']['value']['#value'];
  $number = $form['field_number']['und']['0']['value']['#value'];
  $state = $form['field_state']['und']['#value'];
  $city = $form['field_city']['und']['#value'];
  $phone = $form['field_phone']['und']['0']['value']['#value'];

  // Email
  if (strlen($form['account']['mail']['#value']) == 0) {
    form_set_error('mail', t('Campo de E-mail é obrigatório.'));
  }

  // Password
  if (strlen($form['account']['pass']['pass1']['#value']) == 0) {
    form_set_error('pass', t('Campo de Senha de segurança é obrigatório.'));
  } elseif (strlen($form['account']['pass']['pass1']['#value']) < 6) {
    form_set_error('pass', t('A senha deve ter mais de 6 caracteres.'));
  } elseif ($form['account']['pass']['pass1']['#value'] != $form['account']['pass']['pass2']['#value']) {
    form_set_error('pass', t('As senhas especificados não corresponderem.'));
  }

  validate_cpf_cnpj($cpf_cpnj);
  validate_cep($cep);
  validate_location($address, $number, $state, $city);
  validate_phone($phone);

  if (isset($errors['error'])) {
    foreach ($errors['error'] as $err) {
      if ($err == t('The e-mail address %email is already registered. <a href="@password">Have you forgotten your password?</a>', array('%email' => $form_state['values']['mail'], '@password' => url('user/password')))) {
        form_set_error('mail', t('O endereço de e-mail já está registrado.'));
      } elseif ($err == t('The e-mail address %mail is not valid.', array('%mail' => $form_state['values']['mail']))) {
        form_set_error('mail', t('O endereço de e-mail não é válido.'));
      } elseif ($err == t('CPF / CNPJ field must have either 11 or 14 digits.')) {
        form_set_error('field_cpf_cpnj', t('Campo CPF / CNPJ deve ter 11 ou 14 dígitos.'));
      } elseif ($err ==t('CNPJ field does not allow a sequence of the same number.')) {
        form_set_error('field_cpf_cpnj', t('Campo CNPJ não permite uma sequência do mesmo número.'));
      } elseif ($err ==t('CPF field does not allow a sequence of the same number.')) {
        form_set_error('field_cpf_cpnj', t('Campo CPF não permite uma sequência do mesmo número.'));
      } elseif ($err ==t('CNPJ number you have entered is invalid.')) {
        form_set_error('field_cpf_cpnj', t('CNPJ você digitou é inválido.'));
      } elseif ($err ==t('CPF number you have entered is invalid.')) {
        form_set_error('field_cpf_cpnj', t('CPF você digitou é inválido.'));
      }
    }
  }
}

function complete_profile_validate($form, &$form_state) {
  $errors = drupal_get_messages();
  form_clear_error();

  $cpf_cpnj = $form['field_cpf_cpnj']['und']['0']['value']['#value'];
  $cep = $form['field_cep']['und']['0']['value']['#value'];
  $address = $form['field_address']['und']['0']['value']['#value'];
  $number = $form['field_number']['und']['0']['value']['#value'];
  $state = $form['field_state']['und']['#value'];
  $city = $form['field_city']['und']['#value'];
  $phone = $form['field_phone']['und']['0']['value']['#value'];

  validate_cpf_cnpj($cpf_cpnj);
  validate_cep($cep);
  validate_location($address, $number, $state, $city);
  validate_phone($phone);
}

function contact_validate($form, &$form_state) {
  $errors = drupal_get_messages();
  form_clear_error();

  if (isset($errors['error'])) {
    foreach ($errors['error'] as $err) {
      if ($err == t('Comentário field is required.')) {
        form_set_error('message', t('Comentário campo é requerido.'));
      } elseif ($err == t('Nome field is required.')) {
        form_set_error('name', t('Nome campo é requerido.'));
      } elseif ($err == t('E-mail field is required.')) {
        form_set_error('mail', t('E-mail campo é requerido.'));
      } elseif ($err == t('You must enter a valid e-mail address.')) {
        form_set_error('mail', t('Você deve digitar um endereço de e-mail válido.'));
      } elseif ($err == t('You cannot send more than 5 messages in 1 hour. Try again later.')) {
        form_set_error('mail', t('Você não pode enviar mais de 5 mensagens em 1 hora. Tente mais tarde.'));
      }
    }
  }
}

// function create_sale_bid_validate($form, &$form_state) {
//   // $errors = drupal_get_messages();
//   // form_clear_error();

//   // if (isset($errors['error'])) {
//   //   foreach ($errors['error'] as $err) {
//   //     if ($err == t('This field is required.')) {
//   //       form_set_error('color', t('Você deve escolher uma cor'));
//   //     }
//   //   }
//   // }
// }

function contact_submit($form, &$form_state) {
  $form_state['redirect'] = false;
  drupal_get_messages();
  drupal_set_message('O e-mail foi enviado.');
}

function register_submit($form, &$form_state){
  if (isset($form_state['uid'])) {
    $_SESSION['first_login'] = true;
    global $user;
    drupal_goto("user/{$user->uid}/edit");
  }
}

function validate_cpf_cnpj($cpf_cpnj) {
  // CPF/CNPJ
  if (strlen($cpf_cpnj) == 0) {
    form_set_error('field_cpf_cpnj', t('Campo de CPF/CNPJ é obrigatório.'));
  } else {
    $cpf_format = "/^[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}$/";
    $cnpj_format = "/^[0-9]{2}.[0-9]{3}.[0-9]{3}\/[0-9]{4}-[0-9]{2}$/";
    $is_format = false;
    if (preg_match($cpf_format, $cpf_cpnj) || preg_match($cnpj_format, $cpf_cpnj)) {
      $is_format = true;
    }
    if(!$is_format) {
      form_set_error('field_cpf_cpnj', t('CPF/CNPJ não é o formato correto'));
    }
  }
}

function validate_phone($phone) {
  if($phone != null) {
    $phone_format_11 = "/^\([0-9]{2}\) [0-9]{5}-[0-9]{4}$/";
    $phone_format_10 = "/^\([0-9]{2}\) [0-9]{4}-[0-9]{4}$/";
    $is_phone = false;
    if(preg_match($phone_format_11, $phone) || preg_match($phone_format_10, $phone)) {
      $is_format = true;
    }

    if(!$is_format) {
      form_set_error('field_phone', t('telefone não é o formato correto'));
    }
  }

}

function validate_cep($cep) {
  // CEP
  if (strlen($cep) == 0) {
    form_set_error('field_cep', t('Campo de CEP é obrigatório.'));
  } else {
    $cep_format = "/^[0-9]{5}-[0-9]{3}$/";
    if (!preg_match($cep_format, $cep)) {
      form_set_error('field_cep', t('CEP não é o formato correto'));
    }
  }
}

function validate_location($address, $number, $state, $city) {
  // Address
  if (strlen($address) == 0) {
    form_set_error('field_address', t('Campo de Dirección é obrigatório.'));
  }

  // Number
  if (strlen($number) == 0) {
    form_set_error('field_number', t('Campo de Número é obrigatório.'));
  }

  // State
  if ($state == "_none" || is_array($state)) {
    form_set_error('field_state', t('Campo de Estado é obrigatório.'));
  }

  // City
  if ($city == "_none" || is_array($city)) {
    form_set_error('field_city', t('Campo de Cidade é obrigatório.'));
  }
}
