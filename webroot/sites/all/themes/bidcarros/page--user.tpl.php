<?php include 'templates/header.tpl.php'; ?>
<?php if (defined('DISABLE_BELOW_CODE') && is_user_profile_page() && user_is_logged_in()) : ?>
  <div id="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <strong><?php print t('Minha área'); ?></strong>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 text-right">
          <a href="<?php print url('/user/logout'); ?>"><?php print t('Sair'); ?></a>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>

<div id="content" class="content">
  <div class="container">
    <div class="row">
      <?php $main_content_user = 'col-md-12'; ?>
      <?php if (is_user_profile_page() && user_is_logged_in()) : ?>
        <?php $main_content_user = 'col-md-8'; ?>
        <div id="sidebar" class="col-md-4 col-sm-12">
          <div class="container-fluid">

            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="widget col-xs-12 col-md-12 col-sm-12" id="accordion">
                    <?php if (strlen(extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value')) > 0){
                        $user_name = extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value');
                    }else{
                      $user_name = $user->name;
                    }?>
                  <h3 class="title user-name-area"><?php print t('Bem vindo(a), ').ucwords($user_name); ?></h3>
                  <a class="title nav-menu"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Minha área <span class="menu-profile-text-title">/ Meu perfil</span> <span class="fa fa-angle-down pull-right"></span></a>
                  <ul class="menu" >
                    <li class="col-xs-12 col-sm-4 col-md-12"><a href="<?php print url("user/{$user->uid}/edit"); ?>"><?php print t('Meu perfil'); ?></a></li>
                    <li class="col-xs-12 col-sm-4 col-md-12"><a href="<?php print url("user/{$user->uid}/change-password"); ?>"><?php print t('Alterar Senha'); ?><a/></li>
                    <li class="col-xs-12 col-sm-4 col-md-12"><a href="<?php print url("user/{$user->uid}/notification"); ?>"><?php print t('Notificações'); ?></a></li>
                    <?php if(isset($user->roles[5]) && $user->roles[5] == 'CMS Registered User - Legal Person'){ ?>
                      <!-- <li class="col-xs-12 col-sm-4 col-md-12 <?php #echo $user->roles[3] ?>"><a href="<?#php print url("user/{$user->uid}/massive"); ?>"><?php# print t('Lista de vendas de importação'); ?></a></li> -->
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-3 btn-purchase-list">
                <a href="<?php print url("user/purchase-bids"); ?>" class="btn btn-default btn-large full-width">
                  <?php print t('MEUS <span>BID’s</span>'); ?>
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
              <div class="col-md-12 col-sm-3 btn-sale-list">
                <a href="<?php print url("user/sale-bids"); ?>" class="btn btn-default btn-large full-width">
                  <?php print t('MINHAS <span>OFERTAS</span>'); ?>
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>

              <div class="col-md-12 col-sm-3 btn-buyer-bid">
                <a href="<?php print $front_page; ?>" class="btn btn-primary btn-large full-width">
                  <?php print t('QUERO <strong>COMPRAR</strong>'); ?>
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
              <div class="col-md-12 col-sm-3 btn-seller-bid">
                <a href="<?php print url('search-car'); ?>" class="btn btn-success btn-large full-width">
                  <?php print t('QUERO <strong>VENDER</strong>'); ?>
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>

          </div>
        </div>
      <?php endif; ?>
      <div id="main-content" class="<?php print $main_content_user; ?> col-sm-12">
        <?php
        if (!isset($_SESSION['first_login'])) {
          print $messages;
        }
        print render($page['content']);
        ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"
src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBTiT75yZ5OWYHn3w0QEgtqsTgqvWT0Upw&language=en">
</script>
<?php include 'templates/footer.tpl.php'; ?>
