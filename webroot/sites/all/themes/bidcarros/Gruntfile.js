/**********************************
1. Install Node.js
2. Install Grunt
$ npm install -g grunt
$ npm install -g grunt-cli
$ cd YOUR_PROJECT_DIRECTORY
$ npm init
$ npm install grunt grunt-contrib-less grunt-contrib-watch jit-grunt --save-dev
$ grunt
***********************************/
module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "css/style.css": "less/style.less" // destination file and source file
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};
