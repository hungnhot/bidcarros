<?php

/**
 * Include all global funtions in Bidcarros
 * @author  NgocLB <[ngoclb@elarion.com]>
 * @date (Sat May 16 2015)
 */

function is_user_profile_page() {
  $other_actions = array(
    'login',
    'register',
    );
  $special_actions = array(
    'purchase-bids',
    'sale-bids',
    );
  if (arg(0) == 'user')
  {
    if (is_numeric(arg(1)) && !in_array(arg(2), $other_actions))
      return true;
    elseif (in_array(arg(1), $special_actions))
      return true;
  }
  return false;
}

function is_quero_comprar_page() {
  if (arg(0) === 'buyer') {
    return true;
  }
  return false;
}

function is_quero_vender_page() {
  if (arg(0) === 'seller' || arg(0) === 'seller-info' || arg(0) === 'search-car') {
    return true;
  }
  return false;
}

function is_purchase_bid_page() {
  if(arg(1) === 'purchase-bids') {
    return true;
  }
  return false;
}

function is_sale_bid_page() {
  if(arg(1) === 'sale-bids') {
    return true;
  }
  return false;
}

function is_purchase_bid_detail_page() {
  if(arg(2) === 'bid-details') {
    return true;
  }
  return false;
}

function is_purchase_bid_related_page() {
	global $user;
	if( arg(3) != null && is_numeric(arg(3)) ) {
		$id = arg(3);
		$bid = get_user_info('tbl_sale_bids', 'id', $id);
		if(arg(1) != $bid->seller_id) {
  			return true;
		}
	}
	return false;
}

function is_sale_bid_detail_page() {
  if(arg(2) === 'sale-bid-detail') {
    return true;
  }
  return false;
}

function is_search_car_seller_page() {
  if(arg(0) === 'search-car') {
    return true;
  }
  return false;
}

function get_user_info($table, $field, $value){
  $query = "SELECT * FROM {{$table}} WHERE {$field} = {$value}";
  $result = db_query($query)->fetchObject();
  return $result;
}
