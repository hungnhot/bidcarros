<div class="container-fluid">
  <div class="container-fluid header-form-register">
    <h2 class="title">Informações de registro</h2>
    <a href="javascript:void(0)" class="link-to-fb"><div class=" btn-facebook"><i class="fa fa-facebook"></i><span> Cadastre-se com <b>Facebook</b></span></div></a>
    <?php print render($form['hybridauth']); ?>
  </div>
  <div class="container-fluid form-register">
  <?php
    // echo "<pre>"; print_r($form['field_phone']['und']['0']['value']['#value']);
    define('EMAIL_LENGTH', 64);

    # Field Email
    $form['account']['mail']['#title'] = 'E-mail';
    $form['account']['mail']['#maxlength'] = EMAIL_LENGTH;
    $form['account']['mail']['#description'] = NULL;

    # Field Password
    $form['account']['pass']['pass1']['#title'] = t('Senha de segurança');
    $form['account']['pass']['pass2']['#title'] = t('Senha de segurança Repita');

    # Field Agree
    $form['agree']['#type'] = 'checkbox';
    $form['agree']['#name'] = t('is_agree');
    $form['agree']['#title'] = t('Aceitar os termos e condições');
    $form['agree']['#required'] = true;

    // 0 is checked
    if ((isset($_POST['is_agree']) && intval($_POST['is_agree']) == 0))
    {
      $form['agree']['#checked'] =  "checked";
    }
  ?>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_name']); ?>
      <?php print render($form['account']['mail']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_photo']); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['account']['pass']['pass1']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['account']['pass']['pass2']); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_cpf_cpnj']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_cep']); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_address']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_number']); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_complement']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_state']); ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_city']); ?>
    </div>
    <div class="col-md-6 col-xs-12">
      <?php print render($form['field_phone']); ?>
    </div>
  </div>
  <?php
    print render($form['field_lat']);
    print render($form['field_lng']);
  ?>
    <div class="canvas_map">
      <div id="map-canvas"></div>
      <div class="checkbox checkbox-info checkbox-circle">
        <input id="isShow" type="checkbox" value="ok" onchange="show_map()" checked />
        <label for="isShow">EXIBIR MINHA LOCALIZAÇÃO PARA VENDEDORES/COMPRADORES.</label>
      </div>
    </div>
  </div>
  <div class="container-fluid term-of-use" style="display:none">
    <p><b>Senha de segurança</b></p>
    <div id="content-term">
      <h3><strong>TERMOS DE USO DO SITE</strong></h3>
      <p>&nbsp; BIDCARROS, marca registrada inicialmente pela Segurar Tecnologia S/A e com participa&ccedil;&atilde;o societ&aacute;ria do Grupo Jovem Pan, tem como objetivo aproximar pessoas f&iacute;sicas ou jur&iacute;dicas de direito privado comprador e vendedor de ve&iacute;culos automotores, por meio de mecanismo de busca personalizada oferecidos por pessoas f&iacute;sicas e jur&iacute;dicas parceiras. O software oferecido no site BIDCARROS objetiva fazer com que o usu&aacute;rio possa escolher o ve&iacute;culo automotor que contenha as caracter&iacute;sticas e/ou opcionais previamente especificados por ele, de acordo com seu interesse, visualizando comparativa e gratuitamente as condi&ccedil;&otilde;es oferecidas pelo mercado vendedor do bem escolhido.&nbsp;</p>
      <p>&nbsp; Estes Termos de Uso do Site regulam o acesso &agrave;s p&aacute;ginas da Internet da BIDCARROS e n&atilde;o se aplicam &agrave;s p&aacute;ginas de Internet de terceiros, ainda que tenham conex&atilde;o com as p&aacute;ginas de Internet e aplicativos da BIDCARROS. O uso das p&aacute;ginas de Internet da BIDCARROS est&aacute; sujeito a estes Termos de Uso do Site e tamb&eacute;m &agrave; Pol&iacute;tica de Privacidade.</p>

      <p>&nbsp; Sem preju&iacute;zo aos direitos legais do usu&aacute;rio, BIDCARROS se reserva o direito de alterar estes Termos de Uso do Site de modo a refletir avan&ccedil;os tecnol&oacute;gicos, mudan&ccedil;as na lei ou normas regulat&oacute;rias e boas pr&aacute;ticas comerciais.</p>
      <p>&nbsp; Ao acessar ou utilizar as p&aacute;ginas de Internet ou aplicativos da BIDCARROS, o usu&aacute;rio declara que leu, entendeu e concorda que est&aacute; sujeito &agrave; vers&atilde;o atual destes Termos de Uso do Site. Se o usu&aacute;rio n&atilde;o concordar com estes Termos de Uso do Site ou n&atilde;o estiver satisfeito com as p&aacute;ginas de Internet da BIDCARROS, o seu &uacute;nico recurso &eacute; n&atilde;o us&aacute;-las ou deixar de us&aacute;-las.</p>
      <h3><strong>AVISO LEGAL</strong></h3>
      <p>O usu&aacute;rio reconhece e concorda que:</p>
      <p>&nbsp; &nbsp; a)&nbsp;<span>deve preencher o formul&aacute;rio com informa&ccedil;&otilde;es verdadeiras e precisas, responsabilizando-se civil e criminalmente por sua veracidade, devendo atualizar os dados e informa&ccedil;&otilde;es sempre que houver altera&ccedil;&otilde;es.</span></p>
      <p>&nbsp; &nbsp; b)&nbsp;<span>BIDCARROS n&atilde;o oferece garantias de qualquer natureza quanto &agrave;s informa&ccedil;&otilde;es ou conte&uacute;dos originados dos participantes, sejam eles compradores ou vendedores ou de terceiros publicados nas p&aacute;ginas de Internet da BIDCARROS. Neste ato, BIDCARROS se exime de qualquer responsabilidade por declara&ccedil;&otilde;es ou garantias, expressas ou impl&iacute;citas, previstas em lei, contrato ou outro instrumento, incluindo, sem limita&ccedil;&atilde;o, quaisquer garantias, produtos e servi&ccedil;os oferecidos nas p&aacute;ginas de Internet no site BIDCARROS a finalidade espec&iacute;fica ou de n&atilde;o infra&ccedil;&atilde;o &agrave;s leis vigentes.</span></p>
      <p>&nbsp; &nbsp; c)&nbsp;<span>BIDCARROS n&atilde;o ser&aacute;, em nenhuma hip&oacute;tese, respons&aacute;vel por qualquer dano, de qualquer tipo ou natureza, incluindo, sem limita&ccedil;&atilde;o, qualquer dano direto, indireto, moral ou lucros cessantes, ocasionado por ou relacionado &agrave; exist&ecirc;ncia ou ao uso das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS e/ou &agrave;s informa&ccedil;&otilde;es ou conte&uacute;dos publicados nas p&aacute;ginas de Internet da BIDCARROS e/ou &agrave; contrata&ccedil;&atilde;o de servi&ccedil;os, produtos ou servi&ccedil;os intermediados pela BIDCARROS por meio das p&aacute;ginas de internet e/ou aplicativos da BIDCARROS e/ou &agrave; demora ou atraso de um parceiro da BIDCARROS, ainda que o BIDCARROS possa ter sido avisado da possibilidade de tais danos.</span></p>
      <p>&nbsp; &nbsp; d)&nbsp;<span>BIDCARROS n&atilde;o se responsabiliza e n&atilde;o oferece qualquer garantia da exatid&atilde;o, efetividade, oportunidade e adequa&ccedil;&atilde;o de qualquer informa&ccedil;&atilde;o, conte&uacute;do, seguro, produto ou servi&ccedil;o fornecido por terceiros, inclusive hiperlinks para p&aacute;ginas de terceiros. Salvo men&ccedil;&atilde;o contr&aacute;ria nas p&aacute;ginas de Internet da BIDCARROS, BIDCARROS n&atilde;o editar&aacute;, n&atilde;o exercer&aacute; censura ou qualquer tipo de controle sobre conte&uacute;dos de terceiros veiculados em malas diretas eletr&ocirc;nicas, salas de bate-papo ou outros tipos semelhantes de f&oacute;runs publicados nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS. Tais informa&ccedil;&otilde;es dever&atilde;o ser, portanto, consideradas suspeitas e n&atilde;o endossadas pela BIDCARROS.</span></p>
      <h3><span><strong>CONDI&Ccedil;&Otilde;ES DE USO</strong></span></h3>
      <p><span>O usu&aacute;rio declara que entende, reconhece e concorda com os seguintes termos:</span></p>
      <p>&nbsp; &nbsp; a)&nbsp;<span>Ao usar as p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, concorda em n&atilde;o corromper ou interceptar quaisquer informa&ccedil;&otilde;es eletr&ocirc;nicas publicadas nestas p&aacute;ginas ou armazenadas nos servidores da BIDCARROS. O usu&aacute;rio tamb&eacute;m concorda em n&atilde;o tentar contornar qualquer medida de seguran&ccedil;a das p&aacute;ginas de Internet da BIDCARROS, cumprindo todas as leis e regulamentos pertinentes, sejam elas municipais, estaduais, federais e internacionais.</span></p>
      <p>&nbsp; &nbsp; b)&nbsp;<span>Usu&aacute;rio concede ao BIDCARROS o direito de utilizar todo conte&uacute;do inserido ou de outra forma transmitido por ele nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, sujeito &agrave;s disposi&ccedil;&otilde;es destes Termos de Uso do Site, a crit&eacute;rio da BIDCARROS, inclusive, sem limita&ccedil;&atilde;o a c&oacute;pia, exibi&ccedil;&atilde;o, execu&ccedil;&atilde;o ou publica&ccedil;&atilde;o do conte&uacute;do referido em qualquer formato, modificando-o, incorporando-o a outros materiais ou desenvolvendo materiais com base neste conte&uacute;do. Dentro do limite permitido por lei, o usu&aacute;rio renuncia a quaisquer direitos morais que possa ter sobre o conte&uacute;do inserido por ele ou de outra forma transmitido por ele nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS.</span></p>
      <p>&nbsp; &nbsp; c)&nbsp;</span>Salvo os casos expressamente definidos e acordados previamente com BIDCARROS, n&atilde;o ser&aacute; estabelecida qualquer rela&ccedil;&atilde;o de confidencialidade ou de restri&ccedil;&atilde;o por Propriedade Intelectual caso um usu&aacute;rio das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS envie ao BIDCARROS qualquer coment&aacute;rio, sugest&atilde;o, pergunta, resposta, ideia, ou outra comunica&ccedil;&atilde;o dessa natureza, seja de maneira verbal, escrita ou eletr&ocirc;nica. Se qualquer p&aacute;gina de Internet exigir ou solicitar o envio de tais informa&ccedil;&otilde;es e se tais informa&ccedil;&otilde;es contiverem informa&ccedil;&otilde;es pessoais identific&aacute;veis (ex: nome, endere&ccedil;o, n&uacute;mero de telefone, e-mail), BIDCARROS obter&aacute;, utilizar&aacute; e manter&aacute; tais informa&ccedil;&otilde;es de modo coerente com sua Pol&iacute;tica de Privacidade. Em qualquer outro caso, tais comunica&ccedil;&otilde;es e quaisquer informa&ccedil;&otilde;es encaminhadas por meio das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS ser&atilde;o consideradas n&atilde;o confidenciais, e o BIDCARROS poder&aacute; reproduzir, publicar ou utilizar, de qualquer outra forma tais informa&ccedil;&otilde;es para quaisquer fins, incluindo, sem limita&ccedil;&atilde;o, de pesquisa, desenvolvimento, uso ou venda dos seguros, produtos ou servi&ccedil;os que incorporem tais informa&ccedil;&otilde;es. O remetente de qualquer informa&ccedil;&atilde;o ao BIDCARROS &eacute; totalmente respons&aacute;vel por seu conte&uacute;do, inclusive sua veracidade e exatid&atilde;o e pela n&atilde;o infra&ccedil;&atilde;o de direitos de propriedade ou de privacidade de outras pessoas.</span></p>
      <p>&nbsp; &nbsp;d)&nbsp;<span>Ao inserir nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS as informa&ccedil;&otilde;es necess&aacute;rias para a venda de um ve&iacute;culo automotor, o usu&aacute;rio declara assumir toda a responsabilidade pela veracidade, exatid&atilde;o e atualidade das informa&ccedil;&otilde;es inseridas, reconhecendo que qualquer informa&ccedil;&atilde;o falsa, inver&iacute;dica, incompleta, desatualizada ou incorreta poder&aacute; induzir a seguradora escolhida a erro na avalia&ccedil;&atilde;o do risco envolvido, na aceita&ccedil;&atilde;o da proposta e/ou no c&aacute;lculo do pr&ecirc;mio do seguro, o que poder&aacute; produzir a perda do direito do seguro contratado, conforme artigos 765 e 766 do C&oacute;digo Civil.<span></p>
      <h3><strong>PROPRIEDADE INTELECTUAL</strong></h3>
      <p><span>&nbsp; &nbsp; As informa&ccedil;&otilde;es, documentos e imagens relacionados e publicados nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS (denominados &ldquo;Informa&ccedil;&otilde;es&rdquo;) s&atilde;o de exclusiva propriedade da BIDCARROS, exceto as informa&ccedil;&otilde;es fornecidas por terceiros, sob contrato com BIDCARROS. &Eacute; permitido o uso destas Informa&ccedil;&otilde;es, desde que: (1) a nota sobre direitos autorais acima apare&ccedil;a em todas as c&oacute;pias; (2) o uso das Informa&ccedil;&otilde;es seja exclusivamente para fins informativos, n&atilde;o-comerciais ou pessoais; (3) as Informa&ccedil;&otilde;es n&atilde;o sofram qualquer modifica&ccedil;&atilde;o; e (4) nenhum desenho ou imagem dispon&iacute;vel nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS seja usado separadamente do texto que o (a) acompanha. O BIDCARROS n&atilde;o se responsabiliza por conte&uacute;do fornecido por terceiros e o usu&aacute;rio n&atilde;o est&aacute; autorizado a distribuir tais materiais sem a expressa permiss&atilde;o do detentor dos direitos autorais neles mencionados. Exceto nos casos permitidos acima, n&atilde;o h&aacute; concess&atilde;o expressa ou impl&iacute;cita de licen&ccedil;a ou direito a qualquer pessoa, relativa a qualquer patente, direito autoral, marca comercial ou outro direito de propriedade da BIDCARROS.</span></p>
      <p><span>&nbsp; &nbsp; &nbsp;Nenhuma das marcas ou nomes comerciais, imagens de marca, produtos ou servi&ccedil;os oferecidos pela BIDCARROS veiculados nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, poder&atilde;o ser utilizados sem pr&eacute;via autoriza&ccedil;&atilde;o, por escrito da BIDCARROS.</span></p>
      <h3><span><strong>PRIVACIDADE E SEGURAN&Ccedil;A</strong></span></h3>
      <p><span>&nbsp; &nbsp; &nbsp;BIDCARROS respeita a import&acirc;ncia da privacidade de seus clientes e visitantes das p&aacute;ginas de Internet e/e/ou aplicativos. O uso que BIDCARROS faz das informa&ccedil;&otilde;es pessoais que identificam os seus clientes e visitantes das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS &eacute; regido por suaPol&iacute;tica de Privacidade e, ao acessar e utilizar as p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, o usu&aacute;rio concorda com os termos desta pol&iacute;tica.</span></p>
      <p><span>&nbsp; &nbsp; &nbsp;O usu&aacute;rio reconhece e concorda que, ao encaminhar informa&ccedil;&otilde;es pessoais que o identificam ou identificam terceiros nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, embora BIDCARROS tenha recursos instalados para evitar o acesso n&atilde;o autorizado ou a intercepta&ccedil;&atilde;o das suas informa&ccedil;&otilde;es, n&atilde;o existe garantia absoluta de seguran&ccedil;a. No caso, improv&aacute;vel, de intercepta&ccedil;&atilde;o ou acesso n&atilde;o autorizado, apesar de seus esfor&ccedil;os, BIDCARROS n&atilde;o se responsabiliza por tais intercepta&ccedil;&otilde;es ou acessos n&atilde;o autorizados, ou por qualquer dano direto, indireto, moral ou lucros cessantes sofridos por usu&aacute;rio das p&aacute;ginas de internet e/ou aplicativos da BIDCARROS ou por terceiros. Ainda que BIDCARROS tiver sido previamente avisada da possibilidade de tais danos, BIDCARROS n&atilde;o garante, expressa ou implicitamente, que as informa&ccedil;&otilde;es fornecidas por qualquer usu&aacute;rio n&atilde;o ser&atilde;o objeto de intercepta&ccedil;&atilde;o ou acesso n&atilde;o autorizado. Cada usu&aacute;rio &eacute; respons&aacute;vel pela guarda e manuten&ccedil;&atilde;o da confidencialidade de sua senha de acesso a eventuais p&aacute;ginas de internet e/ou aplicativos da BIDCARROS de uso privativo estando sujeito ter de cumprir com todas as transa&ccedil;&otilde;es legalmente v&aacute;lidas efetuadas em seu nome. Portanto, se, por qualquer motivo, a confidencialidade de sua senha estiver comprometida, imediatamente (1) troque-a, alterando seus dados de registro que foram entregues ao BIDCARROS, e (2) contate BIDCARROS</span></p>
      <h3><strong>LIMITA&Ccedil;&Atilde;O DE RESPONSABILIDADE</strong></h3>
      <p><span>&nbsp; &nbsp; &nbsp;O BIDCARROS n&atilde;o assume qualquer responsabilidade pelos materiais, informa&ccedil;&otilde;es e opini&otilde;es veiculados, fornecidos ou de outra forma disponibilizados nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS. O usu&aacute;rio confiar&aacute; no conte&uacute;do desses materiais, informa&ccedil;&otilde;es e opini&otilde;es exclusivamente por sua conta e risco. O BIDCARROS se exime de qualquer responsabilidade por les&otilde;es ou danos resultantes do uso das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS ou do seu conte&uacute;do.</span></p>
      <p><span>&nbsp; &nbsp;As p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, seu conte&uacute;do e os seguros, produtos e servi&ccedil;os nelas ou por meio delas oferecidos s&atilde;o disponibilizados &ldquo;tal como s&atilde;o&rdquo; com todas as poss&iacute;veis falhas. Em nenhuma circunst&acirc;ncia BIDCARROS, seus parceiros comerciais, fornecedores ou seus respectivos dirigentes, funcion&aacute;rios ou agentes (doravante &ldquo;partes da BIDCARROS&rdquo;) ser&atilde;o responsabilizados por qualquer dano, de qualquer tipo, sob qualquer tese legal, relacionada ou resultante do uso ou mau uso das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, seu conte&uacute;do, qualquer seguro ou produto oferecido ou servi&ccedil;o prestado por meio das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS ou qualquer p&aacute;gina relacionada, inclusive danos diretos, indiretos, morais ou lucros cessantes, resultantes inclusive, sem limita&ccedil;&atilde;o, de demora ou interrup&ccedil;&atilde;o das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, v&iacute;rus, exclus&atilde;o de arquivos ou comunica&ccedil;&otilde;es eletr&ocirc;nicas ou erros, omiss&otilde;es ou outros casos de inexatid&atilde;o nas p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS ou no seu conte&uacute;do, ainda que por neglig&ecirc;ncia da BIDCARROS ou n&atilde;o, mesmo que BIDCARROS tiver sido avisada da possibilidade de tais danos.</span></p>
      <p><span>&nbsp; &nbsp; &nbsp;Outros avisos legais, declara&ccedil;&otilde;es e outros termos e condi&ccedil;&otilde;es que vierem a ser publicados nas p&aacute;ginas de Internet da BIDCARROS integrar&atilde;o estes Termos de Uso do Site como se nele transcritos fossem.</span></p>
      <h3><strong>DISPOSI&Ccedil;&Otilde;ES GERAIS</strong></h3>
      <p><span>&nbsp; &nbsp; &nbsp; &nbsp; O usu&aacute;rio concorda que estes Termos de Uso do Site descrevem o acordo integral entre ele e o BIDCARROS, com rela&ccedil;&atilde;o ao assunto em quest&atilde;o.</span></p>
      <p><span>&nbsp; &nbsp; &nbsp;As p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS foram desenvolvidas e s&atilde;o mantidas de acordo com as leis do Brasil. Essas leis regem as condi&ccedil;&otilde;es previstas nestes Termos de Uso do Site.</span></p>
      <h3><strong>POL&Iacute;TICA DE PRIVACIDADE E SEGURAN&Ccedil;A</strong></h3>
      <p><span>&nbsp; &nbsp; &nbsp;BIDCARROS estabeleceu esta pol&iacute;tica como parte de um conjunto de a&ccedil;&otilde;es que t&ecirc;m por objetivo respeitar a privacidade e reservar o direito ao sigilo de identidade de cada usu&aacute;rio.</span></p>
      <h3><strong>CONDI&Ccedil;&Otilde;ES</strong></h3>
      <p><span>&nbsp; &nbsp; &nbsp;A presente Pol&iacute;tica de Privacidade e Seguran&ccedil;a de Dados do site ou de/e/ou aplicativos BIDCARROS cont&eacute;m diretrizes e informa&ccedil;&otilde;es sobre como ela trata e protege as informa&ccedil;&otilde;es capturadas por meio de seu website e/ou aplicativos e ser&aacute; regida pelas condi&ccedil;&otilde;es que seguem:</span></p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Classifica-se como "usu&aacute;rio" toda pessoa que de alguma forma acessa o site da BIDCARROS, cliente ou n&atilde;o.</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Entende-se por identifica&ccedil;&atilde;o individual toda informa&ccedil;&atilde;o que n&atilde;o est&aacute; dispon&iacute;vel para o p&uacute;blico em geral e que, se divulgada, pode vir a causar algum tipo de constrangimento moral, danos ou preju&iacute;zos &agrave; pr&oacute;pria pessoa. S&atilde;o exemplos de dados de identifica&ccedil;&atilde;o individual os n&uacute;meros de CPF, dados para contato e dados sobre cobran&ccedil;a.</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Criptografia &eacute; o nome dado ao processo de codifica&ccedil;&atilde;o de informa&ccedil;&otilde;es, na qual tais informa&ccedil;&otilde;es s&atilde;o codificadas (embaralhadas) na origem e decodificadas no destino, dificultando, desta forma, que as informa&ccedil;&otilde;es que trafegam na internet sejam decifradas.</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Cookie &eacute; um arquivo-texto enviado pelo servidor da BIDCARROS para o computador do usu&aacute;rio, com a finalidade de identificar o computador, personalizar os acessos do usu&aacute;rio e obter dados deacesso, como p&aacute;ginas navegadas ou links clicados. Um cookie &eacute; atribu&iacute;do individualmente a cada computador, ele n&atilde;o pode ser usado para executar programas nem infectar computadores com v&iacute;rus, e pode ser lido apenas pelo servidor que o enviou.</span></p>
      <h3><span><strong>OBRIGA&Ccedil;&Otilde;ES</strong></span></h3>
      <p><span>BIDCARROS se compromete a:</span></p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Coletar em seu web site e/ou aplicativos apenas as informa&ccedil;&otilde;es sobre identifica&ccedil;&atilde;o individual necess&aacute;rias &agrave; viabiliza&ccedil;&atilde;o de seus neg&oacute;cios e ao fornecimento de produtos e/ou servi&ccedil;os solicitados por seus clientes;</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Utilizar cookies apenas para controlar a audi&ecirc;ncia e a navega&ccedil;&atilde;o em seu web site;</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Cumprir rigorosamente todas as determina&ccedil;&otilde;es desta Pol&iacute;tica de Privacidade e Seguran&ccedil;a de Dados.</p>
      <h3><strong>UTILIZA&Ccedil;&Atilde;O DAS INFORMA&Ccedil;&Otilde;ES</strong></h3>
      <p><span>As informa&ccedil;&otilde;es capturadas por meio do site e/ou aplicativos BIDCARROS s&atilde;o utilizadas pela BIDCARROS com a finalidade de:</span></p>
      <p style="text-align: justify; padding-left: 30px;"><span>- &nbsp;Viabilizar o fornecimento de produtos ou servi&ccedil;os solicitados no web site e/ou aplicativos;</span></p>
      <p style="text-align: justify; padding-left: 30px;"><span>- &nbsp;Identificar o perfil, desejos ou necessidades dos usu&aacute;rios, a fim de aprimorar seus produtos e/ou servi&ccedil;os oferecidos pela empresa;</span></p>
      <p style="text-align: justify; padding-left: 30px;"><span>- &nbsp;Enviar informativos sobre produtos ou servi&ccedil;os que interessem aos seus usu&aacute;rios;</span></p>
      <p style="text-align: justify; padding-left: 30px;"><span>- &nbsp;Divulgar altera&ccedil;&otilde;es, inova&ccedil;&otilde;es ou promo&ccedil;&otilde;es sobre os produtos e servi&ccedil;os da empresa BIDCARROS e de seus parceiros.</span></p>
      <h3><span><strong>DIVULGA&Ccedil;&Atilde;O DE DADOS</strong></span></h3>
      <p><span>BIDCARROS n&atilde;o fornece a terceiros dados sobre a identifica&ccedil;&atilde;o individual de usu&aacute;rios, sem seu pr&eacute;vio consentimento, exceto nos casos em que:</span></p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Haja determina&ccedil;&atilde;o judicial para fornecimento de dados;</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;A viabiliza&ccedil;&atilde;o dos neg&oacute;cios e/ou servi&ccedil;os oferecidos pela BIDCARROS dependa do repasse de dados a parceiros;</p>
      <p style="text-align: justify; padding-left: 30px;">- Exista a necessidade de identificar ou revelar dados do usu&aacute;rio que esteja utilizando o seu web site e/ou aplicativos com prop&oacute;sitos il&iacute;citos (intencionalmente ou n&atilde;o);</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Al&eacute;m dos casos acima citados, havendo a necessidade ou interesse em repassar a terceiros dados de identifica&ccedil;&atilde;o individual dos usu&aacute;rios, BIDCARROS lhes solicitar&aacute; autoriza&ccedil;&atilde;o pr&eacute;via;</p>
      <p style="text-align: justify; padding-left: 30px;">- &nbsp;Terceiros que, por ventura, receberem da BIDCARROS informa&ccedil;&otilde;es, de qualquer natureza, sobre os usu&aacute;rios que acessam o seu web site ou aplicativo cabe, igualmente, a responsabilidade de zelar pelo sigilo e seguran&ccedil;a de referidas informa&ccedil;&otilde;es.</p>
      <h3><span><strong>O USU&Aacute;RIO &Eacute; INTEGRALMENTE RESPONS&Aacute;VEL POR:</strong></span></h3>
      <p>- &nbsp;Zelar pelos dados de sua identifica&ccedil;&atilde;o digital individual sempre que acessar a Internet, informando-os apenas em opera&ccedil;&otilde;es em que exista a prote&ccedil;&atilde;o de dados;</p>
      <p>- &nbsp;Fornecer informa&ccedil;&otilde;es ver&iacute;dicas;</p>
      <p>- &nbsp;Cumprir rigorosamente todas as determina&ccedil;&otilde;es e procedimentos previstos nesta Pol&iacute;tica.</p>
      <h3><span><strong>DISPOSI&Ccedil;&Otilde;ES GERAIS</strong></span></h3>
      <p><span>&nbsp; &nbsp;O usu&aacute;rio concorda que a Pol&iacute;tica de Privacidade descreve o acordo integral entre ele e BIDCARROS, com rela&ccedil;&atilde;o ao assunto em quest&atilde;o.</span></p>
      <p><span>&nbsp; As p&aacute;ginas de Internet e/e/ou aplicativos da BIDCARROS foram desenvolvidas e s&atilde;o mantidas de acordo com as leis da Rep&uacute;blica Federativa do Brasil. Essas leis regem as condi&ccedil;&otilde;es previstas nestes Termos de Uso do Site e na Pol&iacute;tica de Privacidade.</span></p>
      <p><span>&nbsp; &nbsp;Fica eleito o foro da comarca da cidade de S&atilde;o Paulo, Estado de S&atilde;o Paulo, para dirimir quaisquer quest&otilde;es oriundas do uso das p&aacute;ginas de Internet e/ou aplicativos da BIDCARROS, com ren&uacute;ncia expressa a qualquer outro por mais privilegiado que seja ou venha a ser.</span></p>
    </div>

    <div class="checkbox checkbox-info checkbox-circle">
      <input type="checkbox" name="checkbox" id="check-term" />
      <label for="check-term">Aceitar os termos e condições</label>
    </div>
  </div>
  <div class="container-fluid register-submit">
    <div class="row">
      <div class="col-md-offset-8 col-md-4 col-sm-12">
        <?php print render($form['actions']); ?>
      </div>
    </div>
  </div>
  <?php
    print render($form['form_build_id']);
    print render($form['form_id']);
  ?>
</div>
<script type="text/javascript">
  var temp;
  jQuery(document).ready(function($) {

    $('button#edit-submit').unbind();
    $('#user-register-form .hybridauth-widget div.btn-primary .text').text('Cadastre-se com Facebook');

    $('#user-register-form .btn.btn-default.form-submit').on('click', function(e){
      if (!$('#check-term').is(':checked')) {
        e.preventDefault();

        if ($('.term-of-use').css('display') != 'none') {
          $("div.checkbox.checkbox-info label[for^='check-term']").css('background-color', '#ff6363');
        }

        $('.hybridauth-widget').hide();
        $('h2.title').text("Termos de uso");
        jQuery('.form-register').hide();
        jQuery('.term-of-use').show();
        jQuery('html, body').animate({ scrollTop: jQuery('h2.title').offset().top - 100 }, 'slow');
      }

    });

    $(".image-preview").remove();
    if ( $(".image-preview").length <= 0){

      if ($('#edit-field-photo-und-0-upload').length <=0) {
        $(".image-widget-data").append('<input class="form-control form-file" type="file" id="edit-field-photo-und-0-upload" name="files[field_photo_und_0]" size="22">');
      }

      if (temp =="" || typeof(temp) == "undefined" || temp== "undefined") {
        temp = '<?php echo "../" . drupal_get_path("theme", "bidcarros") . "/img/avatar.png"; ?>';
      }

      $(".image-widget-data").append('<label for="edit-field-photo-und-0-upload"><i class="fa fa-long-arrow-up"></i> Fazer upload</label>');
      $(".image-widget-data").prepend("<div class='image-preview'><img id='preview' src='" + temp +"' alt='your image' /></div>");
      $("#edit-field-photo-und-0-upload").change(function(){
        readURL(this);
      });

      $('#preview').bind("click",function(){
        $('#edit-field-photo-und-0-upload').click();
      });
    }
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $(".image-preview").removeClass("hide");
          $('#preview').attr('src', e.target.result);
          temp = e.target.result;
          $('#preview').unbind("click");
          $('#preview').bind("click",function(){
            $('#edit-field-photo-und-0-upload').click();
          });
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

  });
</script>
