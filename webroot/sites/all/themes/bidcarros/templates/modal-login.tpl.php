<?php if (!user_is_logged_in()) : ?>
<!-- Login Modal -->
<div class="modal fade" id="login-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Fazer login</h4>
      </div>
      <div class="modal-body">
        <?php

          $form = drupal_get_form('user_login');
          $form['name']['#title'] = t('Usuário');
          $form['name']['#required'] = TRUE;
          $form['pass']['#title'] = t('Senha');
          $form['pass']['#required'] = TRUE;
          $form['actions']['submit']['#value'] = t('Fazer login');

          print drupal_render($form);
          $form['links']['#markup'] = '<a href="javascript:void(0)" class="link-to-fb"><div class=" btn-facebook"><i class="fa fa-facebook"></i><span> '.t('Entrar com Facebook').' </span></div></a><a class="user-password" href="/user/password">' . t('Esqueceu sua senha?') . '</a>'.'<p class="create_account"><a class="user-password" href="/user/register">' . t('Não tem uma conta? Registe-se aqui') . '</a></p>';
          print drupal_render($form['links']);
          // $output = '<div id="ajax-register-user-login-wrapper">';
          // $output .= '<form action="' . $form['#action'] . '" method="' . $form['#method'] . '" id="' . $form['#id'] . '" accept-charset="UTF-8"><div>';
          // $output .= drupal_render($form['name']);
          // $output .= drupal_render($form['pass']);
          // $output .= drupal_render($form['form_build_id']);
          // $output .= drupal_render($form['form_id']);
          // $output .= drupal_render($form['actions']['submit']);
          // $output .= drupal_render($form['hybridauth']);
          // $output .= drupal_render($form['remember_me']);
          // $output .= drupal_render($form['links']);
          // $output .= '</div></form></div>';
          // print $output;
        ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /Login Modal -->
<?php endif; ?>
