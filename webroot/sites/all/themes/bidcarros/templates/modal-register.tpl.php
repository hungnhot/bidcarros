<!-- Register message Modal -->
<div class="modal fade" id="alert-modal">
  <div class="modal-dialog max-width-450">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Registo com sucesso</h4>
      </div>
      <div class="modal-body">
        <div class="modal-message">
          Se ha enviado un correo <br />electrónico, para validar sus datos.
        </div>
        <div class="footer-btn">
          <a data-target="#alert-modal" data-toggle="modal" data-backdrop="static" href="#" class="btn btn_ok">Aceitar</a>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /Register message Modal -->
