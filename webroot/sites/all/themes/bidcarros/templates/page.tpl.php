<?php include 'header.tpl.php' ?>

<div id="content" class="content">
  <div class="container">
    <div class="row">
      <?php if (!empty($breadcrumb)) : ?>
        <?php print $breadcrumb; ?>
      <?php endif; ?>
      <div class="col-md-12 col-sm-12">
        <div class="main-container">
          <?php if (!empty($title)): ?>
            <h1 class="page-header"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print $messages; ?>
          <?php if ($action_links): ?>
            <ul class="action-links">
              <?php print render($action_links); ?>
            </ul>
          <?php endif; ?>
          <?php if ($primary = menu_primary_local_tasks()): ?>
          <ul class="nav nav-tabs">
            <?php print render($primary); ?>
          </ul>
          <?php endif; ?>
          <?php if ($secondary = menu_secondary_local_tasks()): ?>
          <ul class="nav nav-tabs">
            <?php print render($secondary); ?>
          </ul>
          <?php endif; ?>
          <?php print render($page['content']); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if (is_search_car_seller_page()) : ?>
  <?php echo "<script type='text/javascript' src='". drupal_get_path('theme', 'bidcarros')."/js/bidcarros_search_sale_bid.js'></script>"; ?>
<?php endif;?>
<?php include 'footer.tpl.php' ?>
