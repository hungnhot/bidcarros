<?php require_once dirname(__FILE__) . '/../functions.inc' ?>
<?php global $user, $base_url; ?>
<script type="text/javascript">var baseURL ="<?php echo $base_url; ?>";</script>
<header class="header">
  <div id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
    <div class="container">
      <div class="navbar-header">
        <?php if ($logo): ?>
          <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <?php //if (drupal_is_front_page()) $logo = str_replace('logo', 'logo-front', $logo); ?>
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <?php if (!empty($primary_nav) /*|| !empty($secondary_nav)*/ || !empty($page['navigation'])): ?>
        <div class="navbar-collapse collapse">
          <nav role="navigation">
            <?php if (!empty($primary_nav)): ?>
              <?php print render($primary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </nav>
        </div>
      <?php endif; ?>
    </div>
  </div>
</header>
<?php if (user_is_logged_in()) : ?>
  <?php global $user; ?>
  <?php if (strlen(trim(extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value'))) > 0){
    $user_name = extra_field_block_view($user->uid, 'field_data_field_name', 'field_name_value');
  }else{
    $user_name = $user->name;
  }?>
  <script>jQuery('#minha-area a').text("<?php echo ucwords($user_name); ?>");</script>
<?php else : ?>
  <script>jQuery('li#minha-area a').click(function(e) { e.preventDefault(); jQuery('#login-modal').modal('show'); });</script>
<?php endif; ?>
<?php if (!drupal_is_front_page()) : ?>
  <section id="page-header-title">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-xs-9 text-left">
          <?php if (is_user_profile_page()) : ?>
            <b><a href='<?php print url("user/{$user->uid}/edit"); ?>'><?php print t('Minha área'); ?></a></b>
          <?php elseif (is_quero_comprar_page()) : ?>
            <?php print t('QUERO <b>COMPRAR</b>'); ?>
          <?php elseif (is_quero_vender_page()) : ?>
            <?php print t('QUERO <b>VENDER</b>'); ?>
          <?php else: ?>
            <?php print !empty($title) ? $title : ''; ?>
          <?php endif; ?>

          <?php if(is_purchase_bid_page()) : ?>
            <b><?php print t('/ Meus Bids '); ?></b>
          <?php elseif(is_sale_bid_page()): ?>
            <b><?php print t('/ Minhas Ofertas '); ?></b>
          <?php endif; ?>
          <?php if(is_purchase_bid_detail_page()) : ?>
            <b><a href='<?php print url("user/purchase-bids"); ?>'><?php print t('/ Meus Bids '); ?></a><?php print t('/ Bid detalhes') ; ?></b>
          <?php elseif(is_purchase_bid_related_page()) : ?>
            <b><a href='<?php print url("user/purchase-bids"); ?>'><?php print t('/ Meus Bids '); ?></a><?php print t(' / Bid detalhes / Ofertas Detalhes') ; ?></b>
          <?php elseif(is_sale_bid_detail_page()) : ?>
            <b><a href='<?php print url("user/sale-bids"); ?>'><?php print t('/ Minhas Ofertas '); ?></a><?php print t(' / Ofertas Detalhes') ; ?></b>
          <?php endif; ?>
        </div>
        <div class="col-md-3 col-xs-3 text-right">
          <?php if (user_is_logged_in()) : ?>
            <a href="<?php print url('user/logout'); ?>"><?php print t('Sair'); ?></a>
            <?php
            // Disable cache to prevent user use "Back" button when logged out
            // https://www.drupal.org/node/197786
            drupal_add_http_header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate, post-check=0, pre-check=0');
            ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
