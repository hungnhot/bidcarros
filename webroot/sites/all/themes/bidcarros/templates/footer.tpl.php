<?php include 'modal-login.tpl.php'; ?>
<?php include 'modal-register.tpl.php'; ?>
<?php global $base_url; ?>
<footer class="footer">
  <div class="container">
    <div class="row footer-social">
      <div class="col-xs-5 col-sm-4 col-md-6 ">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>"><img src="<?php print str_replace('logo', 'logo-footer', $logo); ?>" alt="<?php print t('Casa'); ?>" class="img-responsive logo" /></a>
        <?php endif; ?>
      </div>
      <div class="col-xs-7 col-sm-8 col-md-6 social-icon text-right">
        <ul class="social-list">
          <li class="social-list-item">
            <a target="_blank" href="https://www.facebook.com/bidcarros" />
              <i class="fa fa-facebook"></i>
           </a>
          </li>
          <li class="social-list-item">
            <a target="_blank" href="https://twitter.com/bidcarros" />
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="social-list-item">
            <a target="_blank" href="https://www.youtube.com/channel/UCbBEuccq0OJfXok5fzhQ1Yw" />
              <i class="fa fa-youtube"></i>
            </a>
          </li>
          <li class="social-list-item">
            <a target="_blank" href="https://plus.google.com/u/1/109077456990574712371/about" />
              <i class="fa fa-google-plus"></i>
            </a>
          </li>
          <li class="social-list-item">
            <a target="_blank" href="https://www.linkedin.com/company/bidcarros" />
              <i class="fa fa-linkedin"></i>
            </a>
          </li>
          <li class="social-list-item">
            <a target="_blank" href="https://instagram.com/bidcarros" />
              <i class="fa fa-camera"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
<div class="row footer-nav" >
  <div class="col-xs-12 col-md-3 col-sm-12" >
    <ul class="sub-list">
      <li class="sub-item first"><a href="javascript:void(0)" class="first-submenu-footer">Institucional</a><span class="glyphicon glyphicon-plus item-plus"></span></li>
      <li class="sub-item-list"><a href=" <?php echo $base_url; ?>/about">Sobre o BIDCARROS</a></li>
      <li class="sub-item-list"><a href=" <?php echo $base_url; ?>/how-it-work">Como Funciona</a></li>
      <li class="sub-item-list"><a href=" <?php echo $base_url; ?>/privacy-policy">Política de Privacidade</a></li>
      <li class="sub-item-list"><a href=" <?php echo $base_url; ?>/term-of-use">Termos de uso</a></li>
      <li class="sub-item-list"><a href=" <?php echo $base_url; ?>/defesa-do-consumidor">Defesa do Consumidor</a></li>
    </ul>
  </div>

  <div class="col-xs-12 col-md-3 col-sm-12" >
    <ul class="sub-list">
      <li class="sub-item first"><a href="javascript:void(0)" class="first-submenu-footer">Parceiros</a><span class="glyphicon glyphicon-plus item-plus"></span></li>
      <li class="sub-item-list"><a href="http://www.estadao.com.br/jornal-do-carro" target="_blank">Jornal do Carro</a></li>
      <li class="sub-item-list"><a href="http://jovempanfm.bol.uol.com.br" target="_blank">Jovem Pan</a></li>
      <li class="sub-item-list"><a href="https://www.segurar.com" target="_blank">Segurar.com</a></li>
    </ul>
  </div>

  <div class="col-xs-12 col-md-3 col-sm-12" >
    <ul class="sub-list">
      <li class="sub-item first"><a href="javascript:void(0)" class="first-submenu-footer">Atendimento</a> <span class="glyphicon glyphicon-plus item-plus"></span></li>
      <li class="sub-item-list"><a href="<?php echo $base_url; ?>/contact">Fale conosco</a></li>
      <li class="sub-item-list"><a href="<?php echo $base_url; ?>/imprensa">Imprensa</a></li>
      <li class="sub-item-list"><a href="<?php echo $base_url; ?>/advertising">Publicidade</a></li>
    </ul>
  </div>

  <div class="col-xs-12 col-md-3 col-sm-12" >
    <ul class="sub-list">
      <li class="sub-item first"><a href="javascript:void(0)" class="first-submenu-footer">Minha conta</a><span class="glyphicon glyphicon-plus item-plus"></span></li>
      <?php global $user; if($user->uid) { ?>
        <li class="sub-item-list"><a href="<?php echo $base_url; ?>/user">Meus Dados / Perfil</a></li>
      <?php } else { ?>
        <li class="sub-item-list" id="profile-footer"><a href="<?php echo $base_url; ?>/user">Meus Dados / Perfil</a></li>
      <?php } ?>
      
    </ul>
  </div>
</div>

    <!-- <div class="row">
      <div class="col-md-6 col-sm-12">
        <span>Pra dúvidas, sugestoes, criticas e imprensa entre em contato atraves do e-mail:</span>
      </div>

      <div class="col-md-6 col-sm-12">
        contato@bidcarros
      </div>
    </div> -->
    <div class="col-md-8 col-sm-12 footer-left">
      <?php print render($page['footer_left']); ?>
    </div>
    <div class="col-md-4 col-sm-12">
      <?php print render($page['footer_right']); ?>
    </div>
  </div>
</div>
<div class="footer-copyright container">
  <div class="col-md-12">
    <?php print render($page['footer']); ?>
  </div>
</div>
</footer>
