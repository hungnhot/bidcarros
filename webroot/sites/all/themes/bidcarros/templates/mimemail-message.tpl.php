<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?> style="margin: 0; padding: 0; font-size: 15px;">

      <?php $file_uri_header = file_create_url(file_build_uri('image_mail/header.jpg')); 
        $url = $GLOBALS['base_url'];
      ?>
    <table class="main" align="center" cellpadding="0" cellspacing="0" width="850px" style="border: 3px solid #CABCBC;">
      <tr class="header" style="width:650px">
        <td><a href="<?php print $url; ?>" alt="Home page"><img src="<?php print $file_uri_header ?>" alt="header" style="width:850px"/></a</td>
      </tr>
      <tr class="body" style="width:850px">
        <td width="100%" cellpadding='20'>
          <table class="message" align="center" cellpadding="0" cellspacing="0" style="width:100%; padding: 70px 60px 80px 60px; font-size:19px;">
            <tbody style="background: #f5f5f5;">
              <tr class="row-1">
                <td class="content" style="padding: 30px 40px; border-bottom: 2px solid #CABCBC; border-right: 2px solid #CABCBC;">
                  <?php print $body ?>
                </td>
              </tr>
              <tr class="row-2" style="background: #fff;">
                <td>
                  <p class="support">Em caso de dúvidas entre em contato com a <a class="link_to" href="mailto:contato@bidcarros.com">Equipe BIDCARROS</a></p>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <?php $file_uri_footer = file_create_url(file_build_uri('image_mail/footer')); ?>
      <tr class="footer" style="background: #0088ee; width: 100%; height: 150px;">
        <td>      
          <!-- <img src="footer.jpg" alt="footer" width="100%"/> -->
          <table class="footer" align="center" cellpadding="0" cellspacing="0" style="width: 80%; border-bottom: 1px solid #6CAFE2;">
            <tr class="menu_top">
              <td class="logo">
                <a href="<?php print $url; ?>" alt="Home page"><img src="<?php print $file_uri_footer ?>/small_car.png" alt="footer" width="300px;"/></a>
              </td>
              <td class="social" style="width: 400px;">
                <a href="https://www.facebook.com/bidcarros"><img src='<?php print $file_uri_footer ?>/fb_icon.png' alt="social" width="40px" style="margin-right: 30px;"/></a>
                <a href="https://twitter.com/bidcarros"><img src="<?php print $file_uri_footer ?>/twt_icon.png" alt="social" width="40px"style="margin-right: 30px;"/></a>
                <a href="https://www.youtube.com/channel/UCbBEuccq0OJfXok5fzhQ1Yw"><img src="<?php print $file_uri_footer ?>/yt_icon.png" alt="social" width="40px"style="margin-right: 30px;"/></a>
                <a href="https://plus.google.com/u/1/109077456990574712371/about"><img src="<?php print $file_uri_footer ?>/gg_icon.png" alt="social" width="40px"style="margin-right: 22px;"/></a>
                <a href="https://www.linkedin.com/company/bidcarros"><img src="<?php print $file_uri_footer ?>/linkedIn_icon.png" alt="social" width="40px"style="margin-right: 22px;"/></a>
                <a href="https://instagram.com/bidcarros"><img src="<?php print $file_uri_footer ?>/insta_icon.png" alt="social" width="40px"/></a>
                <img src="<?php print $file_uri_footer ?>/2_logos.png" alt="production" width="500px"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr class="text_bot" style="color: #68baf9; text-align: center; background-color: #0088ee;">
        <td style="padding: 10px;">
          BIDCARROS Todos os direitos reservados 2015
        </td>
      </tr>
    </table>
  </body>
</html>