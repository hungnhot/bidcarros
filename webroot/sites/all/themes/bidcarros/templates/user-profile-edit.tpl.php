<h1 class="title">Alterar perfil</h1>
<?php
  print $rendered;
?>
<div class="canvas_map">
  <div id="map-canvas"></div>
  <div class="checkbox checkbox-info checkbox-circle">
    <input id="isShow" type="checkbox" value="ok" onchange="show_map()" checked />
    <label for="isShow">EXIBIR MINHA LOCALIZAÇÃO PARA VENDEDORES/COMPRADORES.</label>
  </div>
</div>

<script type="text/javascript">

  jQuery(document).ready(function($) {

    <?php if (isset($_SESSION['first_login'])) { ?>
      <?php unset($_SESSION['first_login']); ?>
      $("#alert-modal").modal('show');
    <?php } ?>

    var temp;
    // $(".image-preview").remove();
    if ($('.image-preview').length <= 0) {

      temp = '<?php echo "../" . drupal_get_path("theme", "bidcarros") . "/img/avatar.png"; ?>';
    } else {
      temp = $('.image-preview img').attr('src');
    }

    $("button[name^='field_photo_und_0_upload_button']").unbind();
    $("button[name^='field_photo_und_0_remove_button']").unbind();

  });
</script>