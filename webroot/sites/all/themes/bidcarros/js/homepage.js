(function($){
  $('body.front #edit-keyword').attr('placeholder', $('label[for="edit-keyword"]').text());
  $btnSearch_temp = $('body.front #form-search-car').find('.input-group-addon');
  $btnSearch = $btnSearch_temp.clone().css('padding-right', 0);
  $btnSearch_temp.remove();
  $btnSearch.prependTo('body.front #form-search-car .input-group');
  $btnCreate = $('<a id="btnCreateBID" href="'+baseURL+'/buyer/create-bid" role="button" class="input-group-addon btn">Quero <b>Comprar</b><span class="fa fa-angle-right"></span></a>');
  $btnCreate.hide().appendTo('body.front #form-search-car .input-group');
  // Cancel form submition
  $('body.front #form-search-car').submit(function() {
    // $btnCreate.hide();
    // $btnSearch.fadeIn();
    $('#search-results .wrapper').hide();
    $btnSearch.find('.icon')
        .removeClass('glyphicon-refresh fa-spin')
        .addClass('glyphicon-search');
    $.ajax({
      method: "GET",
      url: baseURL + "/get-offer-info",
      cache: true,
      data: { model: $('body.front #edit-keyword').val() }
    }).success(function(json){
      if (json.success) {
        num_of_offer = json.data.num_of_offers > 0 ? json.data.num_of_offers + ' | <small>Ofertas</small>' : '-';
        min_offer_value = json.data.min_offer_value > 0 ? 'R$ ' + number_format(json.data.min_offer_value, 0, ',', '.') : '-';
        max_offer_value = json.data.max_offer_value > 0 ? 'R$ ' + number_format(json.data.max_offer_value, 0, ',', '.') : '-';
        avg_seguro_value = json.data.lock_value > 0 ? 'R$ ' + number_format(json.data.lock_value, 0, ',', '.') : '-';
        $('#num_of_offer .ctn').html(num_of_offer);
        $('#min_offer_value .ctn').html(min_offer_value);
        $('#max_offer_value .ctn').html(max_offer_value);
        $('#avg_seguro_value .ctn').html(avg_seguro_value);
        if (json.data.show_up_icon && avg_seguro_value != '-') {
          $('#avg_seguro_value .fa-caret-up').show();
        } else {
          $('#avg_seguro_value .fa-caret-up').hide();
        }
        $('#search-results .wrapper').fadeIn();
        // if (num_of_offer != '-') {
          // $btnSearch.show();
          $btnCreate.fadeIn();
        // }
      } else {
        // $btnSearch.hide();
        // $btnCreate.hide();
      }
    });
    return false;
  });
  $('body.front #edit-keyword').keyup(function(event) {
    $icon = $btnSearch.find('.icon');
    searchString = $(this).val();
    searchString = searchString.replace(/^\s+|\s+$/, '');
    if (searchString.length <= 1) {
      $icon.removeClass('glyphicon-refresh fa-spin')
        .addClass('glyphicon-search');
    } else if ($icon.hasClass('glyphicon-search')) {
      $icon.removeClass('glyphicon-search')
        .addClass('glyphicon-refresh fa-spin');
    }
    else if ($icon.hasClass('glyphicon-refresh')) {
      $btnCreate.hide();
    }
  });
  // $btnCreate.click(function(event) {
  //   if ($('#login-modal').length > 0) { //unregister user
  //     event.preventDefault();
  //     $('#login-modal').modal('show');
  //   } else {

  //   }
  // });
  $('#search-results .wrapper').hide();
  $('#num_of_offer .fa-map-marker').tooltip({placement: 'bottom', html: true, title: 'QUANTIDADE DE<br /> OFERTAS DISPONIVEIS'});
  $('#min_offer_value .fa-arrow-circle-down').tooltip({placement: 'bottom', html: true, title: 'MENOR PREÇO ENCONTRADO NO BIDCARROS'});
  $('#max_offer_value .fa-arrow-circle-up').tooltip({placement: 'bottom', html: true, title: 'MAIOR PREÇO ENCONTRADO NO BIDCARROS'});
  $('#avg_seguro_value .lock-icon').tooltip({placement: 'bottom', html: true, title: 'PREÇO MINIMO<br/> DO SEGURO'});
})(jQuery);
