jQuery(document).ready(function($) {

  edit_cpf_cpnj_field($('input[id^="edit-field-cpf-cpnj"]'));
  edit_cep_field($('input[id^="edit-field-cep"]'));
  edit_phone_field($('input[id^="edit-field-phone"]'));
  // Login menu
  $('#navbar').find('a[href$="user/login"]').click(function(e) {
    e.preventDefault();
    $('#login-modal').modal('show');
  });

  $('.footer .sub-list #profile-footer').click(function(e) {
    e.preventDefault();
    $('#login-modal').modal('show');
  });

  // [TaiNb] add attribute to email field in my profile page.
  var userSelected = $("body.logged-in form[id^='user-profile']");
  var editUserSelected = userSelected.find("#edit-account");
  editUserSelected.find(".form-item-mail input").attr("disabled", '');
  userSelected.find("div[id^='edit-actions'] button[id^='edit-submit']").text('SALVAR').append("<span class='fa fa-angle-right'></span>");
  userSelected.find("div[id^='edit-actions'] button[id^='edit-cancel']").text('Cancelar a conta').addClass("btn btn-danger");
  // var emailSelector = $("body.page-user-register #user-register-form .form-item-mail label");
  //Get Username for name
  if (!(jQuery('.form-item-field-name-und-0-value input').val())){
    var username = jQuery('.form-item-name input').val();
    jQuery('.form-item-field-name-und-0-value input').val(username);
  }

  //Change the seller info page
  // var sellerImage = jQuery('body.page-seller-info #form-bids-seller-info #edit-seller-avatar');
  // var sellerInfo =  jQuery('body.page-seller-info #form-bids-seller-info #seler_info-wrapper');
  // if (jQuery(window).width() < 767 ) {
  //   sellerImage.after(sellerInfo);
  // } else {
  //   sellerInfo.after(sellerImage);
  // }

  // Change text of label change password to Portugar and adding bootstrap class
  var selectorPassword = jQuery('#change-pwd-page-form');
  selectorPassword.find('label[for="edit-pass-pass1"]').text("Nova senha *");
  selectorPassword.find('label[for="edit-pass-pass2"]').text("Confirmar nova senha *");
  selectorPassword.find('.form-item-current-pass').addClass("col-xs-12 col-md-12 col-sm-12");
  selectorPassword.find('.form-item-current-pass').addClass("col-xs-12 col-sm-6 col-md-6");
  var selectorPasswordConfirm = selectorPassword.find('.form-type-password-confirm');
  selectorPasswordConfirm.addClass("col-xs-12 col-sm-12 col-md-12");
  selectorPasswordConfirm.find(".confirm-parent").addClass("col-xs-12 col-sm-6 col-md-6");
  selectorPasswordConfirm.find(".password-parent").addClass("col-xs-12 col-sm-6 col-md-6");
  selectorPassword.find('#edit-submit').addClass("pull-right");


  //Get image FB
  jQuery('#edit-picture').hide();
  if(jQuery('.image-widget').find('.image-preview').length == 0 && jQuery('#edit-picture img').length > 0 ){
  	var src_image = jQuery('#edit-picture .user-picture img').attr('src');
   jQuery('.image-widget-data').hide();
   jQuery('.image-widget').append("<div class='image-preview image-fb''><img src='"+ src_image +"' width='100' height='100' alt=''></img></div>");
   jQuery('.image-widget').append("<div class='image-widget-data remove-image-fb'><div class='btn btn-danger form-submit'>Remove</div></div></div>")

   jQuery('.remove-image-fb').click(function(){
    jQuery('.remove-image-fb').remove();
    jQuery('.image-fb').remove();
    jQuery('.image-widget-data').show();
  });
 }

  //Disable mimemail template
  // jQuery('#edit-mimemail').parent().hide();

  // remove by NGOCLB
  //insert bootstrap col-xs to user register screen at title page and facebook button
  // jQuery("#user-register-form h2.title").addClass("col-xs-6");
  // jQuery("#user-register-form .hybridauth-widget-wrapper").addClass("col-xs-6");

  var sub = jQuery('.footer .footer-nav').find('.sub-list .sub-item');

  if (jQuery(window).width() < 1024 ) {
    jQuery.each(sub,function(i,e){
      jQuery(e).click(function(d){
        var parent = jQuery(d.currentTarget).parent();
        parent.find('.sub-item-list').toggle();
      });
    });
    jQuery("footer .container .footer-nav .first").click(function(){
      jQuery(this).toggleClass("open-div");
    });
  };

  jQuery('.btn-facebook').click(function(){ jQuery('.hybridauth-widget-wrapper a span').eq(0).trigger('click') });
});
// PHPJS
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '')
  .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
  prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
  sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
  dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
  s = '',
  toFixedFix = function(n, prec) {
    var k = Math.pow(10, prec);
    return '' + (Math.round(n * k) / k)
    .toFixed(prec);
  };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
  .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
  s[1] += new Array(prec - s[1].length + 1)
  .join('0');
}
return s.join(dec);
}

function edit_cpf_cpnj_field(cpf_cpnj) {
  // CPF/CNPJ
  cpf_cpnj.on('keypress', function(e) {
    mascaraMutuario(this,cpfCnpj(this.value));
  });

  cpf_cpnj.on('blur', function(e) {
    clearTimeout();
  });

  cpf_cpnj.on('paste', function(e) {
    var self = this;
    setTimeout(function(e) {
      format_cpf_cnpj(cpf_cpnj, 11, 14);
    }, 0);
  });
}

function edit_cep_field(cep) {
  // CEP
  cep.on('keypress', function(e) {
    setTimeout(function(){
      format_cep(cep, 7);
      fetch_area(cep.val());
    },0)

  });

  cep.on('paste', function(e) {
    var self = this;
    setTimeout(function(e) {
      format_cep(cep, 8);
      fetch_area(cep.val());
    }, 0);

  });

  cep.on('change', function(e) {
    setTimeout(function(e){
      fetch_area(cep.val());
    },0)
  });
}

function edit_phone_field(phone) {
  phone.on('keypress', function(e) {
    mascaraMutuario(this,phoneSetInterval(this.value));
  });

  phone.on('blur', function(e) {
    clearTimeout();
  });

  phone.on('paste', function(e) {
    var self = this;
    setTimeout(function(e) {
      format_tefephone(phone);
    }, 0);
  });
}

function format_cpf_cnpj(cpf_cpnj, length_cpf, length_cnpj){
  var val= cpf_cpnj.val().replace(/[^a-zA-Z0-9 ]/g, "");
  if (val.length == length_cpf) {
    var result = val;
    result = result.replace(/[^a-zA-Z0-9 ]/g, "");
    result = [result.slice(0, 9), '-', result.slice(9)].join('');
    result = [result.slice(0, 6), '.', result.slice(6)].join('');
    result = [result.slice(0, 3), '.', result.slice(3)].join('');
    cpf_cpnj.val(result);
  } else if (val.length == length_cnpj) {
    var result = val;
    result = result.replace(/[^a-zA-Z0-9 ]/g, "");
    result = [result.slice(0, 12), '-', result.slice(12)].join('');
    result = [result.slice(0, 8), '/', result.slice(8)].join('');
    result = [result.slice(0, 5), '.', result.slice(5)].join('');
    result = [result.slice(0, 2), '.', result.slice(2)].join('');
    cpf_cpnj.val(result);
  }
}

function format_cep(cep, length_cep) {
  var val_cep= cep.val().replace(/[^a-zA-Z0-9 ]/g, "");
  if (val_cep.length == length_cep) {
    var result = val_cep;
    result = result.replace(/[^a-zA-Z0-9 ]/g, "");
    result = [result.slice(0, 5), '-', result.slice(5)].join('');
    cep.val(result);
  }
}

function format_tefephone(phone) {
  var val_phone= phone.val().replace(/[^a-zA-Z0-9 ]/g, "");
  var result = val_phone;
  if(result.length == 11) {
    result = result.replace(/[^a-zA-Z0-9 ]/g, "");
    result = [result.slice(0, 7), '-', result.slice(7)].join('');
    result = [result.slice(0, 0), '(', result.slice(0)].join('');
    result = [result.slice(0, 3), ') ', result.slice(3)].join('');
    phone.val(result);
  }else{
    result = result.replace(/[^a-zA-Z0-9 ]/g, "");
    result = [result.slice(0, 6), '-', result.slice(6)].join('');
    result = [result.slice(0, 0), '(', result.slice(0)].join('');
    result = [result.slice(0, 3), ') ', result.slice(3)].join('');
    phone.val(result);
  }
}

function fetch_area(zip){
  // var a = "05709-010"; // CEP test
  if(zip.length == 9){
    var zipcode = zip.replace('-','');
    jQuery.getJSON("http://viacep.com.br/ws/"+ zipcode +"/json",function(data,status){
      // console.log(data);
      if(status == "success"){
        //STATE
        jQuery('select[id^="edit-field-state"] option').filter(function() {
          return this.text == data.uf;
        }).attr('selected', true).change();

        populateCities("select[name^='field_state']", "select[name^='field_city']");

        // ADDRESS
        if(jQuery('select[id^="edit-field-state"]').val() !="_none"){
          jQuery("input[name^='field_address']").val(data.logradouro);
        }

        //CITY
        jQuery('select[id^="edit-field-city"] option').filter(function() {
          return this.text == data.localidade;
        }).attr('selected', true).change();
      }
    });
  }
}

function mascaraMutuario(o,f){
  var v_obj = o;
  var v_fun = f;
  setTimeout(execmascara(v_obj, v_fun),1);
}

function execmascara(v_obj, v_fun){
  v_obj.value = v_fun;
}

function cpfCnpj(v){
  v = v.replace(/\D/g,"");
  if (v.length < 11) { //CPF

    v = v.replace(/(\d{3})(\d)/,"$1.$2");
    v = v.replace(/(\d{3})(\d)/,"$1.$2");
    v = v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

  } else { //CNPJ
    v = v.replace(/^(\d{2})(\d)/,"$1.$2");
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
    v = v.replace(/\.(\d{3})(\d)/,".$1/$2");
    v = v.replace(/(\d{4})(\d)/,"$1-$2");

  }

  return v;
}

function phoneSetInterval(v) {
  v = v.replace(/\D/g,"");
  if (v.length == 11) { //CPF

    v = v.replace(/(\d{2})(\d)/,"($1) $2");
    v = v.replace(/(\d{5})(\d)/,"$1-$2");
    // v = v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");

  }else{
    v = v.replace(/(\d{2})(\d)/,"($1) $2");
    v = v.replace(/(\d{4})(\d)/,"$1-$2");
    // v = v.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
  }
  return v;
}
