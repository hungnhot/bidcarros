var saopaulo;
var map;
var geocoder;
var marker;
var first_time = true;
var is_sale_page = false;
var is_bid_details = false;

jQuery(document).ready(function($) {
  if (typeof google !== "undefined") {
    saopaulo = new google.maps.LatLng(-23.6132239, -46.6189885);
    var $button_save = jQuery("div[id^='edit-actions']");
    var $gg_maps     = jQuery(".canvas_map");
    jQuery($gg_maps).after($button_save);
    geocoder = new google.maps.Geocoder();
  }

  // Checked if address is filled or not
  var address = getFilledAddress();
  if(address != '') first_time = false;

  // Add event handler for address form fields in Profile and Registration page
  if($("select[name^='field_state']")){
    $("select[name^='field_state']").change(function(){
      if($(this).val() != "_none"){
        first_time = false;
      }
      if($('#map-canvas').is(':visible')){
        initialize_map();
      }
    });
  }

  if($("select[name^='field_city']")){
    $("select[name^='field_city']").change(function(){
      if($(this).val() != "_none"){
        first_time = false;
      }
      if($('#map-canvas').is(':visible')){
        initialize_map();
      }
    });
  }

  if($("input[name^='field_address']")){
    $("input[name^='field_address']").focusout(function(el, event){
      if($(this).val() != ""){
        first_time = false;
      }
      if($('#map-canvas').is(':visible')){
        initialize_map();
      }
    });
  }
  // BID 195
  jQuery('#map-canvas').show();
  if($('#map-canvas').is(':visible')){
    initialize_map();
  }
});

function show_map(){
  var isChecked = jQuery('#isShow').is(':checked');
  if(!isChecked){
    jQuery('#map-canvas').hide();
  }else{
    jQuery('#map-canvas').show();
    initialize_map();
  }
}


function initialize_map() {
  var mapOptions = {
    center: saopaulo,
    zoom: 12
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  // click on map
  google.maps.event.addListener(map, 'click', function(event) {
    getAddressOnMap({'latLng': event.latLng}, function(address){
      placeMarker(event.latLng);
      var extracted_address = extractAddress(address);
      if(is_sale_page){
        jQuery("input[name^='lat']").val(event.latLng.lat());
        jQuery("input[name^='lng']").val(event.latLng.lng());
        jQuery("input[name^='location']").val(address.formatted_address);
        showAddressForm(extracted_address);
      }else{
        jQuery("input[name^='field_lat']").val(event.latLng.lat());
        jQuery("input[name^='field_lng']").val(event.latLng.lng());
        setFilledAddress(extracted_address);
      }
    });
  });

  if(is_bid_details){
    var lat = jQuery("input[name^='lat']").val();
    var lng = jQuery("input[name^='lng']").val();
    var position = new google.maps.LatLng(lat,lng);
    placeMarker(position);
    map.setCenter(position);
  }else{
    if(first_time){
      // get current location of client
      getCurrentLocation(function(position){
        placeMarker(position);
        getAddressOnMap({'latLng': position}, function(address){
          if(is_sale_page){
            jQuery("input[name^='lat']").val(position.lat());
            jQuery("input[name^='lng']").val(position.lng());
            jQuery("input[name^='location']").val(address.formatted_address);
          }else{
            jQuery("input[name^='field_lat']").val(position.lat());
            jQuery("input[name^='field_lng']").val(position.lng());
            setFilledAddress(extractAddress(address));
          }
          placeMarker(position);
        });
        map.setCenter(position);
      });
    }else{
      // get data that user entered
      var address = getFilledAddress();
      getAddressOnMap({address: address},function(addr){
        if(addr.geometry){
          jQuery("input[name^='field_lat']").val(addr.geometry.location.lat());
          jQuery("input[name^='field_lng']").val(addr.geometry.location.lng());
          placeMarker(addr.geometry.location);
          map.setCenter(addr.geometry.location);
        }
      });
    }
  }
}

/* Combine address from form field in Profile and Registration page */
function getFilledAddress(){
  var number = jQuery("input[name^='field_number']").val();
  var complemento = jQuery("input[name^='field_complemento']").val();
  var address = jQuery("input[name^='field_address']").val();
  var state = jQuery("select[name^='field_state']").val();
  var city = jQuery("select[name^='field_city']").val();

  if(state == "_none") state = "";
  if(city == "_none") city = "";

  return [number + address, state, city].filter(Boolean).join(',');
}

/* Fill picked address in form field of Profile and Registration page */
function setFilledAddress(address){
  // console.log(address);
  if(address.country == "Brazil"){
    if(address.postal_code.length == 9){
      jQuery("input[name^='field_cep']").val(address.postal_code);
    }else{
      jQuery("input[name^='field_cep']").val('');
    }
    jQuery("input[name^='field_number']").val(address.street_number);
    jQuery("input[name^='field_complemento']").val(address.route);
    jQuery("input[name^='field_address']").val(address.route);
    jQuery("select[name^='field_state']").val(address.administrative_area_level_1);
    jQuery("select[name^='field_city']").val(address.locality);
    reload_cities();
    jQuery("select[name^='field_city']").val(address.locality);
  }
}

/* Get current location of client */
function getCurrentLocation(callback){
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
      callback(pos);
    }, function() {
      // Error: The Geolocation service failed
      callback(false);
    });
  }
}

/* Get address object from address value or longlat */
function getAddressOnMap(options, callback) {
  geocoder.geocode(options, function(results, status) {
    if(status == google.maps.GeocoderStatus.OK) {
      if(status != google.maps.GeocoderStatus.ZERO_RESULTS) {
        callback(results[0]);
      }
      else {
        //"No results"
        callback(false);
      }
    }
    else {
      //"Geocode was not successful for the following reason: " + status
      callback(false);
    }
  });
}

/* Show marker on map */
function placeMarker(location) {
  if(marker) marker.setMap(null);
  marker = new google.maps.Marker({
    position: location,
    map: map
  });
}

/* Extract component from Google Map Address object */
function extractAddress(address){
  var address_components = address.address_components;
  var components={};
  jQuery.each(address_components, function(k,v1) {
    jQuery.each(v1.types, function(k2, v2){
      components[v2]=v1.long_name;
    });
  });

  return components;
}

var c_a = new Array();
c_a[0] = "none";
c_a[1] = "Cruzeiro do Sul|Rio Branco";
c_a[2] = "Arapiraca|Maceió|Palmeira dos Índios|Penedo|União dos Palmares";
c_a[3] = "Coari|Itacoatiara|Manacapuru|Manaus|Parintins|Tefé";
c_a[4] = "Macapá|Santana";
c_a[5] = "Alagoinhas|Barreiras|Camaçari|Eunápolis|Feira de Santana|Guanambi|Ilhéus|Itabuna|Jacobina|Jequié|Juazeiro|Lauro de Freitas|Paulo Afonso|Porto Seguro|Salvador|Santo Antônio de Jesus|Serrinha|Simões Filho|Teixeira de Freitas|Valença|Vitória da Conquista";
c_a[6] = "Aquiraz|Canindé|Caucaia|Crateús|Crato|Fortaleza|Iguatu|Itapipoca|Juazeiro do Norte|Maracanaú|Maranguape|Quixadá|Sobral";
c_a[7] = "Brasília";
c_a[8] = "Aracruz|Cachoeiro de Itapemirim|Cariacica|Colatina|Guarapari|Linhares|Vila Velha|São Mateus|Serra|Vitória";
c_a[9] = "Anápolis|Novo Gama|Goiânia|Valparaíso de Goiás|Luziânia|Rio Verde|Águas Lindas de Goiás|Trindade|Aparecida de Goiânia|Formosa";
c_a[10] = "Açailândia|Bacabal|Balsas|Barra do Corda|Caxias|Codó|Imperatriz|Pinheiro|Santa Inês|Santa Luzia|São José de Ribamar|São Luís|Timon";
c_a[11] = "Araguari|Araxá|Betim|Belo Horizonte|Contagem|Diamantina|Divinópolis|Governador Valadares|Ipatinga|Itabira|Itajubá|Ituiutaba|Juiz de Fora|Mariana|Montes Claros|Ouro Preto|Patos de Minas|Poços de Caldas|Poços de Caldas|Ribeirão das Neves|Sabará|Santa Luzia|Sete Lagoas|Tiradentes|Três Corações|Uberaba|Uberlândia|Varginha";
c_a[12] = "Aquidauana|Campo Grande|Corumbá|Dourados|Três Lagoas|Ponta Porã";
c_a[13] = "Cuiabá|Várzea Grande|Rondonópolis|Sinop|Cáceres|Tangará da Serra|Sorriso|Barra do Garças";
c_a[14] = "Abaetetuba|Altamira|Ananindeua|Barcarena|Belém|Bragança|Breves|Cametá|Castanhal|Itaituba|Marabá|Marituba|Paragominas|Parauapebas|Redenção|Santarém|Tucuruí";
c_a[15] = "Campina Grande|Bayeux|João Pessoa|Patos|Santa Rita";
c_a[16] = "Abreu e Lima|Araripina|Belo Jardim|Cabo de Santo Agostinho|Camaragibe|Carpina|Caruaru|Garanhuns|Goiana|Gravatá|Igarassu|Jaboatão dos Guararapes|Olinda|Paulista|Petrolina|Recife|Santa Cruz do Capibaribe|São Lourenço da Mata|Vitória de Santo Antão";
c_a[17] = "Parnaíba|Picos|Teresina";
c_a[18] = "Cascavel|Colombo|Curitiba|Foz do Iguaçu|Guarapuava|Londrina|Maringá|Paranaguá|Ponta Grossa|São José dos Pinhais";
c_a[19] = "Angra dos Reis|Barra Mansa|Belford Roxo|Cabo Frio|Duque de Caxias|Macaé|Magé|Niterói|Nova Friburgo|Nova Iguaçu|Paraty|Petrópolis|Resende|Rio de Janeiro|São Gonçalo|Teresópolis|Volta Redonda";
c_a[20] = "Ceará-Mirim|Mossoró|Natal|Parnamirim|São Gonçalo do Amarante";
c_a[21] = "Ariquemes|Cacoal|Ji-Paraná|Porto Velho";
c_a[22] = "Boa Vista";
c_a[23] = "Bagé|Canoas|Caxias do Sul|Erechim|Gravataí|Novo Hamburgo|Pelotas|Passo Fundo|Porto Alegre|Rio Grande|Santa Cruz do Sul|Santa Maria|Santana do Livramento|São Leopoldo";
c_a[24] = "Blumenau|Chapecó|Criciúma|Florianópolis|Jaraguá do Sul|Joinville|Lages|São José|Balneário Camboriú|Itajaí|Palhoça|Tubarão";
c_a[25] = "Aracaju|Itabaiana|Lagarto|Nossa Senhora do Socorro|São Cristóvão";
c_a[26] = "Americana|Araraquara|Atibaia|Barretos|Bauru|Bebedouro|Campinas|Campos do Jordão|Caçapava|Catanduva|Cubatão|Diadema|Franca|Guarujá|Guarulhos|Hortolândia|Indaiatuba|Itapeva|Itatiba|Itu|Jacareí|Jaú|Jundiaí|Limeira|Marília|Mauá|Mirassol|Mogi das Cruzes|Poá|Osasco|Ourinhos|Piracicaba|Praia Grande|Presidente Prudente|Registro|Ribeirão Preto|Salto|Santos|Santo André|São Bernardo do Campo|São Caetano do Sul|São Carlos|São João da Boa Vista|São José dos Campos|São José do Rio Preto|São Paulo|São Vicente|Sorocaba|Sumaré|Taboão da Serra|Taubaté";
c_a[27] = "Araguaína|Gurupi|Palmas";

function reload_cities() {
  var city = "select[name^='field_city']";
  var state = "select[name^='field_state']";
  var selectedStateIndex = jQuery(state + " option:selected").index();
  var cityElement = jQuery(city);

  if (selectedStateIndex >= 0) {
    var city_arr = c_a[selectedStateIndex].split("|");

    cityElement.find('option').remove();
    cityElement.append("<option value='_none'>- None -</option>");
    for (var i = 0; i < city_arr.length; i++) {
      cityElement.append("<option value='" + city_arr[i] + "'>" + city_arr[i] + "</option>");
      jQuery(city + " option[value='none']").remove();
    }
  }
}
