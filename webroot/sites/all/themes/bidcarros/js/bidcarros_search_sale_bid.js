(function($){
  // Search page
  $(document).ready(function() {
    $('body.not-front #edit-keyword').attr('placeholder', $('label[for="edit-keyword"]').text());
    $btnSearch_temp = $('body.not-front #form-search-car').find('.input-group-addon');
    $btnSearch = $btnSearch_temp.clone().css('padding-right', 0);
    $btnSearch_temp.remove();
    $btnSearch.prependTo('#form-search-car .input-group');
    $btnCreate = $('<a id="btnCreateBID" href="'+baseURL+'/seller/create-bid" role="button" class="input-group-addon btn btn-primary">Quero <b>Vender</b><span class="fa fa-angle-right"></span></a>');
    $btnCreate.hide().appendTo('body.not-front #form-search-car .input-group');

    $('body.not-front  #form-search-car').submit(function() {
        // $btnCreate.hide();
        // $btnSearch.fadeIn();
        $('body.not-front #search-results .wrapper').hide();
        $btnSearch.find('.icon')
        .removeClass('glyphicon-refresh fa-spin')
        .addClass('glyphicon-search');
        $.ajax({
          method: "GET",
          url: baseURL + "/get-offer-info",
          cache: true,
          data: { model: $('#edit-keyword').val() }
        }).success(function(json){
          if (json.success) {
              // if (num_of_offer != '-') {
                // $btnSearch.show();
                $btnCreate.fadeIn();
              // }
            } else {
              // $btnSearch.hide();
              // $btnCreate.hide();
            }
          });
        return false;
      });
    $('body.not-front #edit-keyword').keyup(function(event) {
      $icon = $btnSearch.find('.icon');
      searchString = $(this).val();
      searchString = searchString.replace(/^\s+|\s+$/, '');
      if (searchString.length <= 1) {
        $icon.removeClass('glyphicon-refresh fa-spin')
        .addClass('glyphicon-search');
      } else if ($icon.hasClass('glyphicon-search')) {
        $icon.removeClass('glyphicon-search')
        .addClass('glyphicon-refresh fa-spin');
      }
      else if ($icon.hasClass('glyphicon-refresh')) {
        $btnCreate.hide();
      }
    });
  });
})(jQuery);
