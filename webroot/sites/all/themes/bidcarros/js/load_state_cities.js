// Load States and Cities
  var state_arr = new Array("AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO");
  var c_a = new Array();
  c_a[0] = "none";
  c_a[1] = "Cruzeiro do Sul|Rio Branco";
  c_a[2] = "Arapiraca|Maceió|Palmeira dos Índios|Penedo|União dos Palmares";
  c_a[3] = "Coari|Itacoatiara|Manacapuru|Manaus|Parintins|Tefé";
  c_a[4] = "Macapá|Santana";
  c_a[5] = "Alagoinhas|Barreiras|Camaçari|Eunápolis|Feira de Santana|Guanambi|Ilhéus|Itabuna|Jacobina|Jequié|Juazeiro|Lauro de Freitas|Paulo Afonso|Porto Seguro|Salvador|Santo Antônio de Jesus|Serrinha|Simões Filho|Teixeira de Freitas|Valença|Vitória da Conquista";
  c_a[6] = "Aquiraz|Canindé|Caucaia|Crateús|Crato|Fortaleza|Iguatu|Itapipoca|Juazeiro do Norte|Maracanaú|Maranguape|Quixadá|Sobral";
  c_a[7] = "Brasília";
  c_a[8] = "Aracruz|Cachoeiro de Itapemirim|Cariacica|Colatina|Guarapari|Linhares|Vila Velha|São Mateus|Serra|Vitória";
  c_a[9] = "Anápolis|Novo Gama|Goiânia|Valparaíso de Goiás|Luziânia|Rio Verde|Águas Lindas de Goiás|Trindade|Aparecida de Goiânia|Formosa";
  c_a[10] = "Açailândia|Bacabal|Balsas|Barra do Corda|Caxias|Codó|Imperatriz|Pinheiro|Santa Inês|Santa Luzia|São José de Ribamar|São Luís|Timon";
  c_a[11] = "Araguari|Araxá|Betim|Belo Horizonte|Contagem|Diamantina|Divinópolis|Governador Valadares|Ipatinga|Itabira|Itajubá|Ituiutaba|Juiz de Fora|Mariana|Montes Claros|Ouro Preto|Patos de Minas|Poços de Caldas|Poços de Caldas|Ribeirão das Neves|Sabará|Santa Luzia|Sete Lagoas|Tiradentes|Três Corações|Uberaba|Uberlândia|Varginha";
  c_a[12] = "Aquidauana|Campo Grande|Corumbá|Dourados|Três Lagoas|Ponta Porã";
  c_a[13] = "Cuiabá|Várzea Grande|Rondonópolis|Sinop|Cáceres|Tangará da Serra|Sorriso|Barra do Garças";
  c_a[14] = "Abaetetuba|Altamira|Ananindeua|Barcarena|Belém|Bragança|Breves|Cametá|Castanhal|Itaituba|Marabá|Marituba|Paragominas|Parauapebas|Redenção|Santarém|Tucuruí";
  c_a[15] = "Campina Grande|Bayeux|João Pessoa|Patos|Santa Rita";
  c_a[16] = "Abreu e Lima|Araripina|Belo Jardim|Cabo de Santo Agostinho|Camaragibe|Carpina|Caruaru|Garanhuns|Goiana|Gravatá|Igarassu|Jaboatão dos Guararapes|Olinda|Paulista|Petrolina|Recife|Santa Cruz do Capibaribe|São Lourenço da Mata|Vitória de Santo Antão";
  c_a[17] = "Parnaíba|Picos|Teresina";
  c_a[18] = "Cascavel|Colombo|Curitiba|Foz do Iguaçu|Guarapuava|Londrina|Maringá|Paranaguá|Ponta Grossa|São José dos Pinhais";
  c_a[19] = "Angra dos Reis|Barra Mansa|Belford Roxo|Cabo Frio|Duque de Caxias|Macaé|Magé|Niterói|Nova Friburgo|Nova Iguaçu|Paraty|Petrópolis|Resende|Rio de Janeiro|São Gonçalo|Teresópolis|Volta Redonda";
  c_a[20] = "Ceará-Mirim|Mossoró|Natal|Parnamirim|São Gonçalo do Amarante";
  c_a[21] = "Ariquemes|Cacoal|Ji-Paraná|Porto Velho";
  c_a[22] = "Boa Vista";
  c_a[23] = "Bagé|Canoas|Caxias do Sul|Erechim|Gravataí|Novo Hamburgo|Pelotas|Passo Fundo|Porto Alegre|Rio Grande|Santa Cruz do Sul|Santa Maria|Santana do Livramento|São Leopoldo";
  c_a[24] = "Blumenau|Chapecó|Criciúma|Florianópolis|Jaraguá do Sul|Joinville|Lages|São José|Balneário Camboriú|Itajaí|Palhoça|Tubarão";
  c_a[25] = "Aracaju|Itabaiana|Lagarto|Nossa Senhora do Socorro|São Cristóvão";
  c_a[26] = "Americana|Araraquara|Atibaia|Barretos|Bauru|Bebedouro|Campinas|Campos do Jordão|Caçapava|Catanduva|Cubatão|Diadema|Franca|Guarujá|Guarulhos|Hortolândia|Indaiatuba|Itapeva|Itatiba|Itu|Jacareí|Jaú|Jundiaí|Limeira|Marília|Mauá|Mirassol|Mogi das Cruzes|Poá|Osasco|Ourinhos|Piracicaba|Praia Grande|Presidente Prudente|Registro|Ribeirão Preto|Salto|Santos|Santo André|São Bernardo do Campo|São Caetano do Sul|São Carlos|São João da Boa Vista|São José dos Campos|São José do Rio Preto|São Paulo|São Vicente|Sorocaba|Sumaré|Taboão da Serra|Taubaté";
  c_a[27] = "Araguaína|Gurupi|Palmas";

  function populateStates(state, city) {
    var stateElement = jQuery(state);

    if (city) {
      stateElement.on('change', function() {
        populateCities(state, city);
      });
    }
  }
  
  function populateCities(state, city) {
    var selectedStateIndex = jQuery(state + " option:selected").index();
    var cityElement = jQuery(city);

    cityElement.find('option').remove();
    cityElement.append("<option value='_none'>- None -</option>");

    var city_arr = c_a[selectedStateIndex].split("|");

    for (var i = 0; i < city_arr.length; i++) {
      cityElement.append("<option value='" + city_arr[i] + "'>" + city_arr[i] + "</option>");
      jQuery(city + " option[value='none']").remove();
    }
  }

jQuery(document).ready(function($) {
  // (function($) {
  load_current_city("select[name^='field_state']", "select[name^='field_city']");
  populateStates("select[name^='field_state']", "select[name^='field_city']"); 

  function load_current_city(state, city) {
    var currentCity = $(city + " option:selected").val();
    var selectedStateIndex = $(state + " option:selected").index();
    var cityElement = $(city);
    
    if (selectedStateIndex >= 0) {
      var city_arr = c_a[selectedStateIndex].split("|");

      cityElement.find('option').remove();
      cityElement.append("<option value='_none'>- None -</option>");
      for (var i = 0; i < city_arr.length; i++) {
        cityElement.append("<option value='" + city_arr[i] + "'>" + city_arr[i] + "</option>");
        $(city + " option[value='none']").remove();
      }
    }
    cityElement.val(currentCity);
  } 

});
// })(jQuery);