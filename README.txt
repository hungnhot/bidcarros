HOW TO DEPLOY BIDCARROS
===
Recommend environment:
    Ubuntu 14.04
    Apache2
    MySQL 5.5
    PHP 5.5
------------------------------------------------------------------------

1. Import lastest database file (ex. db/bidcarros_demo_Aug22.sql)

    cd db/
    mysql -u root -p[root_password] [database_name] < bidcarros_demo_Aug22.sql

2. Change database connection settings in

    webroot/sites/default/settings.php

3. Install Nodejs and LESSC, then run this command to compile css file:

    cd webroot/
    lessc sites/all/themes/bidcarros/less/style.less sites/all/themes/bidcarros/css/style.css

4. Chmod files folder to 777

    cd webroot/
    chmod -R 777 sites/default/files

5. Install some supported libraries:

    sudo apt-get install php5-gd
