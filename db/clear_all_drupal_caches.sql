TRUNCATE TABLE main_cache;
TRUNCATE TABLE main_cache_block;
TRUNCATE TABLE main_cache_bootstrap;
TRUNCATE TABLE main_cache_field;
TRUNCATE TABLE main_cache_filter;
TRUNCATE TABLE main_cache_form;
TRUNCATE TABLE main_cache_image;
TRUNCATE TABLE main_cache_libraries;
TRUNCATE TABLE main_cache_menu;
TRUNCATE TABLE main_cache_page;
TRUNCATE TABLE main_cache_panels;
TRUNCATE TABLE main_cache_path;
TRUNCATE TABLE main_cache_rules;
TRUNCATE TABLE main_cache_token;
TRUNCATE TABLE main_cache_views;
TRUNCATE TABLE main_cache_views_data;


